#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import argparse
import subprocess
import requests
import types
import shutil

def check_installed(appli):
    try:
        # Exécute la commande "docker --version" et capture la sortie
        output = subprocess.check_output([appli, "--version"], stderr=subprocess.STDOUT, universal_newlines=True)

        # Si la commande s'est exécutée avec succès, Docker est installé
        print(appli, "is installed. Version :", output.strip())
    except subprocess.CalledProcessError as e:
        # La commande a renvoyé une erreur, Docker n'est pas installé
        print("Error:", appli, "is not installed.\n")
        exit (1)
    except FileNotFoundError:
        # Le binaire "docker" n'a pas été trouvé, Docker n'est pas installé
        print("Error:", appli, "is not installed.\n")
        exit (1)


def download_file( url, filename):
   
     # Envoyer une requête GET pour télécharger le fichier
    response = requests.get(url)

    # Vérifier si la requête a réussi (code de statut 200)
    if response.status_code == 200:
        # Ouvrir le fichier local en mode binaire et écrire le contenu téléchargé
        with open(filename, 'wb') as fichier_local:
            fichier_local.write(response.content)
        print(f"{filename} downloaded from {url}.")
    else:
        print("Failed to download {url} :", response.status_code)

def update_application_cfg( file_name, dict_args):
    d = types.ModuleType("config")
    d.__file__ = file_name
    # read file by executing it (as done by flask)
    #  and load attribute with their type 
    try:
        with open(file_name, mode="rb") as config_file:
            exec(compile(config_file.read(), file_name, "exec"), d.__dict__)
    except IOError as e:
        e.strerror = "Unable to load configuration file (%s)" % e.strerror
        raise
    conf = {}
    for key in dir(d):
        if key.isupper():
            conf[key] = getattr(d, key)

    for key in dict_args :
        if (key.upper() in conf):
            conf[key.upper()] = dict_args[key]
    with open(file_name, mode="w") as config_file:
        for key in conf :
            if isinstance(conf[key], str) : 
                config_file.write (key+ " = '"+conf[key]+"'\n" )
            else :
                config_file.write (key+ " = "+ str(conf[key])+'\n')


def pull_img_docker (dir, img):

    # Définir la commande Docker que vous souhaitez exécuter
    command = "cd " + dir + "; docker pull registry.forgemia.inra.fr/asterics/asterics/"+img

    # Exécuter la commande Docker à l'aide de subprocess
    try:
        output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT, universal_newlines=True)
        print("Donwload : " + img)
        print(output)
    except subprocess.CalledProcessError as e:
        print("Error while trying to pull " + img)
        print(e.output)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Installation script for Asterics application.")
    parser.add_argument("--path", required=True, help="Path where the application will be installed")

    parser.add_argument("--file_size_limit", default="200M", help='Import limit file size in "B", "KB", "MB" or "GB" (default: 200M), set 0 to remove this limitation.')
    parser.add_argument("--public_url", default="https://127.0.0.1:5000", help="URL for the application (default: https://127.0.0.1:5000)")
    parser.add_argument("--mail_server", default="", help="Mail server (default: '')")
    parser.add_argument("--mail_port", type=int, default=587, help="Mail server port (default: 587)")
    parser.add_argument("--mail_username", default="", help="Mail server username (default: '')")
    parser.add_argument("--mail_password", default="", help="Mail server password (default: '')")
    parser.add_argument("--mail_use_tls", action="store_true", help="Use TLS for mail server (default: True)")
    parser.add_argument("--mail_use_ssl", action="store_true", help="Use SSL for mail server (default: False)")
    parser.add_argument("--use_nginx", action="store_true", help="Use Nginx (default: False)")
    parser.add_argument("--use_ssl", action="store_true", help="Use SSL for Nginx (default: False)")
    parser.add_argument("--ssl_cer", default="", help="SSL certificate path (eg: /path/certif.cer)")
    parser.add_argument("--ssl_key", default="", help="SSL key path (eg: /path/certif.key)")
    parser.add_argument("--contact_email", default="asterics-tlse@inrae.fr", help="Set an contact email for this instance of server (default: 'asterics-tlse@inrae.fr')")

    args = parser.parse_args()
    if (args.use_ssl and not args.use_nginx):
        print("option use_ssl can only be used with use_nginx")
        exit (1)
    if (args.use_ssl and args.use_nginx):
        if os.path.exists(args.ssl_cer):
            print(f"The file '{args.ssl_cer}' exists.")
        else:
            print(f"The file '{args.ssl_cer}' does not exist.")
        if os.path.exists(args.ssl_key):
            print(f"The file '{args.ssl_key}' exists.")
        else:
            print(f"The file '{args.ssl_key}' does not exist.")

    # Votre code d'installation ici
    print("### Installation started with the following parameters:")
    print(args)
    return args

if __name__ == "__main__":
    check_installed("docker")
    check_installed("docker-compose")

    args = parse_arguments()

    dict_args = vars(args)
    try :
        os.mkdir(args.path)
    except FileExistsError as e:
        e.strerror = "Error: directory "+args.path+" already exists!" 
        raise 
        
    url_base = "https://forgemia.inra.fr/asterics/asterics/-/raw/master/"

    appli_sample_config = os.path.join(args.path,'application.sample.cfg')
    download_file(url_base+'application.sample.cfg', appli_sample_config)
    update_application_cfg(appli_sample_config, dict_args)
    print ("Application.cfg created ")

    print ("## Create data_web and log_web directories")
    os.mkdir(os.path.join(args.path,"data_web"))
    os.mkdir(os.path.join(args.path,"log_web"))

    pull_img_docker(args.path, "python-vuejs:latest")
    pull_img_docker(args.path, "r-backend:latest")

    if (args.use_nginx) :
        print ("## Use nginx docker ")
        pull_img_docker(args.path, "nginx:latest")
        docker_compose = os.path.join(args.path,'docker-compose.yml')
        download_file(url_base+'docker-compose.nginx.yml', docker_compose)
        if (args.use_ssl) : 
            os.mkdir(os.path.join(args.path,"certificate"))
            certif_file = os.path.join(args.path,"certificate",os.path.basename(args.ssl_cer))
            certif_key = os.path.join(args.path,"certificate",os.path.basename(args.ssl_key))
            shutil.copy(args.ssl_cer , certif_file)
            shutil.copy(args.ssl_key , certif_key)

            sed_command = "sed -i -e 's,YOUR-URL,"+args.public_url+",g' \
                -e 's/#-/-/g' " + docker_compose
            try:
                subprocess.run(sed_command, shell=True, check=True)
                print("Sed command executed successfully in "+docker_compose+".")
            except subprocess.CalledProcessError as e:
                print("Error executing the sed command in "+docker_compose+":")
                print(e)
            
            ssl_conf = os.path.join(args.path,'nginx-ssl.conf')
            download_file(url_base+'docker/nginx/nginx-ssl.conf', ssl_conf)
            if (args.file_size_limit[-1]== "B"):
                size = args.file_size_limit[:-1]
            else :
                size = args.file_size_limit

            sed_command = "sed -i -e 's,YOUR-URL,"+args.public_url+",g' \
                -e 's,YOUR-CERTIF-FILE,"+os.path.basename(certif_file)+",g' \
                -e 's,YOUR-CERTIF-KEYFILE,"+os.path.basename(certif_key)+",g' \
                -e 's,200M,"+size+",g' \
                  " + ssl_conf
            try:
                subprocess.run(sed_command, shell=True, check=True)
                print("Sed command executed successfully in "+ ssl_conf+".")
            except subprocess.CalledProcessError as e:
                print("Error executing the sed command in ssl conf:")
                print(e)
     
    else: 
        docker_compose = os.path.join(args.path,'docker-compose.yml')
        download_file(url_base+'docker-compose.yml', docker_compose)

    print("### Installation completed , you can start services with : ")
    print("sudo docker-compose up -d\n")

    print("### Check services are running with : ")
    print("sudo docker ps\n")