#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import shutil
import subprocess
import configparser
import argparse
import os
import sys
from datetime import datetime

DATA_WEB = "/home/asterics/data_web"

def runcmd(cmd, verbose = False, *args, **kwargs):
    if verbose:
        print (cmd)
    process = subprocess.Popen(
        cmd,
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
        text = True,
        shell = True
    )
    std_out, std_err = process.communicate()
    if verbose:
        print(std_out.strip(), std_err)
    return std_out.strip()


#Retrieve from data directory all workspace uuid and last R session pid
def getWorkspaces(directory, verbose):
    wkspaces = {}
    for uuid in os.listdir(directory):
        wfile = os.path.join(directory, uuid, "workspace.txt")
        if ((uuid != "tmp") and os.path.exists (wfile)):
            config = configparser.ConfigParser(allow_no_value=True)
            config.read(wfile)
            pid=config.get('USER', 'last.processid') if ('last.processid' in config['USER']) else '-1'
            first = datetime.fromisoformat(config.get('USER', 'first.connection')).date()
            last = datetime.fromisoformat(config.get('USER', 'last.connection')).date()
                # si les process sont du meme jour alors on considere le lien entre workspace et pid en cours
            wkspaces[uuid] = [config.get('USER', 'email'), str(first) , str(last) ,pid]
            if verbose :
                print (uuid,"\t", "\t".join(wkspaces[uuid]))
    return (wkspaces)

#Retrieve rsessions
def getCurrentRSession(withoutDocker,verbose):
    ids_session=""
    if withoutDocker :
        ids_session = runcmd("ps -eo pid,lstart,state,cmd | grep Rserve | grep -v grep ", verbose = False)
    else:
        # get id of container    
        id_docker = runcmd("docker ps | grep r-backend | cut -f 1 -d ' '", verbose = False)
        # get session ids inside container    
        ids_session = runcmd("docker exec  "+id_docker.strip()+" ps -eo pid,lstart,state,cmd | grep Rserve", verbose = False)
    
    rsession = {}
    if not ids_session :
        print(f"Erreur : unable to retrieve any R session ids.", file=sys.stderr)
        sys.exit(1)

    for line in ids_session.split('\n'):
        try:
            # Découper les colonnes
            parts = line.split(maxsplit=7)
            pid = parts[0]  # PID
            date_str = " ".join(parts[1:6])  # Date complète
            status = parts[6]
            
            # Convertir la date
            date_obj = datetime.strptime(date_str, "%a %b %d %H:%M:%S %Y")
            rsession[pid] = [pid,status,str(date_obj.date())]
            if (verbose):
                print (rsession[pid])
            
        except Exception as e:
            print(f"Erreur lors du traitement de la ligne : {line.strip()} - {e}")
        
    return (rsession)


def killrsession(nbdays, without_docker, verbose) :
    if (without_docker) :
        cmd = "ps -o etime,pid,stime,command --no-headers -C Rserve | perl -lane 'chomp;split; if ($F[0]=~/-/) {($d)=$F[0]=~/(\d+)-.*/} else {$d=0}; if ($d>=" + str(nbdays) + ") { print \"$F[1]\"}' | tail -n +2 | xargs kill"
        runcmd(cmd, verbose = verbose)
    else :
        # get id of container    
        id_docker = runcmd("docker ps | grep r-backend | cut -f 1 -d ' '", verbose = False).strip()
        # get session ids inside container    
        cmd = "docker exec "+id_docker+" ps -o etime,pid,stime,command --no-headers -C Rserve | perl -lane 'chomp;split; if ($F[0]=~/-/) {($d)=$F[0]=~/(\d+)-.*/} else {$d=0}; if ($d>=" + str(nbdays) + ") { print \"$F[1]\"}' | tail -n +2 | xargs docker exec "+id_docker+ " kill"
        runcmd(cmd, verbose = verbose)

def rmworkspace(nbdays, directory, ws, verbose) :
    id_docker=""
    now = datetime.now()
    for (uuid, work) in ws.items() :        
        #the workspace has a session running
        lastconnexion = datetime.fromisoformat(work[2])
        delta = now - lastconnexion
        if delta.days >= nbdays:
            print ("delete " + uuid + ": "+ work[0] +" ; date acces: "+work[2] )
            cmd = "rm -rf "+ os.path.join(directory, uuid)
            runcmd(cmd, verbose = verbose)

if __name__ == '__main__':
     # Initialisation de l'analyseur d'arguments
    parser = argparse.ArgumentParser(description="Analyses R session and workspaces consistency.")
    
    # Ajout de l'argument '--directory'
    parser.add_argument(
        '--directory', 
        default= DATA_WEB,
        type=str, 
        required=True, 
        help="Data directory of asterics which contains workspaces."
    )
    parser.add_argument(
        '--without-docker', 
        action='store_true',
        help="Retrieve session on current local device."
    )
    parser.add_argument(
        '--verbose', 
        action='store_true',
        help="Display workspaces list and session list."
    )
    parser.add_argument(
        '--killr', 
        type=int, 
        required=False, 
        help="Close session opened more than x days"
    )
    parser.add_argument(
        '--rmwork', 
        type=int, 
        required=False, 
        help="Remove workspaces not accessed more than y days"
    )
    # Analyse des arguments
    args = parser.parse_args()
    
    # Récupération du répertoire
    directory = args.directory
    verbose = args.verbose
    rmwork=args.rmwork
    killr=args.killr

    # Vérification de l'existence du répertoire
    if not os.path.isdir(directory):
        print(f"Erreur : Le répertoire '{directory}' n'existe pas.", file=sys.stderr)
        sys.exit(1)
    
    # Recupere les workspace uuid, user et lastsession pid de R
    ws = {}
    try:
        if verbose :
            print ("ALL WORKSPACES : ")
        ws = getWorkspaces(directory,verbose)

    except Exception as e:
        print(f"Erreur lors de la lecture du répertoire : {e}", file=sys.stderr)
        sys.exit(1)
    
    sessions={}
    try:
        if verbose :
            print ("ALL R SESSIONS : ")
            
        sessions = getCurrentRSession(args.without_docker,verbose)
        
    except Exception as e:
        print(f"Erreur lors de la lecture du répertoire : {e}", file=sys.stderr)
        sys.exit(1)
    
    
    type_running_session = 0
    type_spleeping_session = 0
    wk_with_session = 0 
    associated_session = []
    associated_workspace = []
    for (uuid, work) in ws.items() :
        (user, datecreate, lastconnexion, lastsessionid) = work
        #the workspace has a session running
        if (lastsessionid in sessions.keys()): 
            #add check data last sessions 
            if (sessions[lastsessionid][2]):
                # si les process sont du meme jour alors on considere le lien entre workspace et pid en cours
                if sessions[lastsessionid][2] == lastconnexion or sessions[lastsessionid][2] == datecreate :
                    wk_with_session=wk_with_session+1
                    if sessions[lastsessionid][1].startswith('S'):
                        type_spleeping_session=type_spleeping_session+1
                    elif  sessions[lastsessionid][1].startswith('R'):
                        type_running_session=type_running_session+1
                    work.append(sessions[lastsessionid][1])
                    work.append(sessions[lastsessionid][2])
                    associated_session.append(lastsessionid)
                    associated_workspace.append(uuid)
                

    print ('-----------')
    print ("Nb total de workspace sur disque: ", len(ws))
    print ("Nb workspace avec une session associée: ", wk_with_session)
    print (" -  sleeping: ", type_spleeping_session)
    print (" -  running: ", type_running_session)
    print ('-----------')
    print ('Liste des workspace avec R associé: ')
    print ("#uuid\tuser\tfirst.connection\tlast.connection\tlast.processid\tprocess state\ttprocess lstart")
    for uuid in associated_workspace :
        print (uuid, "\t", "\t".join(ws[uuid]))
    print ('-----------')
    print ('Nombre de session R ouvertes : ', len(sessions))
    print ('Liste des sessions R ouvertes sans workspace associé (le plus petit pid correspond au serveur principal R): ')
    print ("#pid\tstatus\tdate")
    for (pid, session) in sessions.items() :
        if (pid not in associated_session) :
            print ("\t".join(session))

    if (killr and killr>0)  :
        rep = input("Ouch , t'es sure ?!! Tu veux tuer des sessions de plus de "+str(killr)+" jours ? (y/n)").strip().lower()
        if rep in ['yes', 'y']:
            killrsession(killr, args.without_docker, verbose)
        elif rep in ['no', 'n']:
            exit()
        else:
            exit()

    if (rmwork and rmwork>0)  :
        rep = input("Ouch ! Tu veux supprimer les workspaces non accédé depuis plus de "+str(rmwork)+" jours, tu es sure (si docker lancer en sudo) ? (y/n)").strip().lower()
        if rep in ['yes', 'y']:
            rmworkspace(rmwork, directory, ws,verbose)
        elif rep in ['no', 'n']:
            exit()
        else:
            exit()

    if (not killr and not rmwork):
        print ('\nCommande supprimer les workspaces ouvert depuis plus de 30 jours:')
        print ("\t > python3 asterics_session.py --rmwork 30\n")
        if len(sessions) > 1:
            print ('Commande pour fermer les sessions ouvert depuis plus de 7 jours:')
            print ("\t > python3 asterics_session.py --killr 7\n")
