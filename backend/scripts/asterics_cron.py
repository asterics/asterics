#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import os
import shutil
from flask import Flask, current_app
from flask_mail import Mail
from backend.asterics.workspace import Workspace
from backend.asterics.utils import sendEmail
import subprocess
import socket

app = Flask(__name__)
app.config.from_pyfile('../../application.cfg', silent=False)
app.mail = Mail(app)

def cleanStorageDir(public_url):
    with app.app_context():
        for uuid in os.listdir(current_app.config["STORAGE_DIR"]):
            if uuid != "tmp":
                workspace = Workspace.get_workspace(uuid, mode="control")
                remaining_days = workspace.getRemainingDays()
                email_days = int(current_app.config["STORAGE_EMAIL_DAYS_BEFORE_DELETION"])
                if remaining_days == 0:
                    shutil.rmtree(workspace.directory)
                    print ("Workspace " + uuid + ": deleted")
                elif remaining_days == email_days or remaining_days == 1:
                    # Send email to user
                    body = '''Dear user,

Your workspace {} will be deleted in {} day(s), unless you connect to it.

Thank you for using ASTERICS,
The ASTERICS Team,
'''.format(public_url + 'workspace/' + uuid, str(remaining_days))
                    sendEmail('Workspace deleted in ' + str(remaining_days) + ' day(s)', body, [workspace.getUserEmail()])
                    print ("Workspace " + uuid + ": " + str(remaining_days) + ' remaining days (email sent).')
                else:
                    print ("Workspace " + uuid + ": " + str(remaining_days) + ' remaining days.')

def killRsessions() :
    nbdays = 7
    cmd = "ps -o etime,pid,stime,command --no-headers -C Rserve |awk '{if ($1>=" + nbdays + ") {print $1 \" \" $2 }}' FS='-'| tail -n +2 |awk ' {print $3 }' FS=' ' | xargs kill"
    runcmd(cmd, verbose = True)

def runcmd(cmd, verbose = False, *args, **kwargs):

    process = subprocess.Popen(
        cmd,
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
        text = True,
        shell = True
    )
    std_out, std_err = process.communicate()
    if verbose:
        print(std_out.strip(), std_err)
    return std_out.strip()

if __name__ == '__main__':

    public_url = app.config['PUBLIC_URL']
    # Clean directories
    cleanStorageDir(public_url)
    # Close unused session since 1 day
    url = public_url + "/api/close_old_session"
    runcmd('wget --no-check-certificate ' + url , verbose = True)
    if (os.path.exists("close_old_session")):
        runcmd('rm close_old_session', verbose = True)
    #Remove tmp directories
    tmp_dir = os.path.join(app.config["STORAGE_DIR"], 'tmp') 
    runcmd('find ' + tmp_dir + ' -type f -atime +1 -delete', verbose = True)
    #Kill old Rsession, not linked to a workspace
    killRsessions()
