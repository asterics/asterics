import os
import pyRserve

from flask import current_app
from NamedAtomicLock import NamedAtomicLock
import time
class SessionInUse(Exception):
    def __init__(self, message):      
        super().__init__(message)      
    pass

class RSession (object):

    RSESSION_FILE = 'rsession.Rdata'
    
    def __init__(self, uid):
        self.uid = uid
        self.wd = os.path.join(current_app.config['STORAGE_DIR'], uid)
        self.lock = NamedAtomicLock("lock", lockDir=self.wd, maxLockAge=120)
        self.lock.release( forceRelease=True)
        self.open_Rconnexion()
        self.processId = self.getProcessId()

    def open_Rconnexion(self):
        self.conn = pyRserve.connect(host=current_app.config['RSERVE_HOST'], port=int(current_app.config['RSERVE_PORT']) )
        
    def load_session(self):
        rsession_file= os.path.join(self.wd, RSession.RSESSION_FILE)
        if os.path.isfile(rsession_file):
            if self.lock.acquire():
                try :
                    self.conn.ref.r_upgrade_rsession(inputFile=rsession_file)
                    self.conn.ref.load(file=rsession_file)
                finally :
                    self.lock.release()
            else:
                raise (SessionInUse("Please retry later, Rsession is already in use for "+ self.uid))
    
    def run_r_function (self, func_name, kwargs):
        if self.acquire():
            if self.conn.isClosed : 
                self.open_Rconnexion()
                self.load_session()
            try :
                self.conn.ref.setwd(self.wd)
                res = self.conn.ref.r_wrapp(func_name, **kwargs)
            finally : 
                self.lock.release() 
        else :
            # create temporary connexion :
            raise (SessionInUse("Please retry later, Rsession is already in use for "+ self.uid))
        return res

    def close_session (self):
        if self.acquire():
            try :
                if not self.conn.isClosed:
                    self.conn.close()
            finally : 
                self.lock.release()          
    
    def getProcessId(self):
        self.conn.r("processId <- Sys.getpid()")
        return str(self.conn.r.processId)

    def get_content (self):
        if self.acquire():
            try :
                res = self.conn.ref.ls().tolist()
            finally : 
                self.lock.release()
            return res
        else :
            raise (SessionInUse("Please retry later, Rsession is already in use for "+ self.uid))
    
    def acquire (self):
         #timeout 5s
        endTime = time.time() + 5
        pollTime = 1
        keepGoing = lambda : bool(time.time() < endTime)
        success = False
        while keepGoing():
            if not self.lock.isHeld:
                self.lock.acquire()
                success = True
                break
            else:
                time.sleep(pollTime)
        return success
