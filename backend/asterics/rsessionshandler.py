from .rsession import RSession

class RSessionsHandler (object):

    __sessions = {}
  
    def get_sessions_keys(self):
        return list(self.__sessions.keys())
    
    def get_session(self, uid):
        if uid not in self.__sessions:
            self.__sessions[uid] = RSession(uid)
            self.__sessions[uid].load_session()
        return self.__sessions[uid]
    
    def close_session(self, uid):
        if uid in self.__sessions:
            self.__sessions[uid].close_session()
            del self.__sessions[uid]
