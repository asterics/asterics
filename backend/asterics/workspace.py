import os
import tempfile
import configparser
import datetime
from shutil import copyfile
from NamedAtomicLock import NamedAtomicLock

from flask import current_app
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
from .rsession import RSession


class Workspace (object):

    USER_FILE = "workspace.txt"
    DATASETS_DIR = 'datasets'
    def __init__(self, uid):
        self.uid = uid
        self.directory = os.path.join(current_app.config["STORAGE_DIR"], uid)
        if (not os.path.isdir(self.directory)):
            os.makedirs(self.directory, exist_ok=True)
        self.lock = NamedAtomicLock("lock", lockDir=self.directory, maxLockAge=120)

    def add_file(self, file):
        datasets_dir = os.path.join(self.directory, Workspace.DATASETS_DIR)
        if (not os.path.isdir(datasets_dir)):
            os.makedirs(datasets_dir, exist_ok=True)
        filename=""
        # save raw file 
        if type(file) is FileStorage :
            filename = secure_filename(file.filename)
            file.save(os.path.join(datasets_dir, filename))
        else : 
            # demo files         
            filename = os.path.basename(file)
            copyfile (file,os.path.join(datasets_dir, filename))
        return filename
    
    def add_dataset(self, file, dataset, kwargs):
        datasets_dir = os.path.join(self.directory, Workspace.DATASETS_DIR)
        # import file in rsession
        kwargs["sep"] = kwargs["sep"].replace("\\t", "\t")
        kwargs["data.name"] = dataset["name"]
        kwargs["input"] = os.path.join(datasets_dir, file)
        rsession = current_app.rsessions.get_session(self.uid)
        result = rsession.run_r_function('r_import', kwargs)
        return result[0]

    def run_r_function(self, func_name, **kwargs):
        datasets_dir = os.path.join(self.directory, Workspace.DATASETS_DIR)
        final_kwargs = {}
        new_arg = None
        for arg in kwargs:
            if arg.endswith('_2file'):
                data = kwargs[arg]
                new_arg = arg.split('_')[0]
                final_kwargs[new_arg] = Workspace.suffix_2file(data)
            else:
                final_kwargs[arg] = kwargs[arg]
        if (func_name == "r_import"):
            # add directory to datasets files
            final_kwargs["input"] = os.path.join(datasets_dir, kwargs["input"])
            final_kwargs["sep"] = final_kwargs["sep"].replace("\\t", "\t")
        rsession = current_app.rsessions.get_session(self.uid)
        result = rsession.run_r_function(func_name, final_kwargs)
        if (result) :
            if "error" in result:
                return "workspace.run_r_function error: " + func_name + " ; " + result["error"]
            else :
                return result[0]
        else : 
            return "workspace.run_r_function on " + func_name + " return None object."

    def close(self):
        # close session
        current_app.rsessions.close_session(self.uid)

    def updateLastConnection(self):
        if self.lock.acquire(timeout=0.1):
            now = datetime.datetime.now()
            config = configparser.ConfigParser()
            config.read(os.path.join(self.directory, Workspace.USER_FILE))
            config['USER']['last.connection'] = str(now)
            rsession = current_app.rsessions.get_session(self.uid)
            config['USER']['last.processid'] = rsession.processId
            with open(os.path.join(self.directory, Workspace.USER_FILE), 'w') as configfile:
                config.write(configfile)
            self.lock.release()

    def getRemainingDays(self):
        now = datetime.datetime.now()
        max_days = int(current_app.config["STORAGE_MAX_DAYS"])
        config = configparser.ConfigParser()
        config.read(os.path.join(self.directory, Workspace.USER_FILE))
        try :
            last_connexion = datetime.datetime.strptime(config['USER']['last.connection'], '%Y-%m-%d %H:%M:%S.%f')
        except :
            print ("getRemainingDays: Unable to retrieve USER->last.connection, return 30 for workspace ", self.directory)
            return 0
        delta = now - last_connexion
        if max_days <= delta.days:
            return 0
        else :
            return max_days - delta.days
    
    def getDelayLastConnection(self):
        now = datetime.datetime.now()
        config = configparser.ConfigParser()
        config.read(os.path.join(self.directory, Workspace.USER_FILE))
        try :
            last_connection = datetime.datetime.strptime(config['USER']['last.connection'], '%Y-%m-%d %H:%M:%S.%f')
        except :
            print ("getDelayLastConnection: Unable to retrieve USER->last.connection, return 0 for workspace ", self.directory)
            return 0
        delta = now - last_connection
        return delta.days
       

    def getUserEmail(self):
        config = configparser.ConfigParser()
        config.read(os.path.join(self.directory, Workspace.USER_FILE))
        return config['USER']['email']

    @staticmethod
    def create_workspace(uid, email):
        wspace = Workspace(uid)
        config = configparser.ConfigParser()
        now = datetime.datetime.now()
        config['USER'] = {
            'email': email,
            'first.connection': str(now),
            'last.connection': str(now),
        }
        with open(os.path.join(wspace.directory, Workspace.USER_FILE), 'w') as configfile:
            config.write(configfile)
        return wspace

    @staticmethod
    def get_workspace_uuids():
        if os.path.isdir(current_app.config["STORAGE_DIR"]):
            return os.listdir(current_app.config["STORAGE_DIR"])
        else:
            return []

    @staticmethod
    def get_workspace(uid, mode="access"):
        #Mode possible : [access|control] 
        #control mode do not updage lastconnexion
        wspace = Workspace(uid)
        if mode != "control" :
            wspace.updateLastConnection()
        return wspace

    @staticmethod
    def get_tempfile():
        tempdir = os.path.join(current_app.config["STORAGE_DIR"], "tmp")
        if (not os.path.isdir(tempdir)):
            os.makedirs(tempdir, exist_ok=True)
        return tempfile.NamedTemporaryFile(mode="w", dir=tempdir, delete=False)

    @staticmethod
    def suffix_2file (data):
        fh = Workspace.get_tempfile()
        fh.write(data)
        fh.close()
        return fh.name
