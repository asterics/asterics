---
author: From ASTERICS web application
params: 
    title: NULL
    date: "07/12/2020"
    outanalysis: NULL
    workingdir: "/tmp"
title: |
  `r params$title` 
date: "`r params$date`"
output: 
    html_document:
        css: "astericsreport.css"
        toc: true
        toc_float: true
        df_print: paged
        number_sections: true
        self_contained: true
---

----------------------------------------------------------------------

```{r, include=FALSE}
# Ne change rien à la hauteur des images plotly !
rmddir <- getwd()
knitr::opts_chunk$set(out.width=10, out.height=12, fig.width=10, fig.height=12, warning = FALSE, message = FALSE) 
knitr::opts_knit$set(root.dir = params$workingdir)
```


```{r htmlTemplate, echo=FALSE, results='asis'}
img <- knitr::image_uri(paste0(rmddir, "/LogoAsterics.png"))

htmlhead <- paste0("
<style>
#TOC {
  background: url(", img, ");
  background-size: contain;
  padding-top: 100px !important;
  background-repeat: no-repeat;
}
</style>")
cat(htmlhead)
```

```{r libs, echo = FALSE}
library(ggplot2)
```

```{r, include=FALSE}
# Init Step to make sure that the dependencies are loaded
htmltools::tagList(DT::datatable(cars[1:2,1:2]))
```

```{r getanalysis, include=FALSE, message=FALSE, echo=FALSE}
namecurrent <- params$outanalysis
```

```{r workflow, echo=FALSE, fig.width=8, fig.height=4}
graph_db <- r_extract_wgraph()
ws <- r_print_workspace(graph_db, namecurrent)
if(!is.null(ws)) ws
```


```{r, echo=FALSE}
all_names <- sapply(graph_db$data$nodes, function(x) x$object_name)
current_id <- which(all_names == namecurrent)
params_outputs <- NULL
if(!is.null(graph_db$data$nodes[[current_id]]$savedPlots))
  params_outputs <- graph_db$data$nodes[[current_id]]$savedPlots

# extracting the dataset
datasets_outputs <- sapply(params_outputs, function(x) x[["func_args"]][1])
datasets_outputs <- unlist(datasets_outputs)

# Sorting the outputs by dataset (in order the dataset appear in object_db)
order_datasets <- sort(sapply(object_db, function(x) x$id))
order_datasets <- order_datasets[names(order_datasets) %in% datasets_outputs]
order_datasets <- order_datasets[datasets_outputs]
params_outputs <- params_outputs[order(order_datasets)]
datasets_outputs <- datasets_outputs[order(order_datasets)]
datasets_outputs <- sapply(datasets_outputs, r_get_username)

# getting results for each output. 
saved_outputs <- lapply(params_outputs, function(x) r_run_function(x$function_name, x$func_args))
```

```{r outplotsCurrent, results='asis', echo = FALSE}
previous_dataset <- ""
if(length(params_outputs) > 0){
  for(i in seq_len(length(params_outputs))){
    current_dataset <- datasets_outputs[i]
    cat("\n\n")
    cat("\n\n")
    cat("\n\n")
    # If changing of dataset, print his user_name. 
    if(current_dataset != previous_dataset){
      cat(paste("# Dataset: ", datasets_outputs[i], "\n\n"))
    }
    cat(paste("## ", names(params_outputs)[i], "\n\n"))
    
    # Plots
    # Il faut enlever le niveau Graphical
    ggplots <- sapply(saved_outputs[[i]]$Graphical, is, "ggplot")
    saved_outputs[[i]]$Graphical[ggplots] <- lapply(saved_outputs[[i]]$Graphical[ggplots], plotly::ggplotly)
    if(length(saved_outputs[[i]]$Graphical) > 0){
      cat("\n")
      cat("### Graphical\n\n")
      cat("\n")
      r_print_graphs(saved_outputs[[i]]$Graphical)
    }
    # Table
    if(length(saved_outputs[[i]]$Table) > 0){
      cat("\n\n")
      cat("### Numerical results\n\n")
      print(r_print_tables(saved_outputs[[i]]$Table,
                 title = NULL))
      cat("\n\n")
    }
    
    # Asterics version for the saved output
    asterics_version <- params_outputs[[i]]$asterics_code_version
    if(!is.null(asterics_version) && asterics_version != "Unknown"){
      url_asterics <- "https://forgemia.inra.fr/asterics/asterics/-/releases"
      cat(paste0("Produced with ASTERICS [version ",
                  asterics_version, 
                  "](", url_asterics, ")."))
      cat("\n\n")
    }
    
    previous_dataset <- current_dataset
  }
}
```


```{r, echo=FALSE, messages=FALSE, warning=FALSE}
# Solution for ggplot found here :
# https://stackoverflow.com/questions/49990653/plotly-plot-doesnt-render-within-for-loop-of-rmarkdown-document
# Attach the Dependencies since they do not get included with renderTags(...)$html
allplots <- Map(function(x) x$Graphical, saved_outputs)
allplots <- lapply(allplots, function(x){
  if(is(x[[1]], "plotly")) {
    return(x[[1]])
  } else {
    return(NULL)
  }
})
depsAllPlots <- NULL
if(length(allplots) > 0){# If any plotly graphs
  depsAllPlots <- lapply(
    Filter(f = function(x){inherits(x,"htmlwidget")}, x = allplots),
    function(hw){
      htmltools::renderTags(hw)$dependencies
    }
  )
}
```

```{r html_dependencies, echo=FALSE}
if(!is.null(depsAllPlots))
  htmltools::attachDependencies(x = htmltools::tagList(), value = unlist(depsAllPlots, recursive=FALSE))
```


