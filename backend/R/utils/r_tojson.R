#' jsonification of objects
#'
#' @title jsonification of objects
#' @description Takes an object and returns the object with elements that should
#' be printed on the interface in json version
#'
#' @param object | R object | data.frame or R list with entries within
#' Graphical, Numerical, Table and Object | required | NULL | R object for input
#' that can be a data frame (in which case, the jsonification is performed on
#' the object itself) or a complex objects with subentries in 'Numerical',
#' 'Graphical', 'Table' and 'Object' that require different jsonifications |
#' @param pretty | logical | Should the JSON output of a data.frame be
#' made pretty? | optional | FALSE | Should indentation and whitespaces be added
#' to the JSON output of a data.frame? If object is not a data.frame, this
#' option is ignored
#'
#' @examples
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1,
#'          assign = TRUE)
#' r_tojson(proteins)
#' proteins <- as.matrix(proteins)
#' r_tojson(proteins) ## Note: approximately twice faster when taken from a matrix
#'
#' # numerical variable
#' proteins <- as.data.frame(proteins)
#' out_univariate_num <- r_univariate(proteins, "14.3.3_epsilon")
#' r_tojson(out_univariate_num)
#'
#' # dataset description
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
#' desc_clinical <- r_summary_description("clinical", 1, 5, 2, 10)
#' r_tojson(desc_clinical)
#'
#' # with missing values
#' clinical_miss <- clinical
#' idx_missing <- runif(nrow(clinical) * ncol(clinical)) <= 0.2
#' dim(idx_missing) <- c(nrow(clinical), ncol(clinical))
#' clinical_miss[idx_missing] <- NA
#' desc_clinical_miss <- r_summary_description("clinical_miss", 1, 4, 1, 3)
#' r_tojson(desc_clinical_miss)
#'
#' @importFrom jsonlite toJSON
#' @importFrom plotly plotly_json

r_tojson <- function(object, pretty = FALSE) {
  if(is.data.frame(object) || is.matrix(object)) {
    json_out <- r_create_dataview(object, rstart = 1, rend = nrow(object),
                                  cstart = 1, cend = ncol(object))
    json_out$type <- "DataFrame"
    json_out$title <- "View of a data.frame"
    json_out <- jsonlite::toJSON(json_out, na="string", auto_unbox=TRUE, pretty = pretty)
  } else {
    json_g <- NULL
    json_t <- NULL
    json_m <- NULL
    idx_plotly <- integer()
    
    if("Graphical" %in% names(object)) {
      json_g <- json_g0 <- lapply(object$Graphical, r_tojson_graph)
      idx_plotly <- which(sapply(object$Graphical, function(x) is(x, "ggplot") || is(x, "plotly")))
      json_g[idx_plotly] <- lapply(json_g[idx_plotly], function(x) list(type = "PlotLy", data = c()))
    }
      
    if ("Table" %in% names(object))
      json_t <- object$Table
    
    if("Messages" %in% names(object))
      json_m <- object$Messages
    
    json_out <- c(json_g, json_t, json_m)
    if(is.null(json_out)) return(NULL)
    
    json_out <- jsonlite::toJSON(json_out, na="string", auto_unbox=TRUE, pretty = pretty, dataframe = "rows", rownames=TRUE)
    
    for (i in idx_plotly) {
      char_idx <- stringi::stri_locate_first_fixed(str = json_out, pattern = paste0('"', names(object$Graphical)[i], '":{"type":"PlotLy","data":{}}'))
      json_out <- stringi::stri_sub_replace(str = json_out, from = char_idx[2] - 2, to = char_idx[2] - 1, value = json_g0[[i]])
      class(json_out) <- "json"
    }
  }
  
  return(json_out)
}

r_tojson_graph <- function(graph) {
  if (is(graph, "ggplot") | is(graph, "plotly")) {
    graphlist <- suppressWarnings(plotly::plotly_json(graph, jsonedit = FALSE,
                                          pretty = FALSE))
  } else if (graph$type == "png") {
    graphlist <- graph
  } else if (attr(graph, "type") == "Upset") {
    graphlist <- graph
    # Necessary for the auto_unbox = TRUE ? ELISE : for the force = TRUE apparently.
    # Try to find a better way when upsetJS handled by the interface.
    # graphlist$data <- jsonlite::toJSON(graphlist$data, force = TRUE,
    #                                    auto_unbox = TRUE, pretty = FALSE)
    # graphlist$data <- jsonlite::fromJSON(graphlist$data)
  } else if(!is.null(attr(graph, "type")) && attr(graph, "type") == "Venn") {
    graphlist <- graph
  } else {
    stop("Wrong type!")
  }

  # graphlist <- jsonlite::toJSON(graphlist, na = "string")
  return(graphlist)
}
