#' Call object DB to retrieve a list of objects from their attributes
#' 
#' @title Call object DB
#' @description Call object DB to retrieve a list of objects from their 
#' attributes.
#'
#' @param object.name | Object name | character | optional | NULL | Name of the
#' object to be compared to attribute "object_name". If set, the other arguments
#' are ignored
#' @param user.name | Object user name | character | optional | NULL | Name of
#' the object as given by the user to be compared to attribute "user_name". . If 
#' set, the other arguments are ignored
#' @param nature | Object nature | character | optional | NULL | nature of the 
#' objects to retrieve
#' @param origin.dataset | Parent dataset | character | optional | NULL | 
#' Dataset from which this object originates
#' @param parent.edge | Parent in DAG | character | optional | NULL | Dataset(s)
#' or analysis(es) that is(are) considered as a parent(s) of the object in the 
#' DAG
#' @param type | Object type | character | optional | NULL | Object type
#' (between "analysis", "dataset" and "hidden")
#' @param func.name | Name of the function that created the object | character |
#' optional | NULL | Name of the function that created the object
#' @param func.args | Arguments of the function that created the object | list |
#' optional | NULL | Arguments of the function that created the object
#'
#' @return NULL if no object in the DB satisfies the criteria. Otherwise, the
#' function returns the elements of `object_db` that describe the objects 
#' corresponding to the criteria 
#' 
#' @examples 
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", header = TRUE, sep = " ",
#'          quote = "\"", dec = ".", row.names = 1, assign = TRUE)
#' called_args <- list(datasetName = "proteins", ncp = 5, pca = T)
#' r_call_datadb(func.name = "r_famd", func.args = called_args)
#' out_pca <- r_wrapp("r_famd", "proteins", ncp = 5, pca = T)
#' r_call_datadb(func.name = "r_famd", func.args = called_args)
#' r_call_datadb(object.name = "PCAobj_1")
#' r_call_datadb(user.name = "PCAobj_1")

r_call_datadb <- function(object.name = NULL, user.name = NULL, nature = NULL, 
                          origin.dataset = NULL, parent.edge = NULL, 
                          type = NULL, func.name = NULL, func.args = NULL,
                          groups = NULL, logt = NULL, normalized = NULL) {
  
  cur_ls <- ls(envir = .GlobalEnv)
  if (!("object_db" %in% cur_ls)) return(NULL)
  
  object_db <- get("object_db")
  
  if (!is.null(object.name)) {
    message("'object.name' provided: database called with this argument only")
    all_objectnames <- sapply(object_db, "[[", "object_name")
    selected <- object.name %in% all_objectnames
    if (selected) {
      out <- object_db[all_objectnames == object.name]
      return(out)
    } else return(NULL)
  }
  
  if (!is.null(user.name)) {
    message("'user.name' provided: database called with this argument only")
    all_usernames <- sapply(object_db, "[[", "user_name")
    selected <- user.name %in% all_usernames
    if (selected) {
      out <- object_db[all_usernames == user.name]
      return(out)
    } else return(NULL)
  }
  
  selected <- rep(TRUE, length(object_db))
  
  if (!is.null(nature)) {
    all_natures <- sapply(object_db, "[[", "nature")
    selected <- selected & (all_natures == nature)
  }
  
  if (!is.null(origin.dataset)) {
    all_origindatasets <- lapply(object_db, "[[", "origin_dataset")
    all_origindatasets <- lapply(all_origindatasets, sort)
    cur_sel <- sapply(all_origindatasets, 
                      function(alist) identical(alist, sort(origin.dataset)))
    selected <- selected & cur_sel
  }
  
  if (!is.null(parent.edge)) {
    all_parent_edges <- lapply(object_db, "[[", "parent_edge")
    all_parent_edges <- lapply(all_parent_edges, sort)
    cur_sel <- sapply(all_parent_edges,
                      function(alist) identical(alist, sort(parent.edge)))
    selected <- selected & cur_sel
  }
  
  if (!is.null(type)) {
    all_types <- sapply(object_db, "[[", "type")
    selected <- selected & (all_types == type)
  }
  
  if (!is.null(func.name)) {
    all_funcnames <- sapply(object_db, "[[", "func_name")
    selected <- selected & (all_funcnames == func.name)
  }
  
  if (!is.null(func.args)) {
    all_funcargs <- lapply(object_db, "[[", "func_args")
    cur_sel <- sapply(all_funcargs, 
                      function(alist) isTRUE(all.equal(alist, func.args)))
    selected <- selected & cur_sel
  }
  
  if (!is.null(groups)) {
    all_groups <- lapply(object_db, "[[", "groups")
    all_groups <- lapply(all_groups, sort)
    cur_sel <- sapply(all_groups, 
                      function(alist) identical(alist, sort(groups)))
    selected <- selected & cur_sel
  }
  
  if (!is.null(logt)) {
    all_logs <- sapply(object_db, "[[", "logt")
    selected <- selected & (all_logs == logt)
  }
  
  if (!is.null(normalized)) {
    all_normalized <- sapply(object_db, "[[", "normalized")
    selected <- selected & (all_normalized == log)
  }
  
  if (sum(selected) > 0) {
    out <- object_db[selected]
  } else return(NULL)
  
  return(out)
}
