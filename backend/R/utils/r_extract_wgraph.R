#' Extraction of dataset/analysis graph
#' 
#' @title Extraction of dataset/analysis graph
#' @description This function extracts the graph to be exported as JSON from
#' `object_db`
#'
#' @return An object of type list that contains information on nodes and edges
#' for the JSON exportation
#'
#' @examples
#' ## PCA
#' out_import1 <- r_wrapp("r_import", input = "protein.csv",
#'                        data.name = "proteins", 
#'                        nature = "protein quantification", header = TRUE, 
#'                        row.names = 1, sep = " ")
#' out_import2 <- r_wrapp("r_import", input = "clinical.csv",
#'                        data.name = "clinical", nature = "phenotypes",
#'                        header = TRUE, row.names = 1, sep = ",")
#' out_pca <- r_wrapp("r_famd", "proteins", ncp = 10, pca = T)
#' out_extract <- r_wrapp("r_extract_obj", "PCAobj_1", userName = "myPCs", 
#'                        criterion = "axes", ncp = 5)
#' out_extract <- r_wrapp("r_extract_obj", "PCAobj_1", userName = "myselVars",
#'                        criterion = "correlation", ncp = 2, 
#'                        threshold.cor = 0.7)
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## FAMD
#' out_famd <- r_wrapp("r_famd", "clinical", ncp = 10)
#' out_extract <- r_wrapp("r_extract_obj", "PCAobj_2", userName = "myPCs2", 
#'                        criterion = "axes", ncp = 5)
#' out_extract <- r_wrapp("r_extract_obj", "PCAobj_2", userName = "myselVars2",
#'                        criterion = "correlation", ncp = 2, 
#'                        threshold.cor = 0.7)
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## Clustering
#' ### HAC
#' out_clustering <- r_wrapp("r_clustering", "proteins", "hac")
#' out_cutclustering <- r_wrapp("r_cut_clustering", 
#'                              "HACClustering_1", 5)
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' out_extractclustering <- r_wrapp("r_extract_dataset", "HACClusters_1",
#'                                  "myclusters")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' ### k-means
#' out_clustering <- r_wrapp("r_clustering", "proteins", method = "kmeans", 
#'                           kmin = 3, kmax = 6, seed = 3)
#' out_cutclustering <- r_wrapp("r_cut_clustering", "KmeansCut_4")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' out_extractclustering <- r_wrapp("r_extract_dataset", "KmeansClusters_1",
#'                                  "myaltclusters")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## Combined dataset
#' out_import3 <- r_wrapp("r_import", input = "mrna.csv",
#'                        data.name = "mrna", nature = "gene expression", 
#'                        header = TRUE, row.names = 1)
#' out_combined <- r_wrapp("r_combine_datasets", 
#'                         datasetNames = list("proteins", "clinical", "mrna"))
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## MFA
#' clinicalq <- clinical[, c("patient.gender", "patient.vital_status", 
#'                           "patient.age_at_initial_pathologic_diagnosis", 
#'                           "patient.day_of_form_completion")]
#' tmp <- tempfile()
#' write.table(clinicalq, file = tmp, sep = "\t")
#' out_import4 <- r_wrapp("r_import", input = tmp, data.name = "clinicalq",
#'                        nature = "generic", header = TRUE, row.names = 1,
#'                        sep = "\t")
#' system(paste("rm", tmp))
#' out_combined <- r_wrapp("r_combine_datasets", 
#'                         datasetNames = list("proteins", "clinicalq", "mrna"))
#' out_mfa <- r_wrapp("r_mfa", datasetName = "combinedDF_1")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' out_extract <- r_wrapp("r_extract_obj", "MFAobj_1", userName = "mfa_user", 
#'                        criterion = "axes", ncp = 5)
#' out_extract <- r_wrapp("r_extract_obj", "MFAobj_1", 
#'                        criterion = "correlation", ncp = 2, 
#'                        threshold.cor = 0.7)
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## SOM clustering
#' out_som <- r_wrapp("r_som", "mrna", seed = 5)
#' out_extract_som <- r_wrapp("r_extract_dataset", "SOMClusters_1")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)
#' 
#' ## SOM superclustering
#' out_supercluster <- r_wrapp("r_super_cluster", "SOMobj_1")
#' out_cut_sc <- r_wrapp("r_cut_supercluster", "SOMobj_1", 3)
#' out_extract_sc <- r_wrapp("r_extract_dataset", "SuperClusters_1")
#' out_graph <- r_extract_wgraph()
#' jsonlite::toJSON(out_graph, auto_unbox = TRUE, pretty = TRUE)

r_extract_wgraph <- function() {
  cur_ls <- ls(envir = .GlobalEnv)

  if (!("object_db" %in% cur_ls)) {
    Output <- list("type" = "Workflow", 
                   "data" = list("nodes" = NULL, "edges" = NULL))
    return(Output)
  }

  # If no saved plots
  if (!("outplots_db" %in% cur_ls)) {
      outplots_db <- list()
  } else {
      outplots_db <- get("outplots_db")
  }
  
  # remove hidden nodes
  object_db <- get("object_db")
  all_types <- sapply(object_db, `[[`, "type")
  simplified_db <- object_db[all_types != "hidden"]
  if (length(simplified_db) == 0) {
    Output <- list("type" = "Workflow", 
                   "data" = list("nodes" = NULL, "edges" = NULL))
    return(Output)
  }

  all_nodes <- sapply(1:length(simplified_db), r_extract_nodes, 
                      db = simplified_db, simplify = FALSE)
  all_ids <- sapply(all_nodes, "[[", "id")
  all_names <- sapply(all_nodes, "[[", "object_name")
  
  all_parent_edges <- lapply(simplified_db, "[[", "parent_edge")
  all_parent_edges <- lapply(all_parent_edges, unlist)
  all_parent_edges <- lapply(all_parent_edges, unique)
  
  selected <- sapply(all_parent_edges, function(alist) 
    return((length(alist) == 1 && alist == "none")))
  if (sum(!selected) > 0) {
    selected <- sapply(all_parent_edges, function(alist) !("none" %in% alist))
    targets <- rep(all_ids[selected], 
                   sapply(all_parent_edges[selected], length))
    
    sources <- sapply(all_parent_edges[selected], 
                      function(alist) match(alist, all_names))
    sources <- unlist(sources)
    
    all_edges <- cbind(all_ids[sources], targets)
  } else all_edges <- NULL
  
  # saved plots
  all_nodes <- lapply(all_nodes, 
                        function(x) {
                          name <- x$object_name
                          x$savedPlots <- list()
                          return(x)
                        })
  if(length(outplots_db) > 0){
      all_nodes <- lapply(all_nodes, 
                          function(x) {
                              name <- x$object_name
                              if(length(outplots_db[[name]])>0){
                                  tmp <- outplots_db[[name]]
                                  names(tmp) <- sapply(tmp, function(x) x[["object_name"]])
                                  tmp <- lapply(tmp, function(x) return(x[c("function_name", "func_args", "asterics_code_version")]))
                                  x$savedPlots <- tmp
                              }
                              return(x)
                          })
  }
  
  all_nodes_for_print <- lapply(all_nodes, transform_for_print)
  Output <- list("type" = "Workflow",
                 "data" = list("nodes" = all_nodes_for_print, "edges" = all_edges))
  
  return(Output)
}

r_extract_nodes <- function(id, db) {
  # common information: id, object.name, user.name
  sel_fields <- c("id", "object_name", "user_name", "origin_dataset", "type", 
                  "groups")
  Output <- db[[id]][sel_fields]
  
  # meta for 'dataset'
  if (Output$type == "dataset") {
    selected_infos <- c("nature" = db[[id]]$nature, 
                        "class" = db[[id]]$dataset_info$class, 
                        db[[id]]$dataset_info$meta)
    Output$meta <- as.list(selected_infos)
  } else if (grepl("^editor", Output$object_name)) {
    Output$meta <- db[[id]][c("nature", "func_name")]
    Output$meta[["func_args"]] <- get(db[[id]]$object_name)$history
    Output$meta[["func_args"]][c("datasetName", "retrieve")] <- db[[id]][["func_args"]][c("datasetName", "retrieve")]
  } else if(Output$type != "savedPlots"){ # meta for 'analysis'
    Output$meta <- db[[id]][c("nature", "func_name", "func_args")]
  } else {
    Output$meta <- list()
  }
  
  return(Output)
}

transform_args <- function(args){
    # dataset, datasetName, datasetNames, dataset1, dataset2, X, Y, datasetRowColors
    index_datasets <- c(grep("^dataset", names(args)), which(names(args) %in% c("X", "Y")))
    if(length(index_datasets) > 0){
        args[index_datasets] <- 
          lapply(args[index_datasets], function(x) sapply(x, r_get_username, USE.NAMES = FALSE))
    }
    return(args)
}

# Transform arguments of function to print on the interface. (putting user_name instead of object_name)
# Add a component at the same level than func_args: func_args_for_print
transform_for_print <- function(onenode){
    # Transform meta func_args for print
    if(!is.null(onenode$meta) && !is.null(onenode$meta$func_args)){
        args <- onenode$meta$func_args
        onenode$meta$func_args_for_print <- transform_args(args)
    }
    
    # Transform savedPlots func_args for print (here done in place (not used directly by the interface))
    if(!is.null(onenode$savedPlots) && length(onenode$savedPlots) > 0){
        onenode$savedPlots <- lapply(onenode$savedPlots, 
                                     function(x){
                                         x$func_args_for_print <- transform_args(x$func_args)
                                         return(x)
                                     })
    }
    
    # Return transformed node
    return(onenode)
}
