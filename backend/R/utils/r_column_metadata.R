#' @title Extract names of dataset variables
#' @description Extract names of the variables in the input dataset
#'
#' @param dataset | Dataset name | character | required | NULL | Input dataset
#' from which variable names are to be extracted | 
#'
#' @return a character vector of column names
#' @export
#'
#' @examples
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", header = TRUE, sep = " ", 
#'          quote = '\"', dec = ".", row.names = 1, assign = TRUE)
#' vn <- r_column_metadata("proteins")

r_column_metadata <- function(dataset) {
  df <- get(dataset)
  result <- data.frame("name" = colnames(df),
                       "numeric" = sapply(df, is.numeric),
                       "uniquevals" = sapply(df, function(x) length(unique(x))))
  result$alldistinct <- ifelse(result$uniquevals == nrow(df), "yes", "no")
  return(result)
}
