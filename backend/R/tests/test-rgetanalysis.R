input <- "../data/protein.csv"
out_import <- r_wrapp("r_import", input = input, data.name = "proteins", 
                      row.names = 1, sep = " ")
out_pca <- r_wrapp("r_famd", "proteins", ncp = 10, pca = T)

test_that("r_get_analysis behaves as expected", {
  output_object <- r_get_analysis("PCAobj_1")
  expect_named(output_object, c("Graphical", "Table", "Messages"))
})

rm(proteins, inherits = TRUE)
rm(PCAobj_1, inherits = TRUE)
rm(object_db, envir = .GlobalEnv)
rm(num_obj, envir = .GlobalEnv)
