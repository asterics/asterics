input <- "../data/protein.csv"
r_import(input, data.name = "proteins", header = TRUE, sep = " ",
         quote = '\"', dec = ".", row.names = 1, assign = TRUE)
input <- "../data/clinical.csv"
r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)

test_that("r_common_rownames behaves as expected", {
  rn <- r_common_rownames(list(rownames(clinical), rownames(proteins)))
  expect_true(is.character(rn))
  expect_true(is.vector(rn))
})