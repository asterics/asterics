cur_env <- ls(envir = globalenv())
withr::defer(expr = rm(list = setdiff(ls(envir = globalenv()), cur_env), envir = globalenv()),
             envir = environment())

input <- "../data/protein.csv"
r_wrapp("r_import", input, data.name = "proteins", header = TRUE, sep = " ",
         quote = '\"', dec = ".", row.names = 1)

tmp_file <- tempfile("proteins2", tmpdir = ".", fileext = ".csv")
write.table(proteins[1:30, 1:15], file = tmp_file, sep = ",")
r_wrapp("r_import", tmp_file, data.name = "proteins2", row.names = 1, header = TRUE,
         sep = ",")
unlink(tmp_file)

input <- "../data/clinical.csv"
r_wrapp("r_import", input, data.name = "clinical", row.names = 1)

check_graph <- function(graph_db, object_db = NULL) {
  
  # get object_db
  if (is.null(object_db)) {
    if (exists("object_db", envir = globalenv())) {
      object_db <- get("object_db", envir = globalenv())
    } else {
      return(FALSE)
    }
  }
  
  # handle 0-length case
  if (length(object_db) == 0) {
    res <- length(graph_db$data$nodes) == 0
    return(res)
  }
  
  # get some info from both DBs
  N <- graph_db$data$nodes
  objects_ids <- sapply(object_db, '[[', "id")
  graph_ids <- sapply(N, '[[', "id")
  graph_ids_matched <- match(graph_ids, objects_ids)
  graph_types <- sapply(N, '[[', "type")
  object_types <- sapply(object_db, '[[', "type")
  
  # check nodes
  res_1 <- TRUE
  res_1 <- res_1 && !anyNA(graph_ids_matched)
  res_1 <- res_1 && !any(graph_types == "hidden")
  res_1 <- res_1 && length(N) == sum(object_types != "hidden")
  
  if (res_1) {
    for (i in seq_along(N)) {
      
      j <- graph_ids_matched[i]
      fields <- intersect(names(N[[i]]), names(object_db[[j]]))
      res_1 <- res_1 && identical(N[[i]][fields], object_db[[j]][fields])
      res_1 <- res_1 && setequal(names(N[[i]]), c("id", "object_name", "user_name", 
                                                  "origin_dataset", "type", 
                                                  "groups", "meta", "savedPlots"))
      
      if (N[[i]]$type == "dataset") {
        res_1 <- res_1 && all(N[[i]][["meta"]][c("logt", "normalized")] %in% c("yes", "no", "nc"))
      }
      if (N[[i]]$type != "savedPlots") {
        res_1 <- res_1 && !is.null(N[[i]]$meta$nature)
      }
      
    }
  }
  if (!res_1) {
    return(FALSE)
  }
  
  # check plots
  P <- lapply(N, '[[', "savedPlots")
  names(P) <- sapply(N, '[[', "object_name")
  P <- P[sapply(P, length) > 0]
  P <- lapply(P, unname)
  if (length(P) > 0) {
    P0 <- lapply(get("outplots_db"), function(x) lapply(x, '[', c("function_name", "func_args", "asterics_code_version")))
    P0 <- lapply(P0, function(x) lapply(x, function(y) c(y, list("func_args_for_print" = transform_args(y[["func_args"]])))))
    P0 <- P0[names(P)]
    res_2 <- isTRUE(all.equal(P, P0))
    if (!res_2) {
      return(FALSE)
    }
  }

  

  # check edges
  E_matched <- data.frame(graph_db$data$edges)
  if (nrow(E_matched) == 0) {
    return(TRUE)
  }
  res_3 <- TRUE
  E_matched[, 1] <- match(E_matched[, 1], graph_ids)
  E_matched[, 2] <- match(E_matched[, 2], objects_ids)
  res_3 <- res_3 && !anyNA(E_matched)

  if (res_3) {
    for (i in seq(nrow(E_matched))) {
      
      i_source <- E_matched[i, 1]
      i_target <- E_matched[i, 2]
      res_3 <- res_3 && N[[i_source]]$object_name %in% object_db[[i_target]]$parent_edge
      
    }
  }

  # return
  return(res_3)
}

test_that("graph_db is correct after import", {
  
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after edition", {
  # Change a proteins2 nature (new hidden edge : proteins2 -> editor_1 -> edited_1)
  out_edit <- r_wrapp("r_edit_dataset", "proteins2", "set_dataset_nature", dataset_nature = "rna-count")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  # Extract edited 1 - name it proteins3 (new edge : proteins2 -> editor_1 -> proteins3)
  out_extract <- r_wrapp("r_extract_dataset", "edited_1", "proteins3")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  # Transpose proteins 2 (new hidden edge : proteins2 -> editor_2 -> edited_2)
  out_edit <- r_wrapp("r_edit_dataset", "proteins2", "transpose")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  # Return to an analysis from the analysis "more" button (click on "editor_1")
  out_edit <- r_wrapp("r_edit_dataset", "proteins2", retrieve = "edited_1")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  # From edit screen, select "edited_1" as dataset and transpose it. 
  # Create a new hidden edge : edited_1 -> editor_3 -> edited_3
  out_edit <- r_wrapp("r_edit_dataset", "edited_1", "transpose")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
})

test_that("graph_db is correct after PCA", {
  
  out_pca <- r_wrapp("r_famd", "proteins2", ncp = 10, pca = T)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_extract <- r_wrapp("r_extract_obj", "PCAobj_1", userName = "myPCs",
                         criterion = "axes", ncp = 5)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_extract <- r_wrapp("r_extract_obj", "PCAobj_1", userName = "myselVars",
                         criterion = "correlation", ncp = 2,
                         threshold.cor = 0.7)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_plotind", "PCAobj_1")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_plotind", "PCAobj_1",
                      datasetcolor = "clinical", varcolor = "patient.vital_status")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after k-means clustering", {
  
  out_clustering <- suppressWarnings(r_wrapp("r_clustering", "proteins2", method = "kmeans",
                                             kmin = 3, kmax = 6, seed = 3))
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_cutclustering <- suppressWarnings(r_wrapp("r_cut_clustering", "KmeansCut_4"))
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_extractclustering <- r_wrapp("r_extract_dataset", "KmeansClusters_1",
                                   "myaltclusters")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after SOM clustering", {
  
  ## SOM clustering
  out_som <- r_wrapp("r_som", "proteins2", seed = 5)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_extract_som <- r_wrapp("r_extract_dataset", "SOMClusters_1")
  expect_true(check_graph(graph_db = r_extract_wgraph()))

  ## SOM superclustering
  out_supercluster <- suppressWarnings(r_wrapp("r_super_cluster", "SOMobj_1"))
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_cut_sc <- suppressWarnings(r_wrapp("r_cut_supercluster", "SOMobj_1", 3))
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_extract_sc <- r_wrapp("r_extract_dataset", "SuperClusters_1")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after combining datasets", {
  
  out_combtg <- r_wrapp("r_combine_target", X = "proteins2", Y = "clinical",
                        target = "patient.vital_status")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after saving plots", {
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_bivariate", 
                      dataset1 = "PCAaxes_1", varname1 = "PC1",
                      dataset2 = "PCAaxes_1", varname2 = "PC2")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_bivariate", 
                      dataset1 = "PLSDAaxes_1", varname1 = "comp1",
                      dataset2 = "PLSDAaxes_1", varname2 = "comp2")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_multivariate_dotplot", 
                      datasetxaxis = "PCAaxes_1", varxaxis = "PC1",
                      datasetyaxis = "PCAaxes_1", varyaxis = "PC2", 
                      datasetcolor = "clinical", varcolor = "patient.vital_status")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  out_plot <- r_wrapp("r_save_for_report", function_name = "r_multivariate_dotplot", 
                      datasetxaxis = "PLSDAaxes_1", varxaxis = "comp1",
                      datasetyaxis = "PLSDAaxes_1", varyaxis = "comp2", 
                      datasetcolor = "clinical", varcolor = "patient.vital_status")
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})

test_that("graph_db is correct after deleting datasets / analyses", {
  
  del_obj <- r_check_delete(objectName = "KmeansClustering_1")
  out_delete <- r_wrapp("r_delete_object", objectList = del_obj$Table$ToDelete)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  del_obj <- r_check_delete(objectName = "multivariateDotplots")
  out_delete <- r_wrapp("r_delete_object", objectList = del_obj$Table$ToDelete)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  del_obj <- r_check_delete(objectName = names(which(sapply(object_db, '[[', "type") == "analysis")))
  out_delete <- r_wrapp("r_delete_object", objectList = del_obj$Table$ToDelete)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
  del_obj <- r_check_delete(objectName = names(object_db))
  out_delete <- r_wrapp("r_delete_object", objectList = del_obj$Table$ToDelete)
  expect_true(check_graph(graph_db = r_extract_wgraph()))
  
})
