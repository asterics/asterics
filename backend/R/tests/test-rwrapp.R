test_that("r_wrapp behaves as expected", {
  input <- "../data/clinical.csv"
  
  r_wrapp("r_import", input = input, data.name = "clinicalnew", row.names = 1)
  
  current_wd <- ls(envir = .GlobalEnv)
  expect_true("clinicalnew" %in% current_wd)
  expect_true("object_db" %in% current_wd)
  
  object_db <- get("object_db", envir = .GlobalEnv)
  expect_equal(object_db[[1]]$dataset_info$class, "data.frame")
  expect_named(object_db[[1]], c("id", "object_name", "user_name", "nature", 
                                 "origin_dataset", "parent_edge", "type", 
                                 "dataset_info", "func_name", "func_args", 
                                 "groups", "logt", "normalized", "messages",
                                 "asterics_code_version"))
  expect_named(object_db[[1]]$dataset_info, c("class", "meta"))
  expect_named(object_db[[1]]$dataset_info$meta, 
               c("nrow", "ncol", "nbmissing", "nbnum", "nbcat", "logt", "normalized", "norm_factors"))
  
  
  out_preview <- r_wrapp("r_import", input = input, data.name = "clinical", 
                         row.names = 1, preview = TRUE)
  expect_true("json" %in% class(out_preview))
  
  out_rsummarydesc <- r_wrapp("r_summary_description", dataset = "clinicalnew",
                              rstart = 1, rend = 5, cstart = 1, cend=5)
  expect_equal(class(out_rsummarydesc), "json")
})

test_that("r_wrapp returns correct messages and errors", {
  # Still handles warnings
  expect_warning(
    expect_error(
      test <- r_wrapp("r_import", input = "doesnt_exists", 
                      data.name = "clinicalnew", row.names = 1)
    )
  )
  
  input <- "../data/clinical.csv"
  t <- r_wrapp("r_import", input = input, data.name = "clinical", row.names = 1)
    
  expect_message(
      test <- r_wrapp("r_missing_overview", "clinical"),
      NA
  )
  
  expect_equal(jsonlite::fromJSON(test, simplifyVector=FALSE), 
               list("Messages" = list("type" = "notification",
                                      "data" = list(list("type" = "message",
                                                    "text" = "No missing value found in the dataset")))))
  
  expect_error(
    test <- r_wrapp("r_missing_overview", "clinical", noargument=FALSE),
    "(noargument = FALSE)"
  )
})

rm(clinicalnew, envir = .GlobalEnv)
rm(graph_db, envir = .GlobalEnv)
rm(object_db, envir = .GlobalEnv)
