input <- "../data/protein.csv"
r_import(input, data.name = "proteins", header = TRUE, sep = " ",
         quote = '\"', dec = ".", row.names = 1, assign = TRUE)
input <- "../data/clinical.csv"
r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
input <- "../data/mrna.csv"
r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)

clinical2 <- clinical[, c("patient.age_at_initial_pathologic_diagnosis",
                          "patient.day_of_form_completion",
                          "patient.month_of_form_completion",
                          "patient.clinical_cqcf.consent_or_death_status",
                          "patient.clinical_cqcf.histological_type",
                          "patient.gender")]
tmp_file <- tempfile("clinical2", tmpdir = ".", fileext = ".csv")
write.table(clinical2, file = tmp_file, sep = ",")
r_import(tmp_file, data.name = "clinical2", row.names = 1, header = TRUE, 
         sep = ",", assign = TRUE)
unlink(tmp_file)

out_combdt <- r_wrapp("r_combine_datasets", c("clinical2", "proteins", "mrna"))
out_mfa <- r_wrapp("r_mfa", "combinedDF_1", ncp = 5)

test_that("'r_plotgroup' behaves as expected", {
  out <- r_plotgroup("MFAobj_1")
  expect_true(class(out) == "list")
  expect_named(out, "Graphical")
  
  expect_named(out$Graphical, "PlotGroup")
  expect_true("plotly" %in% class(out$Graphical$PlotGroup))
})

rm(combinedDF_1, inherits = TRUE)
rm(MFAobj_1, inherits = TRUE)
rm(object_db, envir = .GlobalEnv)
rm(num_obj, envir = .GlobalEnv)
