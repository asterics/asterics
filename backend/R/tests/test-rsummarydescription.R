input <- "../data/clinical.csv"
r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)

test_that("r_summary_description behaves as expected", {
  out <- r_summary_description("clinical", rstart = 1, rend = 5, cstart = 1, 
                               cend = 3)
  expect_true(class(out) == "list")
  expect_named(out, "Table")
  
  expect_named(out$Table, c("GeneralInformation", "DataView"))
  out_table_class <- sapply(out$Table, class)
  expect_equal(unname(out_table_class),
               rep("list", length(out_table_class)))
  
  for (ind in 1:length(out$Table)) {
    expect_named(out$Table[[ind]], c("type", "title", "data", "fields"))
    expect_true(is.character(out$Table[[ind]]$type))
    expect_true(is.character(out$Table[[ind]]$title))
    expect_equal(class(out$Table[[ind]]$data), "data.frame")
    expect_equal(class(out$Table[[ind]]$fields), "data.frame")
  }

})

test_that("r_summary_description can return the whole dataset", {
  out <- r_summary_description("clinical", rstart = 1, cstart = 1)
  expect_equal(ncol(out$Table$DataView$data), ncol(clinical))
  expect_equal(nrow(out$Table$DataView$data), nrow(clinical))
  expect_equal(nrow(out$Table$DataView$fields), ncol(clinical))
})

test_that("r_summary_description works on a one column dataset", {
  input <- "../data/subtype.txt"
  r_import(input, data.name = "subtype", row.names = 1, sep = " ",
           header = TRUE, assign = TRUE)
  out <- r_summary_description("subtype", rstart = 1, cstart = 1)
  expect_true(class(out) == "list")
})
