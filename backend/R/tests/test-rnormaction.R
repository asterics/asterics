cur_obj <- ls(envir = .GlobalEnv)

input <- "../data/protein.csv"
r_wrapp("r_import", input = input, data.name = "proteins", header = TRUE, 
        sep = " ", quote = '\"', dec = ".", row.names = 1)


test_that("action 'log' in r_norm_action works as expected", {
  
  # set input data
  df <- abs(proteins) + 0.5
  attributes(df) <- attributes(proteins)
  
  # check that attributes are preserved by 'log'
  a <- attributes(df)
  b <- attributes(log(df))
  b <- b[match(names(a), names(b))]
  expect_identical(a, b)
  
  # check that attributes order is preserved through the function
  expect_identical(
    names(attributes(r_norm_action(df = df, action = "log", value = 0))),
    names(attributes(df))
  )
  
  # check result values and attributes
  expect_identical(
    r_norm_action(df = df, action = "log", value = 0),
    log(df)
  )
  expect_identical(
    r_norm_action(df = df, action = "log2", value = 0),
    log2(df)
  )
  expect_identical(
    r_norm_action(df = df, action = "log10", value = 0),
    log10(df)
  )
  
  # error cases
  
  df[1, 1] <- - 1
  expect_message(res <- r_norm_action(df = df, action = "log", value = 0))
  expect_null(res)
  
  df[1, 1] <- 0
  expect_message(res <- r_norm_action(df = df, action = "log", value = 0))
  expect_null(res)
  
})

test_that("action 'scale' in r_norm_action works as expected", {
  
  # set input data
  df <- proteins
  attributes(df) <- attributes(proteins)
  
  # check attributes
  expect_identical(
    attributes(r_norm_action(df = df, action = "scale")),
    attributes(df)
  )
  
  # check values 
  expect_equivalent(
    r_norm_action(df = df, action = "scale"),
    sweep(x = df, MARGIN = 2, STATS = colMeans(df), FUN = `-`)
  )
  
  # check values 
  expect_equivalent(
    r_norm_action(df = df, action = "scale_reduce"),
    as.data.frame(scale(df))
  )
  
  # messages
  
  df[, 1] <- 0
  expect_message(res <- r_norm_action(df = df, action = "scale_reduce"))
  expect_true(ncol(res) == (ncol(df) - 1))
  
  df[, 1] <- c(2, rep(NA, nrow(df) - 1))
  expect_message(res <- r_norm_action(df = df, action = "scale_reduce"))
  expect_true(ncol(res) == (ncol(df) - 1))
  
  df[, 1] <- NA_real_
  expect_message(res <- r_norm_action(df = df, action = "scale"))
  expect_true(ncol(res) == (ncol(df) - 1))
  
  df[, 3] <- 1
  expect_message(res <- r_norm_action(df = df, action = "scale_reduce"))
  expect_true(ncol(res) == (ncol(df) - 2))
  
  df[, 1:141] <- NA_real_
  expect_message(res <- r_norm_action(df = df, action = "scale"))
  expect_true(all(dim(res) == c(nrow(df), 1)))
  
  df[, 1:142] <- NA_real_
  expect_message(res <- r_norm_action(df = df, action = "scale"))
  expect_true(all(dim(res) == c(nrow(df), 0)))
  
})

test_that("r_norm_action works as expected when non-numeric columns are present", {
  
  # set input data
  df <- abs(proteins) + 0.5
  attributes(df) <- attributes(proteins)
  # add one column
  df$var1 <- sample(x = letters, size = nrow(df), replace = T)
  
  expect_equal(
    r_norm_action(df = df, action = "log", value = 0),
    cbind(log(df[, setdiff(names(df), "var1")]), df[, "var1", drop = F]),
    check.attributes = FALSE
  )
  
  # check that the column position does not change
  df <- df[, c(names(df)[1:50], "var1", names(df)[51:142])]
  
  expect_identical(
    r_norm_action(df = df, action = "log", value = 0),
    cbind(log(df[, names(df)[1:50]]), df[, "var1", drop = F], log(df[, names(df)[52:143]]))
  )
  
  # except when ILR is called
  expect_identical(
    names(r_norm_action(df = df, action = "ILR")),
    c(paste0("ilr.", 1:141), "var1")
  )
  
  # check that filtering all columns still outputs non-numeric ones
  expect_identical(
    suppressMessages(r_norm_action(df = df, action = "filter_relative", value = 1)),
    df[, "var1", drop = F]
  )
  
  # check result when 'df' has only non-numeric columns
  expect_identical(
    r_norm_action(df = df[, "var1", drop = F], action = "filter_relative", value = 0.5),
    df[, "var1", drop = F]
  )
  
})


test_that("action 'offset' in r_norm_action works as expected", {
  
  expect_equivalent(
    r_norm_action(df = proteins, action = "offset", value = 0.5),
    proteins + 0.5, 
  )
  expect_identical(
    attributes(r_norm_action(df = proteins, action = "offset", value = 0.5)),
    attributes(proteins)
  )
  
})



test_that("action 'combat' in r_norm_action works as expected", {
  
  # set input data
  df <- abs(proteins) + 0.5
  attributes(df) <- attributes(proteins)
  
  # test on 'proteins'
  df_num <- proteins
  attr(df_num, "nature") <- "rna-count"
  attr(df_num, "logt") <- "yes"
  B <- sample(letters[1:5], nrow(df), replace = TRUE)
  msg <- capture.output(res <- as.data.frame(t(sva::ComBat(dat = as.data.frame(t(df_num)), batch = B))), type = "message")
  expect_equivalent(
    res,
    r_norm_action(df = df_num, action = "combat", batches = B)
  )
  
  # check errors
  expect_error(r_norm_action(df = df, action = "combat"))
  expect_error(r_norm_action(df = df, action = "combat", batches = 1:nrow(df)))
  
  
  ### batch related errors
  
  # NA present
  B[1] <- NA
  expect_message(res <- r_norm_action(df = df, action = "combat", batches = B))
  expect_null(res)
  
  # one batch with only one individual
  B <- sample(letters[1:5], nrow(df) - 1, replace = T)
  B[nrow(df)] <- "f"
  expect_message(res <- r_norm_action(df = df, action = "combat", batches = B))
  expect_null(res)
  
  # only one batch present
  B <- rep("a", nrow(df))
  expect_message(res <- r_norm_action(df = df, action = "combat", batches = B))
  expect_null(res)
  
  ### check data
  
  # NA present in 'count'
  df_int <- round(df * 10)
  attr(df_int, "nature") <- "rna-count"
  attr(df_int, "logt") <- "no"
  df_int[1,1] <- NA
  B <- sample(letters[1:5], nrow(df), replace = TRUE)
  expect_message({
    res <- r_norm_action(df = df_int, action = "combat", batches = B)
  }, "E: Missing values detected.")
  expect_null(res)
  
  # negative data present in 'count'
  df_int[1, 1] <- -1
  expect_message({
    res <- r_norm_action(df = df_int, action = "combat", batches = B)
  }, "E: Negative values detected")
  expect_null(res)
  
  # check that ComBat returns a message on TMM when performed on RNAseq data
  df_int <- round(df * 10)
  attr(df_int, "nature") <- "rna-count"
  attr(df_int, "logt") <- "no"
  expect_message({
    res <- r_norm_action(df = df_int, action = "combat", batches = B)
  }, "M: TMM normalization performed")

  # same test for data that is not 'count'
  df_num <- df
  attr(df_num, "nature") <- "rna-count"
  attr(df_num, "logt") <- "yes"
  col_sample <- sample(1:ncol(df), 15)
  df_num[B == "c", col_sample] <- 0.5
  res <- r_norm_action(df = df_num, action = "combat", batches = B)
  expect_equivalent(res[, col_sample], df_num[, col_sample])
  expect_error(expect_equivalent(res[, -col_sample], df_num[, -col_sample]))
  
  # check that an error message is returned when too many variables are invalid
  for (i in 2:ncol(df_num)) {
    df_num[B == "d", i] <- i / 100
  }
  expect_message(res <- r_norm_action(df = df_num, action = "combat", batches = B))
  expect_null(res)
  
  # one variable in one batch has only one non-NA value : error
  df_num <- df
  attr(df_num, "nature") <- "rna-count"
  attr(df_num, "logt") <- "yes"
  newcol <- df_num[B == "e", 70]
  newcol[2:length(newcol)] <- NA
  df_num[B == "e", 70] <- newcol
  expect_message(res <- r_norm_action(df = df_num, action = "combat", batches = B))
  expect_null(res)
  
  # but it works when there are two non-NA values
  df_num <- df
  attr(df_num, "nature") <- "rna-count"
  attr(df_num, "logt") <- "yes"
  newcol <- df_num[B == "e", 70]
  newcol[3:length(newcol)] <- NA
  df_num[B == "e", 70] <- newcol
  expect_message(res <- r_norm_action(df = df_num, action = "combat", batches = B))
  expect_null(res)
  
  # error when all variables in one batch have a sum below 5 each
  df_num <- df
  attr(df_num, "nature") <- "rna-count"
  attr(df_num, "logt") <- "no"
  df_num[B == "c",] <- sweep(df_num[B == "c",], 2, colSums(df_num), `/`)
  expect_message(res <- r_norm_action(df = df_num, action = "combat", batches = B))
  expect_null(res)
  
  # it works if one variable has a sum above 5
  df_num[B == "c", 1] <- 1
  res <- r_norm_action(df = df_num, action = "combat", batches = B)
  expect_true(is.data.frame(res))
  
})

test_that("action 'filter' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10)
  attributes(df) <- attributes(proteins)
  
  # check result
  res <- r_norm_action(df = df, action = "filter_absolute", value = 700)
  
  expect_identical(nrow(res), nrow(df))
  expect_true(ncol(res) <= ncol(df))
  expect_true(all(colSums(res) > 700))
  expect_true(all(colSums(df[, setdiff(names(df), names(res)), drop = F]) <= 700))
  expect_identical(
    attributes(res)[setdiff(names(attributes(res)), "names")],
    attributes(df)[setdiff(names(attributes(df)), "names")]
  )
  
  # check extreme cases
  
  expect_identical(
    r_norm_action(df = df, action = "filter_absolute", value = -1), 
    df
  )
  expect_message(res <- r_norm_action(df = df, action = "filter_absolute", value = 8000))
  expect_true(ncol(res) == 0)
  
  
  # relative filters
  
  # check result
  res <- r_norm_action(df = df, action = "filter_relative", value = 0.007)
  
  expect_identical(nrow(res), nrow(df))
  expect_true(ncol(res) <= ncol(df))
  expect_true(all(colSums(res) > 0.007 * sum(colSums(df))))
  expect_true(all(colSums(df[, setdiff(names(df), names(res)), drop = F]) <= 0.007 * sum(colSums(df))))
  expect_identical(
    attributes(res)[setdiff(names(attributes(res)), "names")],
    attributes(df)[setdiff(names(attributes(df)), "names")]
  )
  
  # check extreme cases
  
  expect_identical(
    r_norm_action(df = df, action = "filter_relative", value = -0.1), 
    df
  )
  
  expect_message(res <- r_norm_action(df = df, action = "filter_relative", value = 1))
  expect_true(ncol(res) == 0)
  expect_true(nrow(res) == nrow(df))
  
  # max filter
  
  # check result
  res <- r_norm_action(df = df, action = "filter_max", value = 35)
  
  expect_identical(nrow(res), nrow(df))
  expect_true(ncol(res) <= ncol(df))
  expect_true(all(apply(res, 2, max) > 35))
  expect_true(all(apply(df[, setdiff(names(df), names(res)), drop = F], 2, max) <= 35))
  expect_identical(
    attributes(res)[setdiff(names(attributes(res)), "names")],
    attributes(df)[setdiff(names(attributes(df)), "names")]
  )
  
  # check extreme cases and NA
  
  expect_identical(
    r_norm_action(df = df, action = "filter_max", value = -1), 
    df
  )
  expect_message(res <- r_norm_action(df = df, action = "filter_max", value = 66))
  expect_true(ncol(res) == 0)
  # all NA
  df1 <- df
  df1[] <- NA_real_
  expect_message(res <- r_norm_action(df = df1, action = "filter_max", value = 66))
  expect_true(ncol(res) == 0)
  # partly NA
  df1 <- diag(1:10)
  df1[df1 == 0] <- NA_real_
  df1 <- as.data.frame(df1)
  expect_message(res <- r_norm_action(df = df1, action = "filter_max", value = 5))
  expect_true(all(sapply(res, max, na.rm = T) > 5))
  
  # check other messages
  
  df[1,1] <- -1
  expect_message(res <- r_norm_action(df = df, action = "filter_relative", value = 0.007))
  expect_null(res)
  
  df[1,1] <- NA
  expect_message(res <- r_norm_action(df = df, action = "filter_relative", value = 0.007))
  expect_null(res)
  
  expect_message(res <- r_norm_action(df = df, action = "filter_max", value = 35))
  
  df <- data.frame(matrix(data = 0, nrow = 10, ncol = 10))
  expect_message(res <- r_norm_action(df = df, action = "filter_relative", value = 0.007))
  expect_null(res)
  
})



test_that("action 'norm_quantiles' in r_norm_action works as expected", {
  
  expect_equivalent(
    r_norm_action(df = proteins, action = "norm_quantiles"),
    as.data.frame(t(preprocessCore::normalize.quantiles(x = t(as.matrix(proteins)))))
  )
  expect_identical(
    attributes(proteins),
    attributes(r_norm_action(df = proteins, action = "norm_quantiles"))
  )
  
})



test_that("action 'CLR' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10) + 1
  attributes(df) <- attributes(proteins)
  
  # check values and attributes
  res <- compositions::clr(x = df)
  res <- as.data.frame(res[1:nrow(res), 1:ncol(res)])
  expect_equivalent(
    r_norm_action(df = df, action = "CLR"),
    res
  )
  expect_identical(
    attributes(r_norm_action(df = df, action = "CLR")),
    attributes(df)
  )
  
  # error cases
  
  # negative value or zero
  df <- round(abs(proteins) * 10)
  attributes(df) <- attributes(proteins)
  expect_message(res <- r_norm_action(df = df, action = 'CLR'))
  expect_null(res)
  
  # NA
  df[1, 1] <- NA
  expect_message(res <- r_norm_action(df = df, action = 'CLR'))
  expect_null(res)
  
  # small dimensions
  df <- round(abs(proteins) * 10) + 1
  expect_message(res <- r_norm_action(df = df[, 1, drop = F], action = 'CLR'))
  expect_null(res)
  
  # check that regular and compositional data have identical results
  expect_equivalent(
    r_norm_action(df = df, action = "CLR"),
    r_norm_action(df = sweep(df, 1, rowSums(df), `/`), action = "CLR")
  )
  
})



test_that("action 'ILR' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10) + 1
  attributes(df) <- attributes(proteins)
  
  res <- compositions::ilr(x = df)
  res <- as.data.frame(res[1:nrow(res), 1:ncol(res)])
  
  # check values and attributes
  expect_equivalent(
    r_norm_action(df = df, action = "ILR"),
    res
  )
  expect_identical(
    names(r_norm_action(df = df, action = "ILR")),
    paste0("ilr.", 1:ncol(res))
  )
  expect_identical(
    rownames(r_norm_action(df = df, action = "ILR")),
    rownames(df)
  )
  other_attr <- setdiff(names(attributes(df)), c("names", "row.names"))
  expect_identical(
    attributes(r_norm_action(df = df, action = "ILR"))[other_attr],
    attributes(df)[other_attr]
  )
  
  # check that regular and compositional data have identical results
  expect_equivalent(
    r_norm_action(df = df, action = "ILR"),
    r_norm_action(df = sweep(df, 1, rowSums(df), `/`), action = "ILR")
  )
  
})



test_that("action 'CSS' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10)
  attributes(df) <- attributes(proteins)
  
  # check result format
  expect_equivalent(r_norm_action(df = df, action = "CSS"), df)
  expect_length(attr(r_norm_action(df = df, action = "CSS"), "norm_factors"), 
                nrow(df))
  expect_equal(prod(attr(r_norm_action(df = df, action = "CSS"), "norm_factors")),
               1)
  
  # check factor values by re-computing css factors
  p <- 0.5
  id_rows <- sample(x = 1:nrow(df), size = 2)
  css_sums <- numeric(2)
  for (i in 1:2) {
    id_row <- id_rows[i]
    counts <- as.numeric(df[id_row,])
    counts[counts == 0] <- NA
    q_row <- matrixStats::colQuantiles(x = matrix(counts, ncol = 1), probs = p, 
                                       na.rm = TRUE)
    counts <- counts - .Machine$double.eps
    css_sums[i] <- sum(counts[counts <= q_row], na.rm = TRUE)
  }
  css_factors <- unname(attr(r_norm_action(df = df, action = "CSS"), "norm_factors")[id_rows])
  expect_equal((css_sums[1] / sum(df[id_rows[1],])) / css_factors[1],
               (css_sums[2] / sum(df[id_rows[2],])) / css_factors[2])
  
  # small datasets
  expect_equivalent(attr(r_norm_action(df = df[1,], action = "CSS"), "norm_factors"),
                    1)
  df <- data.frame(a = 1:10)
  expect_message({ res <- r_norm_action(df = df, action = "CSS") })
  expect_null(res)
  
  # NA
  df[1,1] <- NA
  expect_message({ res <- r_norm_action(df = df, action = "CSS") })
  expect_null(res)
  
  # negative values
  df[1,1] <- -1
  expect_message({ res <- r_norm_action(df = df, action = "CSS") })
  expect_null(res)
  
  # some cases where factors must be 1
  # constant rows
  df <- data.frame(a = 1:10, b = 1:10)
  expect_equivalent(
    attr(r_norm_action(df = df, action = "CSS"), "norm_factors"),
    rep(1, 10)
  )
  # rows with identical counts
  df <- as.data.frame(t(data.frame(a = 1:10, b = 10:1)))
  expect_equivalent(
    attr(r_norm_action(df = df, action = "CSS"), "norm_factors"),
    rep(1, 2)
  )
  
})



test_that("action 'TMM' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10) 
  attributes(df) <- attributes(proteins)
  
  # check output format
  expect_equivalent(r_norm_action(df = df, action = "TMM"),
                    df)
  expect_length(attr(r_norm_action(df = df, action = "TMM"), "norm_factors"), 
                nrow(df))
  expect_equal(prod(attr(r_norm_action(df = df, action = "TMM"), "norm_factors")),
               1)
  
  # small datasets
  expect_equivalent(attr(r_norm_action(df = df[1,], action = "TMM"), "norm_factors"),
                    1)
  
  # messages when lib size is 0 for some individuals
  expect_message(r_norm_action(df = df[, 1, drop = F], action = "TMM"))
  expect_message(r_norm_action(df = df[, 1:2, drop = F], action = "TMM"))
  expect_message(r_norm_action(df = df[, 1:3, drop = F], action = "TMM"))
  expect_message(r_norm_action(df = df[, 1:4, drop = F], action = "TMM"))
  expect_equivalent(r_norm_action(df = df[, 1:5, drop = F], action = "TMM"),
                    df[, 1:5])
  
  # NA
  df[1,1] <- NA
  expect_message(res <- r_norm_action(df = df, action = "TMM"))
  expect_null(res)
  
  # negative
  df[1,1] <- -1
  expect_message(res <- r_norm_action(df = df, action = "TMM"))
  expect_null(res)
  
})



test_that("action 'cpm' in r_norm_action works as expected", {
  
  # set input data
  df <- round(abs(proteins) * 10) 
  attributes(df) <- attributes(proteins)
  
  # check values and attributes
  expect_equivalent(r_norm_action(df = df, action = "cpm"),
                    sweep(df, MARGIN = 1, STATS = rowSums(df), FUN = `/`) * 1E6)
  expect_identical(attributes(r_norm_action(df = df, action = "cpm")),
                   attributes(df))
  
  # same with normalization factors
  tmm_factors <- edgeR::calcNormFactors(t(df), method = "TMM")
  tmm_df <- r_norm_action(df = df, action = "TMM")
  
  expect_equivalent(r_norm_action(df = tmm_df, action = "cpm"),
                    sweep(df, MARGIN = 1, STATS = rowSums(df) * tmm_factors, FUN = `/`) * 1E6)
  expect_identical(attributes(r_norm_action(df = tmm_df, action = "cpm")),
                   attributes(r_norm_action(df = df, action = "TMM")))
  
  # messages when lib size is 0 for some individuals
  expect_message(r_norm_action(df = df[, 1, drop = F], action = "cpm"))
  expect_message(r_norm_action(df = df[, 1:2, drop = F], action = "cpm"))
  expect_message(r_norm_action(df = df[, 1:3, drop = F], action = "cpm"))
  expect_message(r_norm_action(df = df[, 1:4, drop = F], action = "cpm"))
  expect_equivalent(r_norm_action(df = df[, 1:5, drop = F], action = "cpm"),
                    sweep(df[, 1:5], MARGIN = 1, STATS = rowSums(df[, 1:5]), FUN = `/`) * 1E6)
  
  # NA
  df[1,1] <- NA
  expect_message(res <- r_norm_action(df = df, action = "cpm"))
  expect_null(res)
  
  # negative
  df[1,1] <- -1
  expect_message(res <- r_norm_action(df = df, action = "cpm"))
  expect_null(res)
  
})



newobj <- setdiff(ls(envir = .GlobalEnv), cur_obj)
rm(list=newobj, inherits = TRUE)
