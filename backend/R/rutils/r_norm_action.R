#' @description modify a dataset (e.g. apply log, filter based on row counts, compute several normalizations)
#'
#' @param df | data.frame to modify | data.frame | required |
#' @param action | action to perform | character | required | one of:
#' log - natural logarithm with an offset given in value (x -> log(x + offset))
#' log2 - logarithm in base 2 with an offset given in value (x -> log2(x + offset))
#' log10 - logarithm in base 10 with an offset given in value (x -> log10(x + offset))
#' unlog - x -> exp(x * log(base)), with base given in value
#' offset - given in value
#' scale - centre data
#' scale_reduce - centre and reduce data
#' filter_max - keeping variables whose max values are above a threshold given in value
#' filter_absolute - keeping variables whose sums are above a threshold given in value
#' filter_relative - keeping variables whose sums are, as shares among all others, higher than a threshold given in value
#' norm_quantiles - quantile normalization
#' CLR - Centered Log-Ratio
#' ILR - Isometric Log-Ratio
#' CSS - Cumulative Sum Scaling (saved in attributes 'norm_factors')
#' TMM - weighted Trimmed Mean of M-values (saved in attributes 'norm_factors')
#' TMMwsp - weighted Trimmed Mean of M-values with singleton pairing (saved in attributes 'norm_factors')
#' cpm - count per million
#' combat - remove batch effects based on a known covariate (the method used depends on attributes in 'df')
#' @param value | numeric value required by action | numeric | optional | 0 | value is used as an offset for actions
#' 'log', 'log2', 'log10' and 'offset'. It is used as a threshold for actions 'filter_max', 'filter_absolute' and 'filter_relative',
#' and it is used as the base of the logarithm for action 'unlog'.
#' @param batches | required by action 'combat' | character or factor | optional | NULL | a vector indicating
#' # groups to which individuals in 'df' belong. Its length should be nrow(df).
#'
#' @return a modified data.frame, whose attributes are in the same order as the ones of
#' the original data.frame (new ones can be added). NULL is returned when the transformation is impossible.
#'
#' @examples
#' # offset + log
#' df <- as.data.frame(matrix(data = rnorm(1E5), nrow = 1000, ncol = 100))
#' df[df <= -0.5] <- NA
#' res <- r_norm_action(df = df, action = "log10", value = 0.5)
#'
#' # cpm
#' df <- floor(as.data.frame(matrix(data = runif(1E5), nrow = 1000, ncol = 100)) * 100)
#' res <- r_norm_action(df = df, action = "cpm")
#'
r_norm_action <- function(df,
                          action = c(
                            "log", "log2", "log10", "unlog", "offset",
                            "scale", "scale_reduce", "filter_max",
                            "filter_absolute", "filter_relative",
                            "norm_quantiles", "CLR", "ILR", "CSS",
                            "TMM", "TMMwsp", "cpm", "combat"
                          ),
                          value = 0, batches = NULL) {
  ### record attributes (since they are deleted upon modifying)

  attributes0 <- attributes(df)

  # leave non-numeric variables aside
  df_other <- df[, !sapply(df, is.numeric), drop = F]
  df <- df[, sapply(df, is.numeric), drop = F]
  if (ncol(df) == 0) {
    return(df_other)
  }

  ### apply transformation
  # When arguments are not correct, send a message and return NULL.
  action <- match.arg(action)


  #### action log, log2, log10 ####
  if (action %in% c("log", "log2", "log10")) {
    df <- df + value
    if (any(df <= 0, na.rm = T)) {
      message("E: Some values are negative or 0. Logarithm cannot be applied!")
      return(NULL)
    }
    df <- do.call(action, list(df))
  }

  #### action unlog ####
  if (action == "unlog") {
    df <- exp(df * log(value))
  }

  #### actions scale and scale_reduce ####
  if (action %in% c("scale", "scale_reduce")) {
    df <- scale(df, center = T, scale = (action == "scale_reduce"))

    rm_cols <- which(colSums(is.na(df)) == nrow(df))
    if (length(rm_cols) > 0) {
      df <- df[, -rm_cols, drop = F]
      message(sprintf(
        paste(
          "W: %s%s numeric variables have been removed after scaling.",
          "They had only missing values%s."
        ),
        ifelse(ncol(df) == 0, "All ", ""),
        length(rm_cols),
        ifelse(action == "scale_reduce", " or their standard deviations were 0", "")
      ))
    }
  }

  #### action offset ####
  if (action == "offset") {
    df <- df + value
  }

  #### action combat: remove batch effects ####
  if (action == "combat") {
    # check 'batches'
    if (length(batches) != nrow(df)) {
      stop("The variable indicating batches should have same length as the number of individuals!")
    }
    if (!is.character(batches) && !is.factor(batches)) {
      stop("The variable indicating batches should be categorical!")
    }
    if (anyNA(batches)) {
      message("E: The variable indicating batches should not contain missing values!")
      return(NULL)
    }
    if (length(unique(batches)) == 1) {
      message("E: There should be at least two batches!")
      return(NULL)
    }
    if (any(table(batches) == 1)) {
      message("E: There should be at least two individuals per batch!")
      return(NULL)
    }

    is_count_data <- grepl("-count$", attributes0[["nature"]], perl = T)
    is_count_data <- is_count_data && attributes0[["logt"]] == "no"

    if (anyNA(df)) {
      message("E: Missing values detected. Batch effects cannot be corrected! \nPlease handle missing values (imputation or removal).")
      return(NULL)
    }

    # check specifically 'count' data
    if (is_count_data) {
      if (any(df < 0)) {
        message("E: Negative values detected. Batch effects cannot be corrected!")
        return(NULL)
      }

      if (any(rowSums(df) == 0)) {
        message("E: Some individuals have a library size of 0. Batch effects cannot be corrected!")
        return(NULL)
      }
    }

    # find invalid cols
    rm_cols <- c()

    for (categ in unique(batches)) {
      if (!is_count_data) {
        rm_col_b <- which(apply(df[batches == categ, ], 2, function(x) var(x) == 0))
      } else {
        rm_col_b <- which(apply(df[batches == categ, ], 2, function(x) all(x == 0)))
      }
      rm_cols <- union(rm_cols, rm_col_b)
    }

    valid_cols <- setdiff(1:ncol(df), rm_cols)

    if (length(valid_cols) <= 1) {
      message(
        "E: Not enough valid variables available in the dataset to correct batch effects.\n",
        "   Either there are too few variables or they all have a variance of 0 (or they are all 0)\n",
        "   in at least one batch!"
      )
      return(NULL)
    }

    # find batches with too small counts
    if (is_count_data) {
      is_small_count <- any(sapply(
        X = unique(batches),
        FUN = function(x) all(colSums(df[batches == x, valid_cols]) < 5)
      ))
      if (is_small_count) {
        message(
          "E: Batch effects cannot be corrected because sum of counts over variables\n",
          "   are too low (<5) in at least one batch!"
        )
        return(NULL)
      }
    }

    # call 'ComBat'
    if (is_count_data) {
      message("M: TMM normalization performed prior to ComBat batch correction effect")
      dge <- edgeR::DGEList(t(df))
      dge <- edgeR::calcNormFactors(object = dge, method = "TMM")
      dge <- edgeR::cpm(dge)
      msg <- capture.output(suppressMessages(suppressWarnings({
        df <- try(sva::ComBat_seq(counts = dge, batch = batches), silent = TRUE)
      })))
    } else {
      msg <- capture.output(suppressMessages(suppressWarnings({
        df <- try(sva::ComBat(dat = t(df), batch = batches),
          silent = TRUE
        )
      })))
    }

    if (class(df)[1] == "try-error") {
      message("E: Unknown error identified when correcting batch effects. Aborting!")
      return(NULL)
    }
    df <- as.data.frame(t(df))
  }

  #### filter actions: filter using an absolute or relative threshold ####
  if (grepl(pattern = "^filter", x = action)) {
    if (any(df < 0, na.rm = T)) {
      message("E: Negative data detected. Filtering cannot be performed!")
      return(NULL)
    }

    if (anyNA(df)) {
      if (action == "filter_max") {
        message("M: Missing data detected and ignored while filtering!")
      } else {
        message("E: Missing data detected. Filtering cannot be performed!")
        return(NULL)
      }
    }

    vec_filter <- colSums(x = df)

    if (action == "filter_relative") {
      tot_sum <- sum(vec_filter)
      if (tot_sum == 0) {
        message("E: The data contains only 0's. Filtering cannot be performed!")
        return(NULL)
      }

      vec_filter <- vec_filter / tot_sum
    }

    if (action == "filter_max") {
      df <- df[, colSums(df > value, na.rm = T) > 0, drop = F]
    } else {
      df <- df[, vec_filter > value, drop = F]
    }

    if (ncol(df) == 0) {
      message("W: Filtering removed all numeric variables.")
    }
  }

  #### action norm_quantiles: normalize quantiles ####
  if (action == "norm_quantiles") {
    dim0 <- dimnames(df)
    df <- preprocessCore::normalize.quantiles(x = t(df), copy = FALSE)
    df <- as.data.frame(t(df))
    dimnames(df) <- dim0
  }


  #### action CLR and ILR: normalize using CLR or ILR ####
  if (action %in% c("CLR", "ILR")) {
    if (any(df <= 0, na.rm = T)) {
      message(sprintf("E: Negative data detected. %s cannot be performed!", action))
      return(NULL)
    }

    if (anyNA(df)) {
      message(sprintf("E: Missing data detected. %s cannot be performed!", action))
      return(NULL)
    }

    if (NCOL(df) < 2) {
      message(sprintf("E: Number of variables below 2. %s cannot be performed!", action))
      return(NULL)
    }

    if (action == "CLR") {
      df <- as.data.frame(as.matrix(compositions::clr(x = df)))
      rownames(df) <- attributes0[["row.names"]]
    }

    if (action == "ILR") {
      df <- as.data.frame(as.matrix(compositions::ilr(x = df)))
      rownames(df) <- attributes0[["row.names"]]
      names(df) <- paste0("ilr.", 1:ncol(df))
    }

    # df <- mixOmics::logratio.transfo(X = df, logratio = action)
    #
    # class(df) <- c("matrix", "array")
    # df <- as.data.frame(df)
    # if (action == "ILR") {
    #   rownames(df) <- rownames0
    #   names(df) <- paste0("ilr.", 1:ncol(df))
    # }
  }

  #### action CSS: normalize using CSS ####
  if (action == "CSS") {
    if (any(df < 0, na.rm = TRUE)) {
      message("E: Negative data detected. CSS cannot be performed!")
      return(NULL)
    }

    if (anyNA(df)) {
      message("E: Missing data detected. CSS cannot be performed!")
      return(NULL)
    }

    if (any(rowSums(df > 0) < 2)) {
      message("E: Some rows have less than two non-zero counts. CSS cannot be performed!")
      return(NULL)
    }

    # compute css quantile
    df <- t(df)
    prob <- try(suppressMessages(suppressWarnings({
      # code to avoid an error in metagenomeSeq
      # (class must not be tested directly and tests with length > 1 now fail)
      # 'Error in if (class(obj) == "MRexperiment") { : the condition has length > 1
      df2 <- metagenomeSeq::newMRexperiment(df)
      # TODO: to be deleted in later metagenomeSeq versions
      metagenomeSeq::cumNormStatFast(obj = df2)
    })), silent = TRUE)

    if (class(prob) == "try-error") {
      if (grepl("Error in if (x <= 0.5) {", prob[[1]], fixed = T)) {
        prob <- 1
      } else {
        message("E: Unknown error identified while computing CSS. Aborting!")
        return(NULL)
      }
    }
    if (all(sapply(df, setequal, df[, 1]))) {
      prob <- 1
    }


    # compute sum up to quantile 'prob'
    df_mat <- as.matrix(df)
    tss_sums <- colSums(df_mat)
    df_mat[df_mat == 0] <- NA
    qs <- matrixStats::colQuantiles(df_mat, probs = prob, na.rm = TRUE)
    df_mat <- df_mat - .Machine$double.eps
    css_sums <- sapply(1:ncol(df_mat), function(i) sum(df_mat[df_mat[, i] <= qs[i], i], na.rm = T))

    # assign normalization factors (normalized so that their product is 1)
    css_ratio <- css_sums / tss_sums
    css_factors <- css_ratio / exp(mean(log(css_ratio)))
    attributes0[["norm_factors"]] <- css_factors

    df <- as.data.frame(t(df))
  }

  #### action TMM: normalize using TMM ####
  if (action %in% c("TMM", "TMMwsp")) {
    if (any(df < 0, na.rm = T)) {
      message("E: Negative data detected. TMM or TMMwsp cannot be performed!")
      return(NULL)
    }

    if (anyNA(df)) {
      message("E: Missing values detected. TMM or TMMwsp cannot be performed!")
      return(NULL)
    }

    if (any(rowSums(df) == 0)) {
      message("E: Some individuals have a library size of 0. TMM or TMMwsp cannot be performed!")
      return(NULL)
    }

    attributes0[["norm_factors"]] <- suppressWarnings({
      edgeR::calcNormFactors(object = t(df), method = action)
    })
  }

  #### action cpm: normalize using cpm ####
  if (action == "cpm") {
    if (any(is.na(df) | df < 0)) {
      message("E: Missing or negative values detected. Count per million cannot be computed!")
      return(NULL)
    }

    if (any(rowSums(df) == 0)) {
      message("E: Some individuals have a library size of 0. Count per million cannot be computed!")
      return(NULL)
    }

    lib.size <- rowSums(df)
    if (length(attributes0[["norm_factors"]]) == length(lib.size)) {
      lib.size <- lib.size * attributes0[["norm_factors"]]
    }

    df <- edgeR::cpm(y = t(df), lib.size = lib.size)
    df <- as.data.frame(t(df))
  }



  ### reset attributes
  # (attributes in 'data' have priority over the ones in 'attributes0',
  # and they are ordered as 'attributes0')

  # transpose to get individuals by rows
  # df <- as.data.frame(t(df))

  # if 'names' are missing, add empty vector
  if (is.null(names(df))) {
    names(df) <- character()
  }

  # add non-numeric columns back
  df <- cbind(df, df_other)
  if (action != "ILR") {
    df <- df[, match(attributes0[["names"]], names(df), nomatch = 0), drop = F]
  }

  new_attributes <- c(attributes(df), attributes0)
  new_attributes <- new_attributes[!duplicated(names(new_attributes))]
  new_attributes <- c(
    new_attributes[names(attributes0)],
    new_attributes[setdiff(names(new_attributes), names(attributes0))]
  )
  attributes(df) <- new_attributes

  return(df)
}
