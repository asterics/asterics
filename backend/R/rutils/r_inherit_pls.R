#' @title Inheritance function for PLS and PLSDA objects
#'
#' @param object | PLS(-DA) object | object | required | 
#' @param ncp | Number of principal components to keep | integer | required | 
#'
#' @return an object of the same class, but with an updated number of components
#' 
#' @examples
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, assign = TRUE)
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
#'
#'
#' # PLS
#' out_combdt <- r_wrapp("r_combine_datasets", list("proteins", "mrna"))
#' out_pls5 <- r_pls("combinedDF_1", ncp = 5)$Object$PLSobj
#' 
#' # Compare the following:
#' out_pls4 <- r_pls("combinedDF_1", ncp = 4)$Object$PLSobj
#' out_inherit <- r_inherit_pls(out_pls5, ncp = 4)
#' 
#' 
#' PLSDA
#' out_combtg <- r_wrapp("r_combine_target", X = "proteins", Y = "clinical", 
#'                      target = "patient.vital_status")
#' out_plsda5 <- r_plsda("multipleDA_1", ncp = 5, seed = 5)$Object$PLSDAobj
#' 
#' # Compare the following:
#' out_plsda4 <- r_plsda("multipleDA_1", ncp = 4, seed = 5)$Object$PLSDAobj
#' out_inherit <- r_inherit_pls(out_plsda5, ncp = 4)
#' 
r_inherit_pls <- function(object, ncp) {
  
  res_pls <- object
  analysis <- gsub("[^::A-Z::]","", attr(object, "nature"))
  
  res_pls$ncomp <- ncp
  
  res_pls$variates <- lapply(res_pls$variates, function(amatrix) 
    amatrix[, 1:ncp, drop = F])
  res_pls$loadings <- lapply(res_pls$loadings, function(amatrix) 
    amatrix[, 1:ncp, drop = F])
  res_pls$prop_expl_var <- lapply(res_pls$prop_expl_var, function(amatrix)
    amatrix[1:ncp, drop = F])
  res_pls$mat.c <- res_pls$mat.c[, 1:ncp, drop = F]
  if (analysis == "PLS") {
    res_pls$cor <- lapply(res_pls$cor, function(amatrix)
      amatrix[, 1:ncp, drop = F])
  } else {
    res_pls$cor <- res_pls$cor[, 1:ncp, drop = F]
  }
  res_pls$Graphical <- suppressMessages(r_make_plsplots(res_pls))
  
  if (analysis == "PLSDA") {
    parent <- attr(object, "parent_edge")
    target <- attr(get(parent), "func_args")$target
    ErrorRatePlot <- plot_error_plsda(res_plsda = res_pls, 
                                      origin.dataset = attr(object, "origin_dataset"),
                                      target.name = target,
                                      seed = attr(object, "func_args")[["seed"]])
    if (!is.null(ErrorRatePlot)) {
      res_pls$Graphical <- c(list(ErrorRatePlot = ErrorRatePlot), res_pls$Graphical)
    }
  }
  
  return(res_pls)
}
