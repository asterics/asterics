#' @title PLS/PLS-DA graphical outputs
#' 
#'
#' @param object | output of PLS/PLS-DA analysis | PLS/PLS-DA object | required | NULL | Object 
#' returned by 'PLS' or 'PLS-DA' for which plots need to be made
#'
#' @return screegraph and plot of the percentage of cumulative variance
#' 
#' @examples 
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, 
#'          assign = TRUE)
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE) 
#'  
#' # PLS
#' out_comb <- r_wrapp("r_combine_datasets", list("proteins", "mrna"))
#' out_pls <- r_wrapp("r_pls", "combinedDF_1")
#' 
#' out_plot <- r_make_plsplots(PLSobj_1)
#' out_plot$Screegraph
#' out_plot$CumulativePercentagePlot
#' 
#' # PLS-DA
#' out_combtg <- r_wrapp("r_combine_target", list("clinical", "mrna"), 
#'                      target = "patient.vital_status")
#' out_plsda <- r_wrapp("r_plsda", "combinedTG_1", ncp = 2, seed = 5)
#'                      
#' out_plot <- r_make_plsplots(PLSDAobj_1)        
#' out_plot$Screegraph
#' out_plot$CumulativePercentagePlot
#' 
r_make_plsplots <- function(object) {
  
  analysis <- gsub("[^::A-Z::]","", attr(object, "nature"))
  
  ## variance plots
  origin.dataset <- attr(object, "origin_dataset")
  origin.dataset <- sapply(origin.dataset, r_get_username)
  
  if (analysis == "PLS") {
    mode <- object$mode
    if (mode == "canonical") {
      dataplot <- data.frame("Block" = c(rep(origin.dataset[1], object$ncomp), 
                                         rep(origin.dataset[2], object$ncomp)),
                             "percVar" = c(object$prop_expl_var$X,
                                           object$prop_expl_var$Y) * 100,
                             "cumPerc" = c(cumsum(object$prop_expl_var$X),
                                           cumsum(object$prop_expl_var$Y)) * 100,
                             "comp" = rep(1:object$ncomp, 2))
      dataplot$Block <- factor(dataplot$Block, levels = origin.dataset,
                               ordered = TRUE)
    } else {
      dataplot <- data.frame("percVar" = object$prop_expl_var$X * 100,
                             "cumPerc" = cumsum(object$prop_expl_var$X) * 100,
                             "comp" = 1:object$ncomp)
    }
  } else {
    mode <- "da"
    origin_obj <- get(attr(object, "func_args")$datasetName)
    target.name <- origin_obj$TargetInfo[["target"]]
    dataplot <- data.frame("percVar" = object$prop_expl_var$X,
                           "cumPerc" = cumsum(object$prop_expl_var$X)*100,
                           "comp" = 1:object$ncomp)
  }

  if (mode == "regression") {
    origin.dataset <- paste0(origin.dataset[1], " (vs ", origin.dataset[2], 
                             ", not represented)")
  } else if (mode == "da") {
    origin.dataset <- paste(origin.dataset, collapse = " predicts ")
    origin.dataset <- paste0(origin.dataset, ": ", target.name)
  } else origin.dataset <- paste(origin.dataset, collapse = ", ")

  Screegraph <- ggplot(dataplot) +
    geom_bar(aes(x = comp, y = percVar), stat = "identity") + theme_bw() +
    ggtitle(paste0(origin.dataset, " - PLS",
                   ifelse(analysis == "PLS", "", "-DA"), ": screegraph")) +
    ylab("% of explained variance") + xlab("Component")
  
  CumulativePercentagePlot <- ggplot(dataplot,  aes(x = comp, y = cumPerc))
  
  if (analysis == "PLS" && mode == "canonical") {
    Screegraph <- Screegraph + facet_grid(~ Block)
    CumulativePercentagePlot <- CumulativePercentagePlot + 
      geom_point(aes(color = Block)) 
    if (nrow(dataplot) > 2) {
      CumulativePercentagePlot <- CumulativePercentagePlot + 
        geom_line(aes(color = Block))
    }
  } else {
      CumulativePercentagePlot <- CumulativePercentagePlot + geom_point()
      if (nrow(dataplot) > 2) 
        CumulativePercentagePlot <- CumulativePercentagePlot + geom_line()
  }
  
  CumulativePercentagePlot <- CumulativePercentagePlot +
    theme_bw() + xlab("component") + scale_color_manual(values = r_colour_palette(k = 2)) +
    ylab("Cumulative % of explained variance") +
    ggtitle(paste0(origin.dataset, " - PLS",
                   ifelse(analysis == "PLS", "", "-DA"), 
                   ": cumulative percentage of variance"))
 
  if (analysis == "PLS" && mode == "canonical") {
    
    Screegraph <- plotly::ggplotly(Screegraph)
    Screegraph <- plotly::layout(Screegraph, 
                                 title=list(xanchor="left", 
                                            y=0.95, 
                                            yref="container"),
                                 margin=list("l"=5, "r"=10, "t"=100, "b"=20))
    CumulativePercentagePlot <- plotly::ggplotly(CumulativePercentagePlot)
    CumulativePercentagePlot <- plotly::layout(CumulativePercentagePlot, 
                                               title=list(xanchor="left", 
                                            y=0.95, 
                                            yref="container"),
                                 margin=list("l"=5, "r"=10, "t"=100, "b"=20))
  }
  
  return(list(
    Screegraph = Screegraph,
    CumulativePercentagePlot = CumulativePercentagePlot
  ))
}
