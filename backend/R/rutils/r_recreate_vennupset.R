#' Recreate the venn diagram fom combined object, used in rmarkdown export. 
#'
#' @param venn a list from a combined object, containing the informations for the venn diagram. 
#'
#' @return a ggplot
#' @examples
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", header = TRUE, sep = " ", 
#'          quote = '\"', dec = ".", row.names = 1, assign = TRUE)
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' out_combdt <- r_wrapp("r_combine_datasets", list("proteins", "mrna"))
#' p <- r_recreate_venn(combinedDF_1$Graphical$VennPlot)
#' u <- r_recreate_upset(combinedDF_1$Graphical$UpsetPlot)
#' 
r_recreate_venn <- function(VennPlot){
  vennData <- VennPlot$data$series[[1]]
  vennDataSets <- unlist(vennData$name)
  vennDataSetsRN <- lapply(names(vennDataSets), function(x) vennData$data[grepl(x, names(vennData$data))])
  vennDataSetsRN <- lapply(vennDataSetsRN, unlist)
  names(vennDataSetsRN) <- vennDataSets
  
  # Same colors as in jvenn
  allColors <- c("#006600", "#5a9bd4", "#f15a60", "#cfcf1b", "#ff7500", "#c09853")
  usedColors <- allColors[seq_len(length(vennDataSets))]
  return(
    ggvenn::ggvenn(
      vennDataSetsRN, 
      fill_color =usedColors,
      stroke_size = 0, set_name_size = 4,
      show_percentage = FALSE
    )
  )
}


r_recreate_upset <- function(upsetPlot){
  upsetDataFull <- lapply(upsetPlot$data, function(x) data.frame("RN" = rep(x$name, length(x$sets)), 
                                                            "DS" = unlist(x$sets)))
  upsetDataFull <- do.call("rbind", upsetDataFull)
  upsetDataDataSets <- unique(upsetDataFull$DS)
  upsetDataDataSetsRN <- lapply(upsetDataDataSets, function(x) unique(upsetDataFull[upsetDataFull$DS==x,]$RN))
  names(upsetDataDataSetsRN) <- upsetDataDataSets
  #TODO : regarder les options de upset (notamment nsets et nintersects) pour recréer le comportement de l'interface
  return(
    UpSetR::upset(UpSetR::fromList(upsetDataDataSetsRN), order.by = "freq")
  )
}
