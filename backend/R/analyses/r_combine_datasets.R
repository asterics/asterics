#'@title Integrative analyses dataset preprocessing
#'@description Dataset preprocessing before running any integrative study.
#'
#' @param datasetNames | List of datasets | list | required | NULL | List of 
#' datasets to process |
#'
#' @return Depending on the number of datasets, a list composed of 
#' \itemize{
#'   \item Graphical - a list of graphical outputs: 
#'     \code{UpSet}, An UpSet plot, 
#'     \code{VennPlot}, A Venn Diagram if the number of datasets is less than 4.
#'   \item Table - a list of descriptive statistics before and after data 
#'   filtering:
#'     \code{dataInfoBefore}: general information on datasets before filtering
#'     based on common individuals,
#'     \code{dataInfoAfter}: general information on datasets after filtering
#'     based on common individuals (only provided if not all individuals were 
#'     common in the input of the analysis),
#'     \code{ObjectName} Name of the complex output object as referenced in
#'     the object database.
#'   \item Object: a list containing a complex object \code{COMobj}, also a list
#'   usable by the users with the following entries:
#'     \code dataGenInfo: a table that contains general information on number of
#'     variables and individuals, as well as types of variables for each input
#'     dataset,
#'     \code{Individuals}: names of individuals kept for integrative analyses, 
#'     \code{Graphical} and \code{Table}, previously described, except for the
#'     entry \code{ObjectName}.
#' }
#'
#' @examples
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
#' clinicals <- clinical[, c("patient.gender", "patient.vital_status")]
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, 
#'          assign = TRUE)
#'
#' # first call
#' out_comb <- r_wrapp("r_combine_datasets", list("clinicals", "proteins", "mrna"))
#' 
#' # recall
#' out_comb2 <- r_combine_datasets(list("mrna", "clinicals", "proteins"))
#' jsonlite::toJSON(out_comb2$Graphical$UpsetPlot, pretty = TRUE, auto_unbox = TRUE)
#' jsonlite::toJSON(out_comb2$Graphical$VennPlot, pretty = TRUE, auto_unbox = TRUE)
#' out_comb2$Table$dataInfoBefore
#' out_comb2$Table$dataInfoAfter
#'
#' # testing it when no common individuals
#' clinicals2 <- clinicals
#' rownames(clinicals2) <- 1:nrow(clinicals2)
#' out_comb2 <- r_combine_datasets(list("clinicals2", "proteins"))
#' out_comb <- r_wrapp("r_combine_datasets", list("clinicals2", "proteins"))
#' 
#' @importFrom upsetjs upsetjsVennDiagram generateDistinctIntersections
#' @importFrom upsetjs fromList chartLabels chartTheme upsetjsVennDiagram

r_combine_datasets <- function(datasetNames) {
  
  if (length(datasetNames) < 2)
    stop("At least two datasets must be provided for integrated analyses!")
  
  datasetNames <- unlist(datasetNames)
  datasetNames <- sort(datasetNames)
  datasetList <- lapply(datasetNames, function(name) get(x = name))
  names(datasetList) <- datasetNames
  nature <- "combinedDF"
  
  # Check if the combination has already been done
  func_args <- list(datasetNames = datasetNames)
  check_exists <- r_call_datadb(func.name = "r_combine_datasets", 
                                func.args = func_args)
  
  if (is.null(check_exists)) { ## case where the analysis has never been performed
    ### perform combination
    Output <- r_combination_analysis(datasetList)
    # Case of no common rownames (already handled by r_combination_analysis)
    if(is.null(Output)){
      return(NULL)
    }
    ### database management: set attributes
    info_name <- r_make_name(nature)
    Output$Object$COMobj <- r_setattr_objects(Output$Object$COMobj, 
                                              object.name = info_name,
                                              nature = nature,
                                              origin.dataset = datasetNames,
                                              type = "combined",
                                              func.args = func_args)
    
  } else { ## case where the analysis already exists
    res_COMdt <- get(names(check_exists)[1])
    info_name <- attr(res_COMdt, "object_name")
    Output <- list(
      Table = res_COMdt$Table[setdiff(names(res_COMdt$Table), "ObjectName")],
      Graphical = res_COMdt$Graphical, 
      Messages = check_exists[[1]]$messages
    )
  }
  
  Output$Table$ObjectName <- info_name
  
  return(Output)
}
