r_corrmat <- function(datasetName){
  dataset <- get(datasetName)
  dataset <- dataset[,r_numeric_columns(dataset), drop=FALSE]
  if(ncol(dataset) == 0) {
      message("E: No numerical variables in the dataset.")
      return(NULL)
  }
  n <- ncol(dataset)
  dataset <- r_remove_zerovar(dataset)
  if(is.null(dataset)) return(NULL)
  if(ncol(dataset) > 20000) {
      message("E: The limit to compute the correlation matrix is set to 20000 numerical variables. ",
              "Choose a dataset with less variables.")
      return(NULL)
  }
  if(ncol(dataset) <=1) {
      message("E: The dataset has only one variable, no correlation plot produced.")
      return(NULL)
  }
  if(ncol(dataset) < n){
      titrevar <- " (Variables with variance > 0)"
  } else {
      titrevar <- ""
  }
  
  # TODO : test with NAs
  if(anyNA(dataset)){
    M <- stats::cor(dataset, use="pairwise.complete.obs")
  } else {
    M <- Rfast::cora(as.matrix(dataset), large=TRUE)
    rownames(M) <- colnames(M) <- colnames(dataset)
  }
  
  if(nrow(M)>20 | ncol(M)>20){
    typePlot <- "heatmap"
  } else {
    typePlot <- "dotplot"
  }
  
  return(list("corrmat" = M, "titrevar" = titrevar, "typePlot" = typePlot))
}


#' Correlation plot of a dataset
#'
#' @param datasetName Name of the dataset
#' @param threshold Threshold to filter the correlation plot in absolute value
#' @examples
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, 
#'          assign = TRUE)
#' r_corrplot("proteins")
#' r_corrplot("proteins", threshold=0.5)
#' prot2 <- proteins[,1:2]
r_corrplot <- function(datasetName, threshold=0){
  
  func.args = list("datasetName" = datasetName)
  check_exists <- r_call_datadb(func.name = "r_corrplot", 
                                func.args = list("datasetName" = datasetName))
  
  Output <- list()
  Messages <- list()
  
  # 1. Get the full correlation matrix (doesn't recompute if already exists)
  if(!is.null(check_exists)){
    # "The analysis have already been performed
    corrmat_list <- get(check_exists[[1]]$object_name)
    Messages <- check_exists[[1]]$messages
  } else {
    corrmat_list <- r_corrmat(datasetName)
    if(is.null(corrmat_list)) return(NULL)
    object.name <- r_make_name(base = "CorrelationPlot")
    corrmat_list <- r_setattr_objects(corrmat_list,
                                 object.name = object.name,
                                 nature = "CorrelationPlot",
                                 func.args = func.args,
                                 origin.dataset = datasetName,
                                 type="hidden")
    Output$Object <- list("CorrMat" = corrmat_list)
  }
  
  M <- corrmat_list$corrmat
  
  # 2. Plot the correlation matrix, with threshold
  userName <- r_get_username(datasetName)
  
  title <- paste0("Correlation plot of ", userName)
  if(threshold>0){
    # Adding 1e-16 because I get wrong results on diagonal with 1 ??
    M[abs(M) < (threshold - 1e-16)] <- NA
    title <- paste0(title, " , |correlation|>", threshold)
  }
  title <- paste(title, corrmat_list$titrevar)
  
  corrplot <- r_heatmap(as.data.frame(M), 
                        dendoRows = FALSE, 
                        dendoCols = FALSE, 
                        typePlot = corrmat_list$typePlot,
                        title = title,
                        xlab = "Variable 1",
                        ylab = "Variable 2",
                        toolx = "Variable 1",
                        tooly = "Variable 2",
                        interactive = TRUE)
  if(corrmat_list$typePlot == "dotplot"){
      # readjust size between O and 1. (min = 0)
      sizes <- corrplot$Graphical$Heatmap[["x"]]$data[[1]]$marker$size
      vals <- c(0, c(M))
      newsizes <- scales::rescale(vals, to=c(min(sizes, na.rm=T), max(sizes, na.rm=T)))
      newsizes <- newsizes[2:length(newsizes)]
      corrplot$Graphical$Heatmap[["x"]]$data[[1]]$marker$size <- newsizes
  }
  # hover for blank parts if threshold
  if(threshold > 0){
      hovertext <- corrplot$Graphical$Heatmap$x$data[[1]]$text
      hovertext <- sapply(hovertext, function(x) substr(x, 1, regexpr("<br>Value:", x)+3), USE.NAMES = FALSE)
      hovertext <- paste0(hovertext, "Value: ")
      hovertext <- paste0(hovertext, c(corrmat_list$corrmat))
      corrplot$Graphical$Heatmap$x$data[[1]]$text <- hovertext
  }
  
  Output$Graphical <- corrplot$Graphical
  if(length(Messages)>0){
    Output$Messages <- Messages
  }
  return(Output)
}
