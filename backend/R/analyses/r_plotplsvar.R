# Plot variables after a PLS/PLS-DA (correlations)
#' @title Plot variables after a PLS/PLS-DA  (correlations)
#' @description  Display the correlation of the variables with the PC of a PlS 
#' on the correlation circle
#'
#' @param datasetName | Name of input object | character | required | NULL | 
#' Name of input object obtained from a PLS/PLS-DA analysis |
#' @param axis1 | x axis | integer | required | 1 | Numbers of the PC to display
#' on the x axis |
#' @param axis2 | y axis | integer | required | 2 | Numbers of the PC to display
#' on the y axis |
#' @param threshold.cor | correlation threshold | numerical | optional | 
#' [{"no threshold": 0}] | Global correlation threshold (with the two axis 
#' overall) to select variables to be displayed |
#' @param mode | Overlap X- and Y-representation plots | character | required | 
#' [{"seperated": seperated, "together": together}] | If `together`, plots 
#' variables into one single graph. If `seperated`, plots variables into two 
#' individual graphs |
#' 
#' @return a list composed of
#' \itemize{
#'   \item Graphical - \code{PlotVar}, the plot
#'   of the correlations of the variables with the selected PCs
#' }
#' 
#' @examples
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, 
#'          assign = TRUE)
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
#' 
#' # PLS
#' out_combdt <- r_wrapp("r_combine_datasets", list("proteins", "mrna"))
#' out_pls <- r_wrapp("r_pls", "combinedDF_1", ncp = 5)
#'                     
#' out_pls_plotvar <- r_plotplsvar("PLSobj_1", mode = "separated")
#' plotly::ggplotly(out_pls_plotvar$Graphical$PlotVar)
#'
#' out_pls_plotvar <- r_plotplsvar("PLSobj_1", mode = "together", 
#'                                 threshold.cor = 0.7)
#' plotly::ggplotly(out_pls_plotvar$Graphical$PlotVar)
#' 
#' PLS-DA
#' out_combtg <- r_wrapp("r_combine_target", X = "mrna", Y = "clinical", 
#'                       target = "patient.vital_status")
#' out_plsda <- r_wrapp("r_plsda", "multipleDA_1", ncp = 5, seed = 5)
#' 
#' out_plsda_plotvar <- r_plotplsvar("PLSDAobj_1", threshold.cor = 0.6) 
#' plotly::ggplotly(out_plsda_plotvar$Graphical$PlotVar)
#' 
r_plotplsvar <- function(datasetName, axis1 = 1, axis2 = 2, 
                         mode = c("separated", "together"),
                         threshold.cor = 0) {
  
  object <- get(datasetName)
  analysis <- gsub("[^::A-Z::]","", attr(object, "nature"))
  origin.dataset.username <- 
    sapply(attr(object, "origin_dataset"), r_get_username)
  
  if (axis1 == axis2)
    stop("Cannot display variables if the two selected PCs are the same.")
  
  mode <- match.arg(mode)
  
  dims <- paste0("Dim.", c(axis1, axis2))
  dim.x <- dims[1]
  dim.y <- dims[2]
  
  if (analysis == "PLS") {
    modepls <- object$mode
    if (modepls == "canonical") {
      dataplot <- data.frame(do.call(rbind, object$cor))
      dataplot <- dataplot[, c(axis1, axis2)]
      dataplot$Block <- rep(origin.dataset.username, 
                            c(nrow(object$cor$X), nrow(object$cor$Y)))
      dataplot$Block <- factor(dataplot$Block, levels = origin.dataset.username,
                               ordered = TRUE)
      
      colnames(dataplot) <- c(dims, "Block")
    } else { # regression: only X is displayed
      omessage <- paste("M: PLS has been performed in regression mode so only",
                        "the first dataset (with more than two variables) is",
                        "represented in the current plot.")
      message(omessage)
      dataplot <- data.frame(object$cor$X)
      dataplot <- dataplot[, c(axis1, axis2)]
      colnames(dataplot) <- dims
    }
    
  } else {
    
    dataplot <- data.frame(object$cor[, c(axis1, axis2)])
    colnames(dataplot) <- dims
    
  }
  
  if (analysis == "PLS" && modepls == "regression") {
    datasets_in_title <- paste0(origin.dataset.username[1], " (vs ", origin.dataset.username[2],
                                ", not represented)")
  } else if (analysis == "PLSDA") {
    origin_obj <- get(attr(object, "func_args")$datasetName)
    target.name <- origin_obj$TargetInfo[["target"]]
    datasets_in_title <- paste(origin.dataset.username, collapse = " predicts ")
    datasets_in_title <- paste0(datasets_in_title, ": ", target.name)
  } else datasets_in_title <- paste(origin.dataset.username, collapse = ", ")
  ptitle <- paste0(datasets_in_title, " - ",
                   ifelse(analysis == "PLS", "PLS", "PLS-DA"),
                   ": correlations of variables with the PCs")
  xlab <- paste0("Component ", axis1)
  ylab <- paste0("Component ", axis2)
  
  if (threshold.cor > 0) {
    cor_axes <- dataplot[, dim.x]^2 + dataplot[, dim.y]^2
    selected <- which(cor_axes >= threshold.cor^2)
    if(length(selected) <= 0){
      message("W: No variable is as correlated with these PCs. ",
              "Try a lower threshold value (maximum for these PCs: ", 
              round(max(sqrt(cor_axes)), 4), ").")
      return(NULL)
    }
    dataplot <- dataplot[selected, ]
  }
  dataplot$variable <- rownames(dataplot)
  
  if (analysis == "PLS") {
    
    if (modepls == "canonical") {
      p <- plot_corrcircle(dataplot, 
                           ptitle, 
                           dim.x, 
                           dim.y, 
                           varcolor = "Block",
                           xlab = xlab, 
                           ylab = ylab) +
        scale_color_manual(values = r_colour_palette(k = length(unique(dataplot$Block))))
      if (mode == "separated") p <- p + facet_grid(~ Block)
    } else {
      p <- plot_corrcircle(dataplot, 
                           ptitle, 
                           dim.x, 
                           dim.y, 
                           xlab = xlab, 
                           ylab = ylab) +
        scale_color_manual(values = r_colour_palette(k = length(unique(dataplot$Block))))
    }
    
  } else {
    
    p <- plot_corrcircle(dataplot, ptitle, dim.x, dim.y, xlab = xlab, 
                         ylab = ylab)
  }
  
  p <- plotly::ggplotly(p)
  
  # Subtitle in case of correlation threshold
  if(threshold.cor>0) {
    subtitle <- paste0("Correlation threshold: ", threshold.cor)
    p <- plotly::layout(p,  
                        title = list(text = paste0(ptitle, '<br>', '<sup>', 
                                                   subtitle,'</sup>'),
                                     xanchor="left", 
                                     y=0.95, 
                                     yref="container"),
                        margin = list("l"=30, "r"=30, "t"=100, "b"=40))
  } else {
    p <- plotly::layout(p,  
                        title = list(text = ptitle,
                                     xanchor="left", 
                                     y=0.95, 
                                     yref="container"),
                        margin = list("l"=30, "r"=30, "t"=60, "b"=40))
  }

  Output <- list("Graphical" = list("PlotVar" = p))
  
  return(Output)
}
