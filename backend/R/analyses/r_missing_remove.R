r_performs_showremove <- function(datasetName, direction = "rows", threshold = 0, size = size){
  out_dataset <- get(datasetName)
  userName <- r_get_username(datasetName)
  
  if(!anyNA(out_dataset)){
    message("M: No NA found in the dataset")
    return(NULL)
  }
  
  out_miss <- r_missing_summarize(datasetName)
  if(is.null(out_miss)) return(NULL)
  
  rowsToKeep <- rownames(out_dataset)
  colsToKeep <- colnames(out_dataset)
  nbRemainingWithMissings <- 0
  nbCells <- sum(out_miss$MissingByRows[,"n_miss"])
  if (direction == "rows") {
    #### Rows filtering ####
    orig_rows <- rownames(out_dataset)
    rowsToKeep <- out_miss$MissingByRows[out_miss$MissingByRows$pct_miss <= threshold,"obs"]
    nbRemoved <- nrow(out_miss$MissingByRows[out_miss$MissingByRows$pct_miss > threshold,])
    nbRemainingWithNA <- nrow(out_miss$MissingByRows[out_miss$MissingByRows$pct_miss <= threshold &
                                                               out_miss$MissingByRows$pct_miss >0,])
    nbCellsRemaining <- sum(out_miss$MissingByRows[out_miss$MissingByRows$pct_miss <= threshold,"n_miss"])
    # Reordering in original order
    rowsToKeep <- orig_rows[orig_rows %in% rowsToKeep]
    if (length(rowsToKeep) == 0) {
      message("W: All individuals (rows) have a percentage of missing values larger than ",
              threshold,
              "%. Impossible to remove all individuals.")
      return(NULL)
    } else if(length(rowsToKeep) == length(orig_rows)){
      message("W: No individual (row) has a percentage of missing values larger than ",
              threshold,
              "%. Removing nothing.")
      return(NULL)
    } 
  } else {
    #### Columns filtering ####
    orig_cols <- colnames(out_dataset)
    colsToKeep <- out_miss$MissingByColumns[out_miss$MissingByColumns$pct_miss <= threshold,"variable"]
    nbRemoved <- nrow(out_miss$MissingByColumns[out_miss$MissingByColumns$pct_miss > threshold,])
    nbRemainingWithNA <- nrow(out_miss$MissingByColumns[out_miss$MissingByColumns$pct_miss <= threshold &
                                                             out_miss$MissingByColumns$pct_miss >0,])
    nbCellsRemaining <- sum(out_miss$MissingByColumns[out_miss$MissingByColumns$pct_miss <= threshold,"n_miss"])
    # Reordering in original order
    colsToKeep <- orig_cols[orig_cols %in% colsToKeep]
    if (length(colsToKeep) == 0) {
      message("W: All variables (columns) have a percentage of missing values larger than ",
              threshold,
              "%. Impossible to remove all variables.")
      return(NULL)
    } else if(length(colsToKeep) == length(orig_cols)){
      message("W:No variable has a percentage of missing values larger than ",
              threshold,
              "%. Removing nothing.")
      return(NULL)
    } 
  }

  # Resulting matrix
  beforeMat <- 1*out_miss$MissingMatrix
  if(direction=="rows"){
    beforeMat[rowsToKeep,] <- 2 + beforeMat[rowsToKeep,]
  } else {
    beforeMat[,colsToKeep] <- 2 + beforeMat[,colsToKeep]
  }
  # No reduction done. The output can be all green or all red... change the summary func ?
  # if(max(dim(beforeMat)) > size){
  #   beforeMat <- r_redim_matrix(beforeMat, summary_func = function(x) min(x))
  # }
  colors = structure(c("#ff7c7c", "#880011", "#dfffdf", "#275227"),
                     names = c("0", "1", "2", "3")) # red, darkred, green, darkgreen

  tmpdir <- paste0(getwd(), "/images")
  if(!dir.exists(tmpdir)) dir.create(tmpdir, recursive = TRUE)
  beforeHeatmapFile <- tempfile(pattern="beforeHeatmap", fileext = ".png", tmpdir = tmpdir)

  dir_name <- ifelse(direction == "rows", "individuals", "variables")
  png(filename = beforeHeatmapFile, width = size, height = size, units = "px")
  ht <- ComplexHeatmap::Heatmap(
    beforeMat, 
    name = "Missingness heatmap before removal", 
    col = colors,
    heatmap_legend_param = list(
      labels = c("Removed", "Removed - missing", "Kept", "Kept - missing"),
      at = c("0", "1", "2", "3"),
      title="Cell status after removal",
      grid_height = unit(5, "mm"),
      grid_width = unit(5, "mm"),
      labels_gp = grid::gpar(fontsize = 10)),
    row_names_side = "left",
    column_names_side = "top",
    row_names_gp = grid::gpar(fontsize = 6),
    column_names_gp = grid::gpar(fontsize = 6),
    row_names_max_width = unit(1, "cm"),
    column_names_max_height = unit(1, "cm"),
    cluster_rows = FALSE,
    cluster_columns = FALSE,
    column_title = paste0(userName, ": original dataset showing", dir_name, "that will be removed"))
  ComplexHeatmap::draw(ht)
  ComplexHeatmap::decorate_heatmap_body("Missingness heatmap before removal", {
    grid::grid.text(paste0("Used threshold: ", threshold, "%"), 
                    unit(1.01, "npc"), unit(0.4, "npc"), just = "left", 
                    gp = grid::gpar(fontsize = 10, fontface = "bold"))
  })
  dev.off()

  # Constructing the summary output
  summaryRemove <- data.frame(
    "label" = c(paste("Number of", dir_name, "that will be removed"),
                paste("Number of", dir_name, "that will be kept"),
                paste("Number of", dir_name, "that will be kept with remaining NA's"),
                "Number of missings cells before removal",
                "Number of missing cells that will be kept"),
    "number" = c(nbRemoved,
                 ifelse(direction == "rows", length(rowsToKeep), length(colsToKeep)),
                 nbRemainingWithNA,
                 nbCells,
                 nbCellsRemaining)
  )
  descSummaryRemove <- data.frame(
                         "field" = c("label", "number"),
                         "label" = c("Indicator", "#"),
                         "labelShort" = c("Indicator", "#"),
                         "type" = c("string", "numeric"))
  titleSumaryRemove <- paste0("Statistics on data removal with current parameters for ", userName)
  statsRemove <- list("type" = "BasicTable",
                      "title" = titleSumaryRemove,
                      "data" = summaryRemove,
                      "fields" = descSummaryRemove,
                      "searchable" = FALSE)

  Output <- list(
    "Table" = list("rowsToKeep" = rowsToKeep,
                   "colsToKeep"= colsToKeep,
                   "statsRemove" = statsRemove),
    "Graphical" = list(
      "beforeHeatMap" = list("type" = "png", "path" = beforeHeatmapFile))
  )
  return(Output)
}

#' @param datasetName | Dataset name | character | required | NULL | Input
#' dataset on which remove missing values |
#' @param direction | direction to remove missing values | character | optional | "rows" |
#' If "rows" (resp. "columns"), the rows (resp. columns) with a percentage of missing
#' greater than the threshold will be removed from the dataset | [{"rows": "rows", "columns": "columns"}]
#' @param threshold | threshold on proportion of missings | numerical | optional |
#' [{"no threshold": 0}] | The rows (or columns) having a proportion of missing
#' values greater thant the threshold will be removed. | [0,100]
#'
#' @examples
#' input <- "proteins_MNAR.csv"
#' r_import(input, data.name = "proteins", header = TRUE, sep = " ",
#'          quote = '\"', dec = ".", row.names = 1, assign = TRUE)
#'
#' # Only numerical variables
#' ProteinsNoNA_rows <- r_missing_showremove("proteins", "rows", 0.2)
#' ProteinsNoNA_rows$Table$statsRemove$data
#' 
#' # Test with r_wrapp
#' tt <- r_wrapp("r_missing_showremove", "proteins", "rows", 0.2)
r_missing_showremove <- function(datasetName,
                                 direction = "rows",
                                 threshold = 0,
                                 size=800) {
  
  ## A. check if the analysis has already been performed
  func_args <- list("datasetName" = datasetName, "direction" = direction, "threshold" = threshold)
  check_exists <- r_call_datadb(func.name = "r_missing_showremove", 
                                func.args = func_args,
                                nature = "MissShowRemove")
  
  if(is.null(check_exists)){
    # Analysis doesn't exists
    showRemoveNA <- r_performs_showremove(datasetName, direction, threshold, size = size)
    if(is.null(showRemoveNA)){
      return(NULL)
    }
    object.name <- r_make_name(base = "MissShowRemove")
    showRemoveNA <- r_setattr_objects(showRemoveNA,
                                      object.name = object.name,
                                      nature = "MissShowRemove",
                                      func.args = func_args,
                                      origin.dataset = datasetName,
                                      type="hidden")
    
    Output <- list(Object = list("showRemove" = showRemoveNA),
                   Graphical = showRemoveNA$Graphical,
                   Table = list("statsRemove" = showRemoveNA$Table$statsRemove,
                                "ObjectName" = attr(showRemoveNA, "object_name")))

  } else {
    # Analysis already exists
    out_show_remove <- get(check_exists[[1]]$object_name)
    Output <-  list(
      Graphical = out_show_remove$Graphical,
      Table = list("statsRemove" = out_show_remove$Table$statsRemove,
                   "ObjectName" = check_exists[[1]]$object_name),
      Messages = check_exists[[1]]$messages
    )
  }
  return(Output)
}

#' @param datasetName | Dataset name | character | required | NULL | Input
#' dataset on which remove missing values |
#' @param direction | direction to remove missing values | character | optional | "rows" |
#' If "rows" (resp. "columns"), the rows (resp. columns) with a percentage of missing
#' greater than the threshold will be removed from the dataset | [{"rows": "rows", "columns": "columns"}]
#' @param threshold | threshold on proportion of missings | numerical | optional |
#' [{"no threshold": 0}] | The rows (or columns) having a proportion of missing
#' values greater thant the threshold will be removed. | [0,100]
#'
#' @examples
#' input <- "proteins_MNAR.csv"
#' r_import(input, data.name = "proteins", header = TRUE, sep = " ",
#'          quote = '\"', dec = ".", row.names = 1, assign = TRUE)
#'
#' # Only numerical variables
#' # Test with r_wrapp
#' tt2 <- r_wrapp("r_missing_showremove", "proteins", "rows", 0.3)
#' tt <- r_wrapp("r_missing_extractremove", "MissShowRemove_1")
r_missing_extractremove <- function(ObjectName, userName = NULL) {
  
  # Check if the user name can be used.
  if (!is.null(userName)) {
    # check that user_name is not already used
    suppressMessages({check_username <- r_call_datadb(user.name = userName)})
    if (length(check_username) > 1)
      stop("It seems that we have a problem: duplicated user names in workspace?")
    
    if (length(check_username) == 1) {
      message("E: This username is already used. Choose another one.")
      return(NULL)
    }
  }
  
  # Check if the data.frame already exists.
  parent <- get(ObjectName)
  attr(parent, "type") <- "analysis"
  func.args <-  list(objectName = ObjectName)
  check_exists <- r_call_datadb(func.name = "r_missing_extractremove", 
                                func.args = func.args)
  
  if(is.null(check_exists)){
    # Constructing the new data.frame
    datasetName <- attr(parent, "origin_dataset")
    dataRemovedNA <- get(datasetName)
    
    parentNature <- attr(dataRemovedNA, "nature")
    parentNormalized <- attr(dataRemovedNA, "normalized")
    parentLogt <- attr(dataRemovedNA, "logt")
    
    if(identical(parent$Table$rowsToKeep, rownames(dataRemovedNA)) &
       identical(parent$Table$colsToKeep, colnames(dataRemovedNA))){
      # Case where no rows or column have to be removed
      message("M: No individual (row) or variable (column) removed. No dataset created.")
      return(NULL)
    } else if(length(parent$Table$rowsToKeep) == 0 |
        length(parent$Table$colsToKeep) == 0){  
        # Case where no rows or column would be kept
          message("M: An empty dataset would be returned. No dataset created.")
          return(NULL)
    } else {
      dataRemovedNA <- dataRemovedNA[parent$Table$rowsToKeep, parent$Table$colsToKeep]
      dfName <- r_make_name(base = "MissRemoved")
      
      dataRemovedNA <- r_setattr_objects(dataRemovedNA,
                                         object.name = dfName,
                                         nature = parentNature,
                                         func.args = func.args,
                                         origin.dataset = datasetName,
                                         parent.edge = ObjectName,
                                         user.name = userName,
                                         normalized = parentNormalized,
                                         logt = parentLogt,
                                         type = "dataset")

      Output <- list(Object = list("showRemove" = parent, "DF" = dataRemovedNA),
                     Table = list("ObjectName" = attr(dataRemovedNA, "object_name"),
                                  "UserName" = attr(dataRemovedNA, "user_name")))
    }
  } else {
    # Case the data.frame already exists
    object <- get(check_exists[[1]]$object_name)
    # update userName
    if (attr(object, "user_name") != userName) {
      attr(object, "user_name") <- userName
      message(paste0("T: This dataset already existed. Its name has been",
                      "successfully changed to ", userName, "."))
      Output <- list(Object = list("showRemove" = parent, "DF" = object),
                     Table = list("ObjectName" = attr(object, "object_name"),
                                  "UserName" = attr(object, "user_name")))
    } else {
      Output <- list(Table = list("ObjectName" = attr(object, "object_name"),
                                  "UserName" = attr(object, "user_name")))
    }
  }
  return(Output)
}
