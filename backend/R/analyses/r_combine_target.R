#'@title Target variables and dataset preprocessing
#'@description Dataset preprocessing before running any study having a target variable.
#'
#' @param X | X dataset | dataset | required | NULL | X dataset |
#' @param Y | Y dataset | dataset | required | NULL | Y dataset |
#' @param target | Target variable | character | required | NULL | Feature from Y
#' whose values are to be modeled  |
#'
#' @return Given two datasets, a list composed of 
#' \itemize{
#'   \item Graphical - a list of graphical outputs: 
#'     \code{UpSet}, An UpSet plot, 
#'     \code{VennPlot}, A Venn Diagram if the number of datasets is less than 4.
#'   \item Table - a list of descriptive statistics before and after data 
#'   filtering:
#'     \code{dataInfoBefore}: general information on datasets before filtering
#'     based on common rows,
#'     \code{dataInfoAfter}: general information on datasets after filtering
#'     based on common rows (only provided if not all rows were common in the
#'     input of the analysis),
#'     \code{ObjectName} Name of the complex output object as referenced in
#'     the object database.
#'   \item Object: a list containing a complex object \code{COMobj}, also a list
#'   usable by the users with the following entries:
#'     \code dataGenInfo: a table that contains general information on number of
#'     variables and individuals, as well as types of variables for each input
#'     dataset,
#'     \code{Individuals}: names of individuals kept for integrative analyses,
#'     \code{TargetInfo}: a vector containing information (name and dataset) about the target variable,  
#'     \code{Graphical} and \code{Table}, previously described, except for the
#'     entry \code{ObjectName}.
#' }
#'
#' @examples
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "clinical_MCAR.csv"
#' r_import(input, data.name = "clinicalMCAR", header = TRUE, sep = " ",
#'          quote = '\"', dec = ".", row.names = 1, assign = TRUE)
#' 
#' out_combtgt <- r_wrapp("r_combine_target", X = "mrna", Y = "clinicalMCAR",
#'                        target = "patient.gender")
#' 
#' out_comb2 <- r_combine_target("mrna", "clinicalMCAR", target = "patient.gender")
#' jsonlite::toJSON(out_comb2$Graphical$UpsetPlot, pretty = TRUE, auto_unbox = TRUE)
#' 
r_combine_target <- function(X, Y, target) {
  
  # Step 1: Initialization
  X <- unlist(X)
  Y <- unlist(Y)
  target <- unlist(target)
  nature <- "multipleDA"
  
  # Step 2: Check if the combination has already been done
  func_args <- list("X" = X, "Y" = Y,  "target" = target)
  check_exists <- r_call_datadb(func.name = "r_combine_target", 
                                func.args = func_args)
  
  # Step 3: Perform the combination 
  ## A. If the combination has already been performed
  if (is.null(check_exists)) { 
    
    Output <- target_combanalysis(X, Y, target)
    
    # Case of no common rownames (already handled by r_combination_analysis)
    if(is.null(Output)){
      return(NULL)
    }
    info_name <- r_make_name(nature)
    Output$Object$COMobj <- r_setattr_objects(Output$Object$COMobj, 
                                              nature = nature,
                                              object.name = info_name,
                                              origin.dataset = c(X, Y),
                                              type = "combined",
                                              func.args = func_args)

  } else { ## B. If the combination has not been performed yet
    
    res_COMtarget <- get(names(check_exists)[1])
    info_name <- attr(res_COMtarget, "object_name")
    Output <- list(
      Table = res_COMtarget$Table,
      Graphical = res_COMtarget$Graphical, 
      Messages = check_exists[[1]]$messages
    )
  }
  
  ## C. Output
  Output$Table$ObjectName <- info_name
  
  return(Output)
}

########################## AUXILIARY FUNCTIONS ##############################

target_combanalysis <- function(X, Y, target) {
  
  dataX <- get(X)
  dataY <- get(Y)
  
  TargetInfo <- c("X" = X, "Y" = Y, "target" = target)
  
  # Step 1: Check if the target variable...
  
  ## ...belongs to Y dataset
  if (!target %in% colnames(dataY)) 
    stop("The target variable can't be found in Y dataset.")
  
  target <- dataY[, target]
  
  ## ...is categorical
  if (!(is.factor(target) || is.character(target))) {
    message("M: The target variable must be categorical.")
  }
  if (is.character(target)) target <- as.factor(target)
  
  ## ...has more than one level
  if (length(levels(target)) == 1) {
    stop("The target variable must have at least 2 levels.")
  }
  
  # Step 2: Combination
  datasetList <- list(dataX, dataY)
  names(datasetList) <- c(X, Y)
  Output <- r_combination_analysis(datasetList)
  # Case of no common rownames (already handled by r_combination_analysis)
  if(is.null(Output)){
    return(NULL)
  }
  ## Remove from common individuals any missing values that have been found in
  ## the target variable
  missing_target <- is.na(target)
  if (any(missing_target)) {
    Individuals <- Output$Object$COMobj$Individuals
    Individuals <- Individuals[!(Individuals %in% rownames(dataY)[missing_target])]
    Output$Object$COMobj$Individuals <- Individuals
    if(sum(missing_target) > 1) {
      rmessage <- paste0("M: The target variable contains ",
                         sum(missing_target), " missing values.\n",
                         "They have been removed from common individuals.")
    } else {
      rmessage <- paste0("M: The target variable contains one missing value.\n",
                         "It has been removed from common individuals.")
    }
    message(rmessage)
  }
  
  # Step 3: Add information about the target
  Output$Object$COMobj[["TargetInfo"]] <- TargetInfo
  
  return(Output)
}
