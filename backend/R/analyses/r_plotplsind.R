# Plot individuals after a PLS/PLS-DA (coordinates)
#' @title Plot individuals after a PLS (coordinates)
#' @description Display the projections of the individuals on PC after a PLS 
#'
#' @param datasetName | Name of input object | character | required | NULL | 
#' Name of input object obtained from a PLS/PLS-DA |
#' @param axis1 | x axis | integer | required | 1 | Numbers of the PC to display
#' on the x axis |
#' @param axis2 | y axis | integer | required | 2 | Numbers of the PC to display
#' on the y axis |
#' @param datasetcolor |Dataset for color | data.frame | optional | NULL | Name
#' of the dataset containing the variable for the color of dots | 
#' @param varcolor | Variable for color | character | optional | NULL | Name of 
#' the variable to use for the color of dots |
#' @param datasetshape | Dataset for shape | data.frame | optional | NULL | Name
#' of the dataset containing the variable for the shape of dots | 
#' @param varshape | Variable for shape | character | optional | NULL | Name of 
#' the variable to use for the shape of dots |
#' @param datasetsize | Dataset for size | data.frame | optional | NULL | Name 
#' of the dataset containing the variable for the size of dots | 
#' @param varsize | Variable for size | character | optional | NULL | Name of 
#' the variable to use for the size of dots |
#'
#' @return a list composed of
#' \itemize{
#'   \item Graphical - \code{PlotInd}, the plot
#'   of the projections of the individuals on the selected PCs
#' }
#'
#' @examples
#' input <- "mrna.csv"
#' r_import(input, data.name = "mrna", row.names = 1, assign = TRUE)
#' input <- "protein.csv"
#' r_import(input, data.name = "proteins", sep = " ", row.names = 1, 
#'          assign = TRUE)
#' input <- "clinical.csv"
#' r_import(input, data.name = "clinical", row.names = 1, assign = TRUE)
#' 
#' # PLS          
#' out_combdt <- r_wrapp("r_combine_datasets", list("proteins", "mrna"))
#' out_pls <- r_wrapp("r_pls", "combinedDF_1", ncp = 5)
#'                    
#' out_pls_plotind <- r_plotplsind("PLSobj_1", 
#'                                 axis1 = 1, axis2 = 2,
#'                                 datasetcolor = "clinical", 
#'                                 varcolor = "patient.gender",
#'                                 datasetsize = "clinical", 
#'                                 varsize = "patient.age_at_initial_pathologic_diagnosis")
#' out_pls_plotind$Graphical$PlotInd
#' 
#' # PLS-DA
#' out_combtg <- r_wrapp("r_combine_target", X = "mrna", Y = "clinical", 
#'                      target = "patient.vital_status")
#' out_plsda <- r_wrapp("r_plsda", "multipleDA_1", ncp = 5, seed = 5)
#' 
#' out_plsda_defplot <- r_plotplsind("PLSDAobj_1", axis1 = 1, axis2 = 2)
#' plotly::ggplotly(out_plsda_defplot$Graphical$PlotInd)
#' 
#' out_plsda_plotind <- r_plotplsind("PLSDAobj_1",
#'                                   axis1 = 1, axis2 = 2,
#'                                   datasetcolor = "clinical", 
#'                                   varcolor = "patient.gender",
#'                                   datasetsize = "clinical", 
#'                                   varsize = "patient.samples.sample.2.portions.portion.analytes.analyte.aliquots.aliquot.volume")
#' plotly::ggplotly(out_plsda_plotind$Graphical$PlotInd)

r_plotplsind <- function(datasetName, axis1 = 1, axis2 = 2,  
                         datasetcolor = NULL, varcolor = NULL, 
                         datasetshape = NULL, varshape = NULL, 
                         datasetsize = NULL, varsize = NULL) {
  
  object <- get(datasetName)
  analysis <- gsub("[^::A-Z::]","", attr(object, "nature"))
  
  if (analysis == "PLS") {
    mode <- object$mode
    if (mode == "canonical") {
      dataset <- object$variates$X[, c(axis1, axis2)] + 
        object$variates$Y[, c(axis1, axis2)]
      dataset <- data.frame(dataset / 2)
    } else {
      dataset <- data.frame(object$variates$X[, c(axis1, axis2)])
      
      if (is.null(datasetcolor) || (is.null(datasetsize) && length(object$names$colnames$Y) > 1)) {
        
        parent_edge <- get(attr(object, "parent_edge"))
        datasetList <- suppressMessages(r_clean_plsdatainput(object = parent_edge))
        ind_datasetY <- ifelse(ncol(datasetList[[2]]) <= 2, 2, 1)
        datasetY <- parent_edge$dataGenInfo$internal_name[ind_datasetY]
        varsY <- colnames(datasetList[[ind_datasetY]])
        if (is.null(datasetcolor)) {
          datasetcolor <- datasetY
          varcolor <- varsY[1]
        }
        if (is.null(datasetsize) && length(varsY) > 1) {
          datasetsize <- datasetY
          varsize <- varsY[2]
        } 
        
      }
    }
  } else { # Not PLS
    dataset <- data.frame(object$variates$X[, c(axis1, axis2)])
    if (is.null(datasetcolor)) {
      # Put default color as target variable
      parent_edge <- get(attr(object, "parent_edge"))
      datasetcolor <- parent_edge$dataGenInfo$internal_name[2]
      varcolor <- parent_edge$TargetInfo[["target"]]
    }
  }
  
  add_Ychar <- ifelse(analysis == "PLS" && mode == "canonical", "Y", "")
  xlab <- paste0("X", add_Ychar, "-variate ", axis1)
  ylab <- paste0("X", add_Ychar, "-variate ", axis2)
  data.name <- attr(object, "origin_dataset")
  data.name <- sapply(data.name, r_get_username)
  
  if (analysis == "PLSDA") {
    origin_obj <- get(attr(object, "func_args")$datasetName)
    target.name <- origin_obj$TargetInfo[["target"]]
    datasets_in_title <- paste(data.name, collapse = " predicts ")
    datasets_in_title <- paste0(datasets_in_title, ": ", target.name)
    ptitle <- paste0(datasets_in_title, " - PLS-DA: projection of individuals")
  } else {
    datasets_in_title <- paste(data.name, collapse = ", ")
    ptitle <- paste0(datasets_in_title, " - PLS: X", add_Ychar, "-space")
  }
  
  dims <- paste0("Dims.", c(axis1, axis2))
  colnames(dataset) <- dims
  
  # produce plotly directly
  p <- r_multivariate_dotplot(dataset, dims[1], dataset, dims[2],
                              datasetcolor = datasetcolor, varcolor = varcolor,
                              datasetshape = datasetshape, varshape = varshape,
                              datasetsize = datasetsize, varsize = varsize,
                              xlab = xlab, ylab = ylab, title = ptitle, 
                              axes0 = TRUE)
  if(is.null(p)) return(NULL)
  p <- p$Graphical$Dotplot

  p$x$layout$title <- list(text = ptitle, x = 0, xref = "paper", 
                           font = list(size = 1.2 * p$x$layout$font$size))
  p$x$layout$xaxis$title$text <- xlab
  p$x$layout$yaxis$title$text <- ylab
  
  Output <- list("Graphical" = list("PlotInd" = p))
  
  return(Output)
}
