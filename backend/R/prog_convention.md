# Conventions pour la programmation

- Pas de listes de listes en sortie de fonction stats : conversion json non 
prévue. Les sorties prévues sont pour l'instant : Numerical (voué à disparaître
probablement), Graphical (des graphiques, de préférences en ggplot2, qui sont
plotlyables), Table (des tableaux à afficher tel quels, à exporter au format
matrix plutôt que data.frame), Object (des objets R complexes)

- Dans les fonctions, les arguments qui pointent vers des datasets doivent 
commencer par `dataset`...

ex. dataset.name.1 = "truc"
ex. dataset.name.2 = "mrna"
ex. dataset1 = "machin"

- Ne pas utiliser de "print" pour les messages à destination de l'interface. 
Utiliser la fonction "message" à la place (ou "stop" ou "warning")

### Paramètres des fonctions

- Utiliser la nomenclature Roxygen (depuis la fonction : code > inser Roxygen Skeleton)

- Dans la documentation, la description des paramètres (ici "variable") doit être du type

    @param variable | displayName | type | required or optional | NULL or default value | help | choices (if any) 

où displayName est le nom qui sera affiché à l'écran (compréhensible pour 
l'utilisateur), type est le type tel que retourné par class() (ou éventuellement
"vector of ..."), "required." est à ajouter lorsque la fonction retournera une
erreur si l'argument n'est pas fourni (not required dans le cas inverse),
"no default" quand il n'est pas nécessaire de préciser une valeur par défaut
et préciser la valeur par défaut sinon, help est l'aide compréhensible pour 
l'utilisateur, [{"Comma": ",", "Semi column": ";", "Tab": "\t"}] sont les choix 
possibles du paramètre
si les choix sont tous les noms de colonnes ou de lignes d'un dataset, il faut
écrire dataset1.colnames ou dataset.rownames

ex. pour le paramètre "sep" de la fonction d'import
@param sep | field separator | character | not required | "," | field separator in your text file (only one character) | [{"Comma": ",", "Semi column": ";", "Tab": "\t"}]

- Le nom utilisé pour la fonction est consistant exactement avec le nom utilisé
pour le fichier qui la contient.

- appeler les fonctions des packages par PKG::FUNCTION et pas directement par
FUNCTION sauf pour les fonctions de ggplot2

### Copies d'objets

Dans une fonction, si les objets appelés sont modifiés, ils sont copiés (ce uqi peut devenir lourd en mémoire). 
Bonne pratique : 
Sur une fonction qui travaille sur des variables (ex. r_multivariate_dotplot), extraire directement les variables nécessaires en début de fonction et ne modifier que les vairables si besoin de modifier. 

Exemple : 
au lieu de 

    ex_function <- function(dataset, varname){
       dataset[,varname] <- as.factor(dataset[,varname)])
   
Faire : 

    ex_function <- function(dataset, varname){
       var <- dataset[,varname]
       var <- as.factor(var)
    }

Ainsi le dataset n'est pas copié en entier !

### Format d'outputs

Pour les outputs de type table (tableau, matrice, data.frame, voire vecteur avec des noms) : 
A mettre dans output$Table.

- si noms de lignes et/ou noms de colonnes à faire passer : il faut du data.frame obligatoirement
- si pas de noms de lignes ou de colonnes, matrix c'est OK

S'assurer que la classe de l'output est bien data.frame ou matrix

Sur les exemples lancer les codes suivants :

    lapply(nomoutput$Table, class)

Vérifier que c'est data.frame ou matrix (rien d'autre)

    lapply(nomoutput$Table$Table, jsonlite::toJSON)

Vérifier que dans la sortie du toJSON toutes les informations voulues y sont : noms de ligens, colonnes, ...


