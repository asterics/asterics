export const table = {
  data () {
    return {
      printLen: 20,
      currentPage: 1,
      perPage: 8
    }
  },
  methods: {
    getCsv: function () {
    
      let rowHeader =  [];
      this.data.fields.forEach(field => {
            rowHeader.push(field.label)
          })
      let csvContent = rowHeader.join(',') + '\r\n';

      for (let i = 0; i < this.data.data.length; i++) {
          let rowData = [];
          //traverse the table columns and create comma separeted columns
          this.data.fields.forEach(field => {
            let fieldname = field.field
            let cell = this.data.data[i][fieldname]
            rowData.push(cell)
          })
          //add line feed to complete a row
          csvContent += rowData.join(',') + '\r\n';
      }
      const blob = new Blob([csvContent], { type: 'text/csv' });
      //create a url to link to csv content blob
      const url = URL.createObjectURL(blob);
      //add the anchor to document, so it can be clicked
      this.$refs.export.setAttribute('href', url)
      this.$refs.export.click()
    }
  },
  computed: {
    isPaginated () {
      return (this.data.data.length > this.perPage)
    },
    sortable: function () {
      if (!('sortable' in this.data)) {
        return true
      }
      return this.data.sortable
    },
    searchable: function () {
      if (!('searchable' in this.data)) {
        return true
      }
      return this.data.searchable
    },
    height () {
      let val = 100
      if (this.searchable )  {
        val = val + 30
      }
      if (this.isPaginated ) {
        val = val + this.perPage * 33
      }      
      return val
    }
  }  
}
