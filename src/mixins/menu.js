
import { useMenuStore } from '@/stores/menu'

export const menu = {
  setup () {
    const menuStore = useMenuStore()
    return { menuStore }
  },
  computed: {
    selectedItem: function () {
      return this.menuStore.selectedItem
    }
  },
  methods: {
    setMenuItems: function (items) {
      this.menuStore.setMenuItems(items)
    },
    selectItem: function () {
      return this.menuStore.selectedItem
    }
  },
    mounted () {
    this.setMenuItems(this.menuItems)
  }
}
