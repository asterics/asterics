export let uppercase = (string) => string.toUpperCase();

export let truncateentry = (value, end) => {
  if (value.length > end + 3) {
    return (value.substring(0, end) + '...');
  } else {
    return value;
  }
};

