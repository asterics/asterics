
const SERVER_URL = import.meta.env.MODE === 'development' ? 'http://localhost:5000' : ''
const ADD_DATASET_URL = import.meta.env.MODE === 'development' ? 'http://localhost:5000/api/add_dataset' : '/api/add_dataset'
const API_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:5000/api' : '/api'
const SAVE_FILE_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:5000/api/save_file' : '/api/save_file'
export default { API_URL, ADD_DATASET_URL, SAVE_FILE_URL, SERVER_URL }