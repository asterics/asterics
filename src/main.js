
/* Styles */

import Buefy from '@ntohq/buefy-next'
import '@/scss/main.scss'
import '@/css/style.css'

/* Core */
import { createApp, h } from "vue";

import App from './App.vue'

/* Router & Store */
import router from './router'
import { createPinia } from 'pinia'

/* Vue. Main component */
import Vue3Progress from "vue3-progress";

// Filters

import {uppercase, truncateentry} from "@/filters/strings.js";
import { scientific } from '@/filters/scientific.js'

// Vee-validate globals rules
import './plugins/rules'


const optionsProgress = {
  position: "fixed",
  height: "3px",
  color: "#ffffff",
};

const app = createApp({
  render: () => h(App)
});


app.use(createPinia())
app.use(router)
app.use(Buefy, {
  defaultIconPack: 'fa',
  customIconPacks: {
    'icomoon': {
      sizes: {
        'default': 'is-size-5',
        'is-small': '',
        'is-medium': 'is-size-3',
        'is-large': 'is-size-1'
      },
      iconPrefix: 'icon-',
      internalIcons: {
        'xaxis': 'xaxis',
        'yaxis': 'yaxis',
        'color': 'color',
        'size': 'size',
        'shape': 'shape',
        'arrow-down': 'arrow-down',
        'correlation': 'correlation',
        'basic-test': 'basic-test',
        'cross-table': 'cross-table'
      }
    }
  }
})

// Vue3 filters
app.config.globalProperties.$filters = {
  uppercase: uppercase,
  truncateEntry: truncateentry,
  scientific: scientific
};
app.use(Vue3Progress, optionsProgress)
app.mount('#app')
