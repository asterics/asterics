import { defineRule } from 'vee-validate';
import { required, email, min, integer ,not_one_of, one_of, between} from '@vee-validate/rules';
import { configure  } from 'vee-validate';
import { localize } from "@vee-validate/i18n";
import apiService from '@/services/apiService';

defineRule('required', required);
defineRule('email', email);
defineRule('min', min);
defineRule('integer', integer);
defineRule('between', between);
defineRule('not_one_of', not_one_of);
defineRule('one_of', one_of);


defineRule('min_max', (value, [min, max]) => {
  // The field is empty so it should pass
  if (!value || !value.length) {
    return true;
  }
  const numericValue = Number(value);
  if (! (numericValue <= max && numericValue >= min)) {
    return `This field must be between [ ${min}, ${max} ]`;
  }
  return true;
});


defineRule('datasetname_valid', (value, [datasetNames]) => {
    if (! value.match(/^[a-zA-Z0-9_]*$/ ) ){
      return ('The field value is not accepted [alphanumeric caracters and \'_\' only].')
    }
    if (value.match(/^\d/) ){
      return ('The field value is not accepted [must not starts with a number].')
    }
    if (value.length > 30) {
      return ('The field value is too long.')
    }
    if (datasetNames && datasetNames.includes(value)) {
      return ('Dataset name already exists')
    }
    return true
  });

  defineRule('authorize_name', async function (value){
    let res = await apiService.runRFunction({
      'func_name': 'r_forbidden_name',
      'username': value
    })
    if (res && res.forbidden) {
      return res.message
    }
    return true
  });



configure({
  generateMessage: localize("en", {
    messages: {
      required: "This field is required",
      email: "Please enter a valid email {field}",
      min: "Lower than min value",
      between: "Expect value [ ${min} , ${max} ] ",
    },
  })
});