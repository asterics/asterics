The ASTERICS team (2020-2023)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Élise Maigné <elise.maigne@inrae.fr>
Céline Noirot <celine.noirot@inrae.fr>
Julien Henry <julien.henry@inrae.fr>
Yaa Adu Kesewaah <yadukesewaah@gmail.com>
Ludovic Badin <ludovic.badin@hyphen-stat.com>
Sébastien Déjean <sebastien.dejean@math.univ-toulouse.fr>
Camille Guilmineau <camille.guilmineau@inrae.fr>
Arielle Krebs <krebs.arielle@gmail.com>
Fanny Mathevet <fanny.mathevet@gmail.com>
Audrey Segalini <audrey.segalini@hyphen-stat.com>
Laurent Thomassin <laurent.thomassin@hyphen-stat.com>
David Colongo <david.colongo@hyphen-stat.com>
Christine Gaspin <christine.gaspin@inrae.fr>
Laurence Liaubet <laurence.liaubet@inrae.fr>
Nathalie Vialaneix <nathalie.vialaneix@inrae.fr>
	
