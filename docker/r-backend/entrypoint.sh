#!/bin/bash
echo "Launch Rserve"
sed -e "s/STORAGE_DIR.*/STORAGE_DIR = \'\/home\/asterics\/data\'/" -e "s/RSERVE_HOST.*/RSERVE_HOST = 'r-backend'/" application.overwrite.cfg > application.cfg

R CMD /home/asterics/app/renv/library/R-4.3/x86_64-pc-linux-gnu/Rserve/libs/Rserve  --vanilla --no-save --RS-conf /etc/Rserv.conf > /home/asterics/log/Rserve.log
