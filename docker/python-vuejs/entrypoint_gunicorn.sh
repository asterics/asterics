#!/bin/bash
echo "## Launch service cron"
cron

echo "## Launch web server via gunicorn"
sed -e "s/STORAGE_DIR.*/STORAGE_DIR = \'\/home\/asterics\/data\'/" -e "s/RSERVE_HOST.*/RSERVE_HOST = 'r-backend'/" application.overwrite.cfg > application.cfg
export LANG=C.UTF8 

echo "## Configuration used :"
cat application.cfg

echo "## Launch server :"
. /home/asterics/app/envasterics/bin/activate && gunicorn --bind 0.0.0.0:5000 --timeout 600  asterics:app
