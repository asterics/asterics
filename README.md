# ASTERICS: Installation instruction and legal information

## License

ASTERICS source code is distributed under the 
[GPL3 license](https://forgemia.inra.fr/asterics/asterics/-/blob/master/LICENSE). 

In addition, up to some exceptions listed below, you can reuse the general 
structure of the website, the content, the text and the images displayed on the 
website/application according to the terms of the 
[Etalab 2.0 open licence](https://www.etalab.gouv.fr/licence-ouverte-open-licence). 
ASTERICS characters (
[here](https://forgemia.inra.fr/asterics/asterics/-/blob/master/src/assets/img/biologiste-edit.svg)
and
[here](https://forgemia.inra.fr/asterics/asterics/-/blob/master/src/assets/img/help/explore/bio-variables-at-glance.png))
have been created by [Camille Ferrari](https://camilleferrari.com/)). Please 
acknowledge her work when using these images.

INRAE, as the publisher of this website, owns the trademarks and logos on the 
site unless otherwise stipulated. Consequently, you must not reuse the said 
trademarks, logos, and images without prior written authorisation from INRAE. 
Other content may also be subject to intellectual property rights.

## Technical documentation

A technical documentation (for developers) can be found [here](https://asterics.pages.mia.inra.fr/doc_asterics/).

## Installation instructions

You can install ASTERICS in local mode with Python and Rserve or via Docker.


### Docker installation (recommended)

#### Pre-requisites

* docker
* docker-compose
* an access to a mail server to send emails (might be skipped for local use)
* internet connexion

#### Configuration

Copy (locally) and edit the file `application.sample.cfg` found in ASTERICS 
repository

```
wget https://forgemia.inra.fr/asterics/asterics/-/raw/master/application.sample.cfg
```

and:

* add configuration information for the email server
* remove or set the file size limit as you wish

In addition, create data directories:
```
mkdir data_web/ log_web/
```

All further command lines must be run from the directory where these two
directories have been created and in which the file `application.sample.cfg`
must have been copied.


##### Run docker locally

The docker-compose file used to launch ASTERICS locally is `docker-compose.yml`.
Copy this file locally from ASTERICS repository

```
wget https://forgemia.inra.fr/asterics/asterics/-/raw/master/docker-compose.yml
```

and use it with:

* Add your user in docker group (not required but it is a good practice): 
```
sudo usermod -aG docker ${USER}̀
```

* Pull the latest docker containers from our registry (this is advised only if 
you want to update a previously installed version of the containers; otherwise,
for a fresh install, the next step will automatically perform the pull):
```
docker pull registry.forgemia.inra.fr/asterics/asterics/python-vuejs:latest
docker pull registry.forgemia.inra.fr/asterics/asterics/r-backend:latest
docker pull registry.forgemia.inra.fr/asterics/asterics/nginx:latest
```

* Launch ASTERICS with docker-compose (from the directory where the file 
`docker-compose.yml` has been copied):
```
sudo docker-compose up -d
```
Compiled images are retrieved from our registry and used to deploy ASTERICS.

* Check that the 2 containers are running properly:
```
sudo docker ps
```

The application is available at http://localhost:8080 .


##### Deploy ASTERICS on a server with docker and nginx

The docker-compose file used to launch ASTERICS on a server using nginx is 
`docker-compose.nginx.yml`. Copy this file locally from ASTERICS repository

```
wget https://forgemia.inra.fr/asterics/asterics/-/raw/master/docker-compose.nginx.yml
```

as well as the file ` docker/nginx/nginx-ssl.conf`

```
wget https://forgemia.inra.fr/asterics/asterics/-/raw/master/docker/nginx/nginx-ssl.conf
```

and use them with:

* Edit the file `docker-compose.nginx.yml` it to adapt to your server 
configuration (especially if you want to use ssl)

* Edit the file ` docker/nginx/nginx-ssl.conf` to adapt it to your needs 
(depending to the fact that you want to use ssl or not and also to set the
maximum size for uploads)

* Pull the latest docker containers from our registry (this is advised only if 
you want to update a previously installed version of the containers; otherwise,
for a fresh install, the next step will automatically perform the pull):
```
docker pull registry.forgemia.inra.fr/asterics/asterics/python-vuejs:latest
docker pull registry.forgemia.inra.fr/asterics/asterics/r-backend:latest
docker pull registry.forgemia.inra.fr/asterics/asterics/nginx:latest
```

* Launch ASTERICS with docker-compose (from the directory where the files have
been copied)
```
sudo docker-compose -f docker-compose.nginx.yml up -d
```
* Check that the 3 containers are running properly:
```
sudo docker ps
```

### Installation from sources

#### Pre-requisites

* git
* node & npm
* Python 3 (tested with v. 3.8)
* R v. 4.0.3
* orca v. 1.3.1 (on headless servers, orca has to be installed to be run 
through svfb; we detail the installation using this method here).

Detailed installation is described for Ubuntu 22.04 LTS.

#### Installation of required Ubuntu packages

List of Ubuntu required packages (to be used with SSL configuration): 

```
sudo apt install libigraph-dev build-essential python-dev-is-python3 libxml2 \  
  libxml2-dev zlib1g-dev zip unzip libpaper-utils xdg-utils libblas3 \ 
  libbz2-1.0 libc6 libcairo2 libcurl4 libglib2.0-0 libgomp1 libicu70 libjpeg8 \ 
  liblapack3 liblzma5 libpango-1.0-0 libpangocairo-1.0-0 libpcre2-8-0 libpng \ 
  libreadline8 libtcl8.6 libtiff5 libtirpc3 libtk8.6 libx11-6 libxt6 zlib1g \ 
  ucf ca-certificates xvfb libnss3 librust-gdk-pixbuf-sys-dev libgtk-3-0 \ 
  libxtst6 libasound2 pip bison flex pandoc
```

#### Orca installation

Download the AppImage of orca at: https://github.com/plotly/orca/releases/ ,
make it executable and extract it:
```
chmod 777 orca-1.3.1.AppImage
./orca-1.3.1.AppImage --appimage-extract
```

The uncompressed directory `squashfs-root` can be moved if needed. Supposed it 
is moved into `/home/asterics/.local/share/orca`. Make it executable with:
```
chmod 777 /home/asterics/.local/share/orca/
```

Create a file `orca` in your `$PATH` (e.g., `/usr/bin/orca`) and edit it with:
```
#!/bin/bash

xvfb-run --auto-servernum --server-args "-screen 0 640x480x24" /home/asterics/.local/share/orca/orca --no-sandbox "$@"
```
Finally make this file executable as well and test orca with:
```
orca --help
```

#### Retrieve sources and configure

Retrieve sources from ASTERICS repository: 
```
git clone https://forgemia.inra.fr/asterics/asterics.git
cd asterics
```
and copy the files `application.sample.cfg` and `asterics.sample.R` to,
respectively, `application.cfg` and `asterics.R`. Edit these two files to adapt
them to your needs (in particular, if you installed orca as described in the
previous section, `asterics.R` must contain `ORCA_MODE <- "svfb"`; you can 
disable orca and thus the analysis previous in the menu Workspace of ASTERICS
with `ORCA_MODE <- "none"`).

The remaining commands must be run from within the directory `asterics`.


#### Configure cron (not mandatory)

ASTERICS includes an auto-cleaning script that deletes old repository on a 
regular basis. It is activated using [CRON](https://en.wikipedia.org/wiki/Cron)
with a entry similar to the one available at

[docker/python-vuejs/crontab.txt](https://forgemia.inra.fr/asterics/asterics/-/blob/master/docker/python-vuejs/crontab.txt)

Adapt it to your needs and include it in your server CRON configuration.


#### Create the virtualenv with python __3.8__ and dependencies

```
python3.8 -m venv --clear --without-pip ./envasterics
source envasterics/bin/activate 
pip install --upgrade pip setuptools
pip install -r requirements.txt


```

#### Install npm dependencies

```
npm install
```

#### Install R v. 4.0.3 and Rserver

Install R version 4.0.3. You can use (for instance) 
[this tutorial](https://docs.rstudio.com/resources/install-r/) and make a
symbolic link pointing to `/usr/bin/R`.

R dependencies are to be installed using `renv`. Within R, use:
```
> install.packages("renv")
> renv::init()
```
This installation is long (at least one hour).

#### Compile and minify for flask

** Use Node v18 **

Need node > 18
```
nvm install v18.18.0
```

If allready installed 
```
nvm use v18.18.0
```

**Build front-end source**
```
npm run build
```

**Launch Python server**
```
source envasterics/bin/activate
FLASK_APP=asterics.py flask run --port=80 --host=<IP>
```
where `<IP>` has to be adapted to your case.

**Launch Rserve**
```
R CMD ./renv/library/R-4.3/x86_64-pc-linux-gnu/Rserve/libs/Rserve --RS-conf ./backend/R/Rserv.conf
```


## Additional instructions for the development team

### Run ASTERICS with full hot-reloads 

**Configure application**
```
cp application.sample.cfg application.cfg
cp asterics.sample.R asterics.R
```
Edit and set email parameters if needed.

**Launch front-end server**
```
npm run dev
```
OR
```
npm run debug
```


**Launch Python server**
```
source envasterics/bin/activate
FLASK_APP=asterics.py flask run --debug
```

**Launch Rserve**
```
R CMD ./renv/library/R-4.3/x86_64-pc-linux-gnu/Rserve/libs/Rserve --RS-conf ./backend/R/Rserv.conf
```


**Launch E2E test**
Need nvm (to handle node versions), node > 18 and playwright with deps

To install nvm : https://snowpact.com/blog/nvm-guide-d-installation

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

```
nvm install v18.18.0
```

```
npx playwright install --with-deps
```

Launch playwright test for VueJS part 
```
npx playwright test  --project firefox
```

To launch tests only on firefox (to start c'est pas mal)
```
npx playwright test --project firefox
```
La configuration des tests est dans `./playwright.config.ts`



To launch tests only on firefoxand in ui mode (to debug)
```
npx playwright test --ui --project firefox
```

To launch only one test : (be careful some tests are interdependents)
```
npx playwright test --ui --project firefox -g "Edit tests"
```


To generate tests, use tool codegen to generate code :

```
npx playwright codegen
```
