import { test, expect } from '@playwright/test'

test.describe.configure({ mode: 'serial' })
test.setTimeout(120000)

test.describe('Explore tests', () => {
  let page
  let url
  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage()
    await page.goto('./')
    await page.getByPlaceholder('Email').click()
    await page.getByPlaceholder('Email').fill('celine.noirot@inrae.fr')
    await page.getByPlaceholder('Workspace name').fill('DEMO')
    await page.locator('label').filter({ hasText: 'Load TCGA demo data into your workspace' }).locator('span').first().click()
    await page.locator('label').filter({ hasText: 'I confirm that I have read and that I agree to Asterics\' Privacy Policy.' }).locator('span').first().click()
    await page.getByRole('button', { name: 'Create' }).click()
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 })
    url = page.url()
    console.log('Before all create workspace test at ' + page.url())
  })

  test.beforeEach(async ({}, testInfo) => {
    console.log(`Running ${testInfo.title}`)
    await page.goto(url)
    await page.once('load', () => console.log('Page loaded!'));
  })

  test('Explore variables', async () => {
    await new Promise(r => setTimeout(r, 2000));
    await page.goto('./explore/variate')
    // 1 VARIABLE
    let divariate=page.locator('#univariate')
    await divariate.getByRole('button', { name: 'Dataset' }).click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).click()
    await divariate.getByPlaceholder('Select a variable').click()
    await divariate.getByRole('button', { name: '4E.BP1', exact: true }).click()
    await divariate.getByRole('button', { name: 'Explore' }).click()
    await expect(divariate.getByText(/PlotsSummaryNormality test/), 'Explore variable - One variable - Expect 3 tabs in results.').toBeVisible({ timeout: 20000 })
    await divariate.locator('#summary-label').click()
    await divariate.locator('#plots-label').click()
    // 2 VARIABLES
    await page.getByText('2 variables', { exact: true }).click()
    divariate=page.locator('#bivariate')
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'mrna2000 variables' }).first().click()
    await divariate.getByPlaceholder('Select a variable').first().click()
    await divariate.getByRole('button', { name: 'APOD' }).click()
    await divariate.getByRole('button', { name: 'Dataset' }).click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).nth(1).click()
    await divariate.getByPlaceholder('Select a variable').nth(1).click()
    await divariate.getByRole('button', { name: '4E.BP1_pT70' }).click()
    await divariate.getByRole('button', { name: 'Explore' }).click()
    await expect(divariate.locator('div').filter({ hasText: /^Keeping the 379 common rows\.$/ }).first(), 'Explore variable - Two variables - Expect result message.').toBeVisible()
    await expect(divariate.locator('div').filter({ hasText: /^PlotsSummaryStatistical tests$/ }).first(), 'Explore variable - Two variables - Expect 3 tabs in results.').toBeVisible()
    
    // 5 VARIABLES
    await page.getByText('up to 5 variables').click()
    divariate=page.locator('#multivariate')
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).first().click()
    await divariate.getByPlaceholder('Select a variable').first().click()
    await divariate.getByRole('button', { name: '4E.BP1', exact: true }).click()
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).nth(1).click()
    await divariate.getByPlaceholder('Select a variable').nth(1).click()
    await divariate.getByRole('button', { name: 'CDK1' }).click()
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'clinical217 variables' }).nth(2).click()
    await divariate.getByPlaceholder('Select a variable').nth(2).click()
    await divariate.getByRole('button', { name: 'patient.vital_status' }).click()
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'clinical217 variables' }).nth(3).click()
    await divariate.getByPlaceholder('Select a variable').nth(3).click()
    await divariate.getByRole('button', { name: 'patient.gender' }).click()
    await divariate.getByRole('button', { name: 'Dataset' }).first().click()
    await divariate.locator('a').filter({ hasText: 'clinical217 variables' }).nth(4).click()
    await divariate.getByPlaceholder('Select a variable').nth(4).click()
    await divariate.getByRole('button', { name: 'patient.day_of_form_completion' }).click()
    await divariate.getByRole('button', { name: 'Explore' }).click()
    await expect(divariate.getByText('Keeping the 379 common individuals (rows)'), 'Explore variable - Up tu 5 variables - Expect results message.').toBeVisible()
    // ALL VARIABLE
    
    await page.getByText('All variables in a dataset').click()
    divariate=page.locator('#univariatedataset')
    await divariate.getByRole('button', { name: 'Dataset' }).click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).click()
    await divariate.getByRole('button', { name: 'Explore' }).click()
    await expect(divariate.getByText('Numeric summaryNormality testPlots'), 'Explore variable - All variables - Expect 3 tabs in results.').toBeVisible({ timeout: 30000 })
    // CORRELATION
    await page.getByText('Correlation plot of a dataset', { exact: true }).click()
    divariate=page.locator('#corrplot')
    await divariate.getByRole('button', { name: 'Dataset' }).click()
    await divariate.locator('a').filter({ hasText: 'protein142 variables' }).click()
    await divariate.getByRole('button', { name: 'Explore' }).click()
    await expect(divariate.getByText('2. Correlation plot'), 'Explore variable - Correlation plot - Expect text "Correlation plot" in results.').toBeVisible({ timeout: 20000 })
    await divariate.getByRole('button', { name: 'Add to report' }).click()
    await expect(page.getByText('Analysis added to report')).toBeVisible({ timeout: 20000 })
  })
  test('Explore Heatmap', async () => {
    await new Promise(r => setTimeout(r, 2000));
    await page.goto('./explore/heatmap')
    await page.getByRole('button', { name: /Dataset/ }).first().click()
    await expect(page.getByText('protein142 variables').first()).toBeVisible({ timeout: 20000 })
    await page.locator('a').filter({ hasText: 'protein142 variables' }).first().click()
    await page.locator('label').filter({ hasText: 'Compute the dendrogram on individuals (rows) and sort them.' }).check()
    await page.getByRole('button', { name: /Dataset/ }).click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(1).click()
    await page.getByPlaceholder('Select a variable').click()
    await page.getByRole('button', { name: 'patient.samples.sample.2.sample_type' }).click()
    await page.getByRole('button', { name: 'Create heatmap' }).click()
    await expect(page.getByText('2. Heatmap'), 'Explore Heatmap - Error in expected result message.').toBeVisible({ timeout: 40000 })
  })

  test('PCA workflow', async () => {
    await new Promise(r => setTimeout(r, 3000));
    await page.goto('./explore/pca')   
    await page.once('load', () => console.log('Page PCA workflow loaded!')); 
    // step 1
    await page.getByRole('button', { name: /Dataset/ }).click()
    await page.locator('a').filter({ hasText: 'protein142 variables' }).first().click()
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.getByText('PlotsInertia table'), 'Explore PCA - Screen 1 - Expect 2 tabs in results.').toBeVisible({ timeout: 20000 })
    // step 2
    await page.getByText('Explore individuals').click()
    await page.getByRole('button', { name: /Dataset/ }).first().click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(1).click()
    await page.getByPlaceholder('Select a variable').first().click()
    await page.getByRole('button', { name: 'patient.gender' }).first().click()
    await page.getByRole('button', { name: 'Plot individuals' }).click()
    await expect(page.getByText('2. Plot individuals of protein on components'), 'Explore PCA - Screen individuals - Expect text results.').toBeVisible({ timeout: 8000 })
    // step 3
    await page.getByText('Explore variables', { exact: true }).click()
    await page.getByRole('button', { name: 'Plot variables' }).click()
    await expect(page.getByText('2. Plot the correlations of variables from protein with components'), 'Explore PCA - Screen variables - Expect text results.').toBeVisible()
    // step 4 - export
    await page.getByText('Extract new data').click()
    await page.getByRole('button', { name: 'Add to workspace' }).click()
    await expect(page.getByText('Dataset PCA_axes_ncp5_protein added to workspace'), 'Explore PCA - Screen extract - Expect text "Dataset successfully extracted".').toBeVisible({ timeout: 8000 })
  })

  test('Explore Clustering', async () => {
    await new Promise(r => setTimeout(r, 2000));
    await page.goto('./explore/clustering')
    await expect(page.getByText('2. Explore clustering'), 'Explore Clustering - Help - Error number of tabs.').toBeVisible({ timeout: 10000 })
    // step 1
    await page.getByRole('button', { name: 'Dataset' }).click()
    await page.locator('#datasetselect a').filter({ hasText: 'protein142 variables' }).click()
    await page.getByRole('button', { name: 'Run' }).click()
    await expect(page.getByText('Number of clusters chosen by broken stick: 10')).toBeVisible({ timeout: 20000 })
    // step 2
    await page.getByText('Make and explore clusters').click()
    await page.getByRole('textbox').fill('5')
    await page.getByRole('button', { name: 'Explore' }).click()
    await expect(page.getByText('HeatmapPlotsFrequency tableClusters'), 'Explore Clustering - Make cluster - Error number of tabs.').toBeVisible({ timeout: 20000 })
    // step 3 extract
    await page.locator('#dataset-label').click()
    await page.getByRole('button', { name: 'Add to workspace' }).click()
    await expect(page.getByText('added to workspace'), 'Explore clustering - Screen extract - Expect text "Dataset extracted successfully".').toBeVisible({ timeout: 30000 })
  })

  test('Explore SOM', async () => {
    await new Promise(r => setTimeout(r, 2000));
    await page.goto('./explore/som')
    await page.getByRole('button', { name: 'Dataset' }).click()
    await page.locator('#datasetselect a').filter({ hasText: 'protein142 variables' }).click()
    await page.getByRole('button', { name: 'Run SOM' }).click()
    await expect(page.getByText('PlotsSummaryCluster table'), 'Explore SOM - Error expect tabs in results.').toBeVisible({ timeout: 40000 })
    // 2nd step
    await page.locator('#wholeSection').getByText('Explore individuals').click()
    await page.getByRole('button', { name: 'Dataset' }).click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(1).click()
    await page.getByRole('textbox', { name: 'Select a variable' }).click()
    await page.getByText('patient.samples.sample.2.sample_type_id').click()
    await page.getByText('Purpose of Self-Organizing Map').first().click()
    await page.getByRole('button', { name: 'Explore individuals' }).click()
    await expect(page.getByText('Add to report').first(), 'Explore SOM - result message.').toBeVisible({ timeout: 10000 })
    // 3rd step
    await page.getByText('Explore prototypes', { exact: true }).click()
    await page.getByPlaceholder('Select a variable').click()
    await page.getByRole('button', { name: '4E.BP1_pT37' }).click()
    await page.getByRole('button', { name: 'Explore individuals' }).click()
    await expect(page.getByText('2. Explore prototypes'), 'Explore SOM - Explore prototypes, result error.').toBeVisible()
    await page.getByText('Superclustering').click()
    await expect(page.getByText('1. Cut clusters'), 'Explore SOM - Super clustering, loading error.').toBeVisible()
    await page.getByRole('textbox').fill('2')
    await page.getByRole('button', { name: 'Explore' }).click()
    await expect(page.getByText('PlotsFrequency tableSuper clusters'), 'Explore SOM - Error expect tabs in results of superClustering.').toBeVisible({ timeout: 40000 })
    
  })
  test('Explore - Back on previous results', async () => {
    await page.goto('./workflow/PCAobj_1')
    await page.once('load', () => console.log('Page PCAobj_1 loaded!'));
    await expect(page.getByText('Inertia table').first()).toBeVisible({ timeout: 10000 })
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible()
    
    await page.goto('./workflow/HACClustering_1')
    await page.once('load', () => console.log('Page HACClustering_1 loaded!'));
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible()
    await expect(page.getByText('Number of clusters chosen by broken stick: 10')).toBeVisible({ timeout: 10000 })

    await page.goto('./workflow/HACCutClusters_1')
    await page.once('load', () => console.log('Page HACCutClusters_1 loaded!'));
    await new Promise(r => setTimeout(r, 2000));
    await expect(page.getByText('1. Explore clustering of protein with HC')).toBeVisible({ timeout: 10000 })
    await expect(page.getByRole('textbox')).toHaveValue('5');
    await page.getByText('Choose the number of clusters').first().click()
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible()

    await page.goto('./workflow/SOMobj_1')
    await page.once('load', () => console.log('Page SOMobj_1 loaded!'));
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible({ timeout: 10000 })
    await expect(page.locator('div').filter({ hasText: /^PlotsSummaryCluster table$/ }).first()).toBeVisible();
      
    await page.goto('./workflow/SOMCutSuperClust_1')
    await page.once('load', () => console.log('Page SOMCutSuperClust_1 loaded!'));
    await expect(page.getByRole('textbox')).toHaveValue('2');
    await expect(page.locator('div').filter({ hasText: /^PlotsFrequency tableSuper clusters$/ }).first()).toBeVisible({ timeout: 10000 });
    await page.getByRole('list').getByText('Self-Organizing Map').click();
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible()

    await page.goto('./workflow/UnivariateDataset_1')
    await page.once('load', () => console.log('Page UnivariateDataset_1 loaded!'));
    let divariate=page.locator('#univariatedataset')
    await expect(divariate.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible({ timeout: 10000 })
    await expect(divariate.locator('div').filter({ hasText: /^Numeric summaryNormality testPlots$/ }).first()).toBeVisible();

  })
})
