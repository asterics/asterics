import { test, expect } from '@playwright/test'

test.describe.configure({ mode: 'serial' })
test.setTimeout(120000)

test.describe('Edit tests', () => {
  let page
  let url
  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage()
    await page.goto('./')
    await page.getByPlaceholder('Email').click()
    await page.getByPlaceholder('Email').fill('celine.noirot@inrae.fr')
    await page.getByPlaceholder('Workspace name').fill('DEMO')
    await page.locator('label').filter({ hasText: 'Load TCGA demo data into your workspace' }).locator('span').first().click()
    await page.locator('label').filter({ hasText: 'I confirm that I have read and that I agree to Asterics\' Privacy Policy.' }).locator('span').first().click()
    await page.getByRole('button', { name: 'Create' }).click()
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 }) 
    url = page.url()
    console.log('Before all create workspace test at ' + page.url())
  })

  test.beforeEach(async ({}, testInfo) => {
    console.log(`Running ${testInfo.title}`)
    await page.goto(url)
    await page.once('load', () => console.log('Page loaded!'));
  })

  test('Dataset edition', async () => {
    await page.goto('./edit/edit')
    await page.getByRole('button', { name: /Dataset/ }).click()
    await expect(page.locator('css=div.dropdown-menu')).toBeVisible({ timeout: 2000 })
    await page.locator('a').filter({ hasText: /protein/ }).click()
    await page.getByText('Transpose').check()
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.getByText('Select an action.')).toBeVisible({ timeout: 10000 })
    await page.getByPlaceholder('Dataset name').fill('protein_transposed');
    await page.getByRole('button', { name: 'Save' }).click()
    await expect(page.getByText('No history to display')).toBeVisible({ timeout: 10000 })
  })
  test('Add dataset with NA', async () => {
    await page.getByRole('button', { name: 'Add' }).click()
    await page.getByLabel('Select a file').setInputFiles('tests/data/proteinsMNAR.csv')
    await page.getByRole('combobox').selectOption('generic')
    await page.locator('css=button.pagination-next').filter({ hasNotText: 'Import' }).click()
    await expect(page.getByText('Space', { exact: true })).toBeVisible({ timeout: 20000 })
    await page.getByText('Space', { exact: true }).check()
    await expect(page.getByRole('cell', { name: '14.3.3_epsilon numeric' }).locator('span')).toHaveText('14.3.3_epsilonnumeric ')
    await page.getByRole('button', { name: 'Import' }).click()
    await page.waitForURL('**/workspace', { timeout: 30000 })
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 })
    await expect(page.getByText('Dataset proteinsMNAR added to')).toBeVisible()
  })
  test('Missing', async () => {
    await page.goto('./edit/missing')
    await page.getByRole('button', { name: /Dataset/ }).click()
    await expect(page.locator('css=div.dropdown-menu')).toBeVisible({ timeout: 2000 })
    await page.locator('a').filter({ hasText: /proteinsMNAR/ }).click()
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.locator('div').filter({ hasText: /^HeatmapsPlotsSummary$/ }).nth(3)).toBeVisible({ timeout: 15000 })
    await page.getByText('Remove missing values').click()
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.locator('div').filter({ hasText: /^SummaryPlotsExtract new data$/ }).nth(2)).toBeVisible()
    await page.getByText('Extract').click()
    await page.getByPlaceholder('Dataset name').fill('proteinsMNAR_MissRemoved')
    await page.getByRole('button', { name: /Add to workspace/ }).click()
    await expect(page.getByText('Dataset proteinsMNAR_MissRemoved added to workspace')).toBeVisible({ timeout: 10000 })
    await page.getByText('Impute missing values', { exact: true }).click()
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.getByText('Imputation by PCA')).toBeVisible({ timeout: 10000 })
    await page.locator('#extract-label').nth(1).click({ timeout: 10000 })
    await page.getByRole('button', { name: /Add to workspace/ }).click()
    await expect(page.getByText(/Dataset proteinsMNAR_Imputed_1 added to workspace/)).toBeVisible({ timeout: 10000 })
  })

  test('Normalized', async () => {
    await page.goto('./edit/normalization')
    await page.getByRole('button', { name: /Dataset/ }).click()
    await expect(page.locator('css=div.dropdown-menu')).toBeVisible({ timeout: 2000 })
    await page.locator('a').filter({ hasText: 'protein' }).first().click()
    await page.getByRole('combobox').selectOption('norm_quantiles')
    await page.getByRole('button', { name: /Run/ }).click()
    await expect(page.getByText(/PlotsExtract new data/)).toBeVisible({ timeout: 30000 })
    await page.getByText('Extract').click()
    await page.getByRole('button', { name: /Add to workspace/ }).click()
    await expect(page.getByText('Dataset protein_normalized_1')).toBeVisible()
  })
  test('Edit - Back on previous results', async () => {
        
    await page.goto('./workflow/editor_1')
    await expect(page.getByText(/transpose dataset/)).toBeVisible({ timeout: 20000 })
    await expect(page.getByText(/Dataset “protein_transposed”/)).toBeVisible()
    
    await page.goto('./workflow/MissOverview_1')
    await expect(page.getByRole('button', { name: 'proteinsMNAR' })).toBeVisible();
    await expect(page.getByText('Explore missing values')).toBeVisible()

    await page.goto('./workflow/MissShowRemove_1')
    await expect(page.getByText('Statistics on data removal')).toBeVisible()
    await expect(page.getByRole('textbox')).toHaveValue('proteinsMNAR');

    await page.goto('./workflow/imputeMissing_1')
    await expect(page.getByRole('textbox')).toHaveValue('proteinsMNAR');
    await expect(page.getByText(/Information on imputed values/)).toBeVisible()
    
    
    await page.goto('./workflow/norms_1')
    await expect(page.getByText(/PlotsExtract new data/)).toBeVisible({ timeout: 20000 })
    await expect(page.getByRole('button').filter({ hasText: 'Protein' })).toBeVisible()
  })
})
