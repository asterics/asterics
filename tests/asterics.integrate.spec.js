import { test, expect } from '@playwright/test'

test.describe.configure({ mode: 'serial' })
test.setTimeout(55000)

test.describe('Integrate tests', () => {
  let page
  let url
  // let url = 'http://localhost/workspace/b51dd917-912f-4edf-bfba-1651e1a7f8be'

  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage()
    await page.goto('./')
    await page.getByPlaceholder('Email').click()
    await page.getByPlaceholder('Email').fill('celine.noirot@inrae.fr')
    await page.getByPlaceholder('Workspace name').fill('DEMO')
    await page.locator('label').filter({ hasText: 'Load TCGA demo data into your workspace' }).locator('span').first().click()
    await page.locator('label').filter({ hasText: 'I confirm that I have read and that I agree to Asterics\' Privacy Policy.' }).locator('span').first().click()
    await page.getByRole('button', { name: 'Create' }).click()
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 })
    url = page.url()
    console.log('Before all create workspace test at ' + page.url())
  })

  test.beforeEach(async ({ page }, testInfo) => {
    console.log(`Running ${testInfo.title}`)
    await page.goto(url)
  })

  test('PLS', async ({ page }) => {
    await page.goto('./integrate/pls')
    await page.once('load', () => console.log('Page pls loaded!'));
    await expect(page.getByText('Purpose of Integrate two datasets with PLS:').first()).toBeVisible({ timeout: 10000 });
    await page.getByPlaceholder('Add a dataset').click();
    await page.getByRole('button', { name: 'mrna' }).first().click();
    await page.getByRole('button', { name: 'protein' }).first().click();
    await page.getByRole('button', { name: 'Integrate' }).click();
    await expect(page.getByText('General informationVenn diagramUpset plot')).toBeVisible({ timeout: 20000 });
    await page.getByText('Run PLS', { exact: true }).click();
    await page.getByRole('button', { name: 'Run' }).click();
    await expect(page.getByRole('alert').getByText('PLS performed in canonical mode.')).toBeVisible({ timeout: 40000 });
    await page.getByText('Explore individuals').click();
    await page.getByRole('button', { name: 'Dataset' }).first().click();
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).first().click();
    await page.getByPlaceholder('Select a variable').first().click();
    await page.getByRole('button', { name: 'patient.vital_status' }).click();
    await page.getByRole('button', { name: 'Plot individuals' }).click();
    await expect(page.getByRole('alert').getByText('Keeping the 379 common individuals (rows)')).toBeVisible();
    await page.getByText('Explore variables', { exact: true }).click();
    await page.getByRole('button', { name: 'Plot variables' }).click();
    await page.getByRole('button', { name: 'Add to report' }).click()
    await page.getByText('Extract new data').click();
    await page.getByRole('button', { name: 'Add to workspace' }).click();
    await expect(page.getByText('added to workspace')).toBeVisible();
    
  })

  test('PLS-DA', async ({ page }) => {
    test.slow()
    await page.getByRole('link', { name: 'Integrate', exact: true }).click()
    // 3d card is PLS DA
    await page.goto('./integrate/plsda')
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'protein142 variables' }).first().click()
    await page.getByRole('button', { name: 'Dataset' }).click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(1).click()
    await page.getByRole('textbox', { name: 'Select a variable' }).click()
    await page.getByRole('button', { name: 'patient.vital_status' }).click()
    await page.getByRole('button', { name: 'Integrate' }).click()
    await expect(page.getByText('General informationVenn diagramUpset plot')).toBeVisible({ timeout: 40000 })
    // step 2
    await page.getByText('Run PLS-DA', { exact: true }).click()
    await page.getByRole('button', { name: 'Run' }).click()
    await expect(page.getByText('2. Plots', { exact: true })).toBeVisible({ timeout: 10000 })
    // step 3
    await page.getByText('Explore individuals').click()
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'protein142 variables' }).nth(2).click()
    await page.getByPlaceholder('Select a variable').nth(1).click()
    await page.getByRole('button', { name: '53BP1' }).click()
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(3).click()
    await page.getByPlaceholder('Select a variable').nth(2).click()
    await page.getByRole('button', { name: 'patient.vital_status' }).click()
    await page.getByRole('button', { name: 'Plot individuals' }).click()
    await expect(page.getByRole('alert').getByText('Keeping the 379 common individuals (rows)')).toBeVisible()
    await expect(page.getByText('2. Plot individuals on components')).toBeVisible()
    // step 4
    await page.getByText('Explore variables', { exact: true }).click()
    await page.getByRole('button', { name: 'Plot variables' }).click()
    // step 5
    await page.getByText('Extract new data').click()
    await page.getByRole('button', { name: 'Add to workspace' }).click()
    /* await expect(page.getByText('This dataset name is too long')).toBeVisible()*/
    await page.getByRole('textbox').nth(1).fill('PLSDA_axes_ncp5')
    await page.getByRole('button', { name: 'Add to workspace' }).click() 
    await expect(page.getByText('added to workspace')).toBeVisible({ timeout: 10000 })
  })

  test('MFA', async ({ page }) => {
    test.slow()
    await page.goto('./integrate/mfa')
    await page.getByPlaceholder('Add a dataset').click()
    await page.getByRole('button', { name: 'protein' }).first().click()
    await page.getByRole('button', { name: 'mrna' }).first().click()
    await page.getByRole('button', { name: 'clinical' }).first().click()
    await page.getByText('Purpose of Integrate datasets with MFA').first().click()
    await page.getByRole('button', { name: 'Integrate' }).click()
    await expect(page.getByRole('alert').locator('div').nth(1)).toBeVisible({ timeout: 10000 });
    await expect(page.getByText('General informationVenn diagramUpset plot')).toBeVisible({ timeout: 10000 })
    await page.getByText('Run MFA', { exact: true }).click()
    await page.getByRole('button', { name: 'Run' }).click()
    await expect(page.getByText('94 irrelevant variables (admin.bcr, admin.disease_code, admin.file_uuid, admin.p').nth(1)).toBeVisible({ timeout: 150000 })
    await expect(page.getByText('PlotsNumeric summaries')).toBeVisible()
    await page.getByText('Explore individuals').click()
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).first().click()
    await page.getByPlaceholder('Select a variable').first().click()
    await page.getByRole('button', { name: 'patient.clinical_cqcf.tumor_type' }).click()
    await page.getByRole('button', { name: 'Plot individuals' }).click()
    await expect(page.getByRole('alert').getByText('Keeping the 379 common individuals (rows)')).toBeVisible({ timeout: 40000 })
    await page.getByText('Explore variables', { exact: true }).click()
    await page.getByRole('button', { name: 'Plot variables' }).click()
    await expect(page.getByRole('alert').getByText('No categorical variable reaches this correlation threshold on the PCs. Only nume')).toBeVisible({ timeout: 40000 })
    await page.getByText('Explore groups', { exact: true }).click()
    await page.getByRole('button', { name: 'Plot groups' }).click()
    await expect(page.getByText('2. Plots', { exact: true })).toBeVisible()
    await page.getByText('Extract new data').click()
    await page.getByRole('textbox').nth(1).fill('MFA_all')
    await page.getByRole('button', { name: 'Add to workspace' }).click()
    //await expect(page.getByText('Dataset MFA_all added to workspace')).toBeVisible({ timeout: 8000 })
  })

  test('Differential analysis', async ({ page }) => {
    test.slow()
    await page.goto('./integrate/differentialanalysis')
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'protein142 variables' }).first().click()
    await page.getByRole('button', { name: 'Dataset' }).first().click()
    await page.locator('a').filter({ hasText: 'clinical217 variables' }).nth(1).click()
    await page.getByPlaceholder('Select a variable').click()
    await page.getByRole('button', { name: 'patient.samples.sample.2.sample_type' }).click({ timeout: 5000 })
    await page.getByRole('button', { name: 'Integrate' }).click()
    await expect(page.getByText('379 common individuals (rows) have been kept (based on row names).')).toBeVisible({ timeout: 15000 })
    await page.getByText('Multiple tests', { exact: true }).click()
    await page.getByText('Parametric', { exact: true }).click();
    await page.getByRole('textbox').fill('0.7');
    await page.getByRole('button', { name: 'Run' }).click()
    await expect(page.getByText('PlotsHeatmapSummary')).toBeVisible({ timeout: 20000 })
    await page.getByText('Posthoc tests', { exact: true }).click();
    await page.getByRole('button', { name: 'Run post-hoc tests' }).click();
    await expect(page.getByRole('alert').getByText('Given your data and options,')).toBeVisible({ timeout: 20000 });
    await page.locator('#posthocplot-label').click();
    await page.getByRole('combobox').selectOption('Caveolin.1');
    await page.getByRole('button', { name: 'Plot results' }).click();
    await expect(page.getByText('Forest plot', { exact: true })).toBeVisible();
    await page.getByText('Extract new data').click()
    await page.getByRole('button', { name: 'Add to workspace' }).click()
    await expect(page.getByText('added to workspace')).toBeVisible();
  })
})
