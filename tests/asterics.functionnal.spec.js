import { test, expect } from '@playwright/test'

test.describe.configure({ mode: 'serial' })
test.setTimeout(120000)

test.describe('Functionnal tests', () => {
  let page
  let url
  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage()
  })

  test('Go to non existing workspace', async () => {
    await page.goto('./workspace/false-uuid')
    expect(await page.locator('.column > img').first().screenshot({ path: './img/404.png' }))
  })
  test('2 workspaces in same browser', async () => {
    let url_nodata
    let url_data
    await page.goto('./')
    await page.getByPlaceholder('Email').click()
    await page.getByPlaceholder('Email').fill('celine.noirot@inrae.fr')
    await page.getByPlaceholder('Workspace name').fill('DATA')
    await page.locator('label').filter({ hasText: 'Load TCGA demo data into your workspace' }).locator('span').first().click()
    await page.locator('label').filter({ hasText: 'I confirm that I have read and that I agree to Asterics\' Privacy Policy.' }).locator('span').first().click()
    await page.getByRole('button', { name: 'Create' }).click()
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 })
    url_data = page.url()
    await page.goto('./')
    await page.getByPlaceholder('Email').click()
    await page.getByPlaceholder('Email').fill('celine.noirot@inrae.fr')
    await page.getByPlaceholder('Workspace name').fill('NODATA')
    await page.locator('label').filter({ hasText: 'I confirm that I have read and that I agree to Asterics\' Privacy Policy.' }).locator('span').first().click()
    await page.getByRole('button', { name: 'Create' }).click()
    await expect(page.getByText('Workflow')).toBeVisible({ timeout: 30000 })
    url_nodata = page.url()
    await page.goto(url_data)
    await expect(page.locator('.card-content').filter({ hasText: /^Datasets3$/ })).toBeVisible();
    await page.goto(url_nodata)
    await expect(page.locator('.card-content').filter({ hasText: /^Datasets0$/ })).toBeVisible();
    
  })
})
